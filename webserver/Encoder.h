//
// Encoder object
//

#ifndef TANGOGQL_ENCODER_H
#define TANGOGQL_ENCODER_H
#include <inttypes.h>
#include <string>

namespace TangoGQL_ns {

class Encoder {

public:
  static void sha1(const char *input,int length, uint8_t *digest);
  static std::string sha1_hex(uint8_t *digest);
  static std::string base64_decode(std::string const& encoded_string);
  static std::string base64_encode(unsigned char const* bytes_to_encode, unsigned int in_len);

};

} // namespace TangoGQL_ns

#endif //TANGOGQL_ENCODER_H
