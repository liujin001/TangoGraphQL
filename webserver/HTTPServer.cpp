// -------------------------------------------------------------------------------
// HTTP main thread
// -------------------------------------------------------------------------------
namespace TangoGQL_ns {
class HTTPServer;
}

#include "HTTPServer.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>

namespace TangoGQL_ns {

// ----------------------------------------------------------------------------------------
// Constructor
// ----------------------------------------------------------------------------------------
HTTPServer::HTTPServer(TangoGQL *tangoGql, omni_mutex &m, int p) :
        Tango::LogAdapter(tangoGql), mutex(m), ds(tangoGql), port(p) {

  exitThread = false;
  serverSock = -1;
  start_undetached();

}

// ----------------------------------------------------------------------------------------
// Main loop
// ----------------------------------------------------------------------------------------
void *HTTPServer::run_undetached(void *arg) {

  INFO_STREAM << "HTTPServer::run(): Start thread" << std::endl;

  // Create socket
  serverSock = socket(AF_INET,SOCK_STREAM,0);

  if(serverSock<0) {
    std::cerr << "Error: Invalid socket :" << std::string(strerror(errno)) << std::endl;
    return nullptr;
  }

  struct sockaddr_in soc_addr{};

  // Reuse Address
  int32_t yes = 1;
  if(setsockopt(serverSock,SOL_SOCKET,SO_REUSEADDR,(char *)&yes,sizeof(yes)) < 0) {
    ERROR_STREAM << "Warning: Couldn't Reuse Address: " << std::string(strerror(errno)) << std::endl;
  }

  // Bind
  memset(&soc_addr,0,sizeof(soc_addr));
  soc_addr.sin_family = AF_INET;
  soc_addr.sin_port = htons(port);
  soc_addr.sin_addr.s_addr = htonl(INADDR_ANY);

  if(bind(serverSock,(struct sockaddr*)&soc_addr,sizeof(soc_addr))) {
    std::cerr << "Error: Can not bind socket. Another server running?" << std::endl << std::string(strerror(errno)) << std::endl;
    return nullptr;
  }

  // Listen (listen queue size = 256)
  if(listen(serverSock,256)<0) {
    std::cerr << "Error: Can not listen to socket " << std::string(strerror(errno)) << std::endl;
    return nullptr;
  }
  std::cout << "HTTPServer: listen on port " << port << std::endl;

  // Accept loop
  while(!exitThread) {

    int clientSock;
    struct sockaddr_in client_add{};
    socklen_t len = sizeof(sockaddr_in);

    if((clientSock = accept(serverSock,(struct sockaddr*)&client_add,&len)) < 0) {

      if(!exitThread) {
        ERROR_STREAM << "Error: Invalid Socket returned by accept(): " << std::string(strerror(errno)) << std::endl;
      }

    } else {

      CLIENTINFO clientInfo;
      clientInfo.ip = std::string(inet_ntoa(client_add.sin_addr));
      clientInfo.port = ntohs(client_add.sin_port);
      clientInfo.name = "";
      clientInfo.socket = clientSock;
      int32_t yes = 1;
      if(setsockopt(clientSock,SOL_SOCKET,SO_REUSEADDR,(char *)&yes,sizeof(yes)) < 0) {
        ERROR_STREAM << "Warning: Couldn't Reuse Address: " << std::string(strerror(errno)) << std::endl;
      }
      if(ds->resolveClientIP) {
        // Resolve name
        socklen_t len;
        char hbuf[NI_MAXHOST];
        int err = getnameinfo((struct sockaddr *) &client_add, sizeof(sockaddr_in), hbuf, sizeof(hbuf),
                        NULL, 0, NI_NAMEREQD);
        if(err==0) {
          clientInfo.name = std::string(hbuf);
        } else {
          std::cout << "Error: " << gai_strerror(err) << std::endl;
        }
      }
      INFO_STREAM << "HTTPServer::accept("<<clientSock<<")" << std::endl;
      ds->connManager->start(clientInfo);

    }

  }

  INFO_STREAM << "HTTPServer::run(): End thread" << std::endl;
  return nullptr;

}

void HTTPServer::stop() {
  exitThread = true;
  shutdown(serverSock,SHUT_RDWR);
}


} // namespace TangoGQL_ns