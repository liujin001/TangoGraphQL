// -------------------------------------------------------------------------------
// Connection  class
// -------------------------------------------------------------------------------

#ifndef TANGOGQL_CONNECTION_H
#define TANGOGQL_CONNECTION_H

#define WRITE_TIMEOUT 250
#define MAX_WS_FRAME (32*1024*1024)
#define MAX_POST_CONTENT (32*1024*1024)
#define MAX_GET_REQUEST (8*1024)

#include <TangoGQL.h>
#include <DeviceFactory.h>
#include "../graphql/GQLEngine.h"

namespace TangoGQL_ns {

class WSFrame;
class Request;
class WSKeepAlive;


class Connection : public omni_thread, public Tango::LogAdapter {

public:

  // Constructor
  Connection(TangoGQL *,ConnectionManager *,int , int, std::string);
  ~Connection();
  void *run_undetached(void *);

  /**
   * Returns the WebSocket UUID
   */
  std::string getUUID();

  /**
   * Anort connection
   * @param signalEnd Signal abort to ConnectionManager
    * @param signalWSEnd Signal closing to WS client
   */
  void Abort(bool signalEnd=true,bool signalWSEnd=true);

  /**
   * Write the given request to the socket
   * @param request
   * @param timeout
   * @return
   */
  int Write(Request &request,int timeout);

  /**
   * Write the given WebSocket frame to the socket
   * @param frame
   * @param timeout
   * @return
   */
  int Write(WSFrame &frame,int timeout);

  /**
   * Returns true if this connection has been aborted
   */
  bool isAborted() const;

  /**
   * Send complete message to WS client
   */
  void sendComplete();

  /**
   * Return the socket
   */
  int getSock() const;

  /**
   * Return true if protocol has been upgraded to WS
   */
  bool isWS();

  /**
   * Mutex locked while a Tango event is processing
   */
  omni_mutex wsProcessingMutex;

  /**
   * Returns total number of byte sent through this connection
   */
   uint64_t getNbSent();

  /**
   * Returns total number of byte received through this connection
   */
  uint64_t getNbRecv();

  /**
   * Returns total number of call received by this connection
   */
  uint64_t getNbCall();


  /**
   * Returns connection status
   */
  std::string& getStatus();

private:

  bool HandleGet(Request& req);
  bool HandlePost(Request& req);
  bool HandleOption(Request& req);
  bool HandleWebSocket(char firstByte);
  bool HandleAuth(Request &req,bool& authenticationOK);

  void Close();
  int _WaitFor(int sock,int timeout,int mode);
  int _Write(int sock,char *buf,int bufsize,int timeout);
  int _Read(int sock,char *buf,int bufsize,int timeout);

  int DecodeWSFrame(WSFrame &frame,char firstByte);

  TangoGQL *ds;
  ConnectionManager *parent;
  WSKeepAlive *keepAlive;
  int socket;
  int idleTimeout;
  bool signalEnd;
  bool signalWSEnd;
  bool isWebSocket;
  bool aborted;
  std::string UUID;
  std::string lastError;
  std::string clientInfo;
  DeviceFactory *factory;
  GQLEngine *gqlEngine;
  omni_mutex wsWriteMutex;
  SSL *ssl;
  uint64_t bytesRecv;
  uint64_t bytesSent;
  uint64_t nbCall;
  int connStatus;

};

} // namespace TangoGQL_ns

#endif //TANGOGQL_CONNECTION_H
