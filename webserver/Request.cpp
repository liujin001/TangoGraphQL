//
// HTTP request
//

#include "Request.h"
#include <iostream>
#include "StringUtils.h"
#include "Encoder.h"


namespace TangoGQL_ns {


Request::Request() {
  clear();
}

void Request::clear() {
  type.clear();
  url.clear();
  complete = false;
  line = 0;
  buffer.clear();
  buffer.reserve(1024);
  content.clear();
  headers.clear();
}

bool Request::isComplete() const {
  return complete;
}

bool Request::getHeader(const std::string& name,std::string &header) {

  bool found = false;
  size_t i = 0;
  while(!found && i<headers.size()) {
    found = ::strcasecmp(name.c_str(),headers[i].name.c_str())==0;
    if(!found) i++;
  }
  if(found)
    header = headers[i].value;

  return found;

}

int Request::getContentLength() {
  std::string cl;
  bool hasHeader = getHeader("Content-Length",cl);
  if(!hasHeader) {
    return 0;
  } else {
    int size = (int)::strtol(cl.c_str(), nullptr, 10);
    if(size>MAX_POST_CONTENT)
      return 0;
    else
      return size;
  }
}

void Request::addChar(char c) {

  buffer.push_back(c);
  if(buffer.length()>MAX_GET_REQUEST)
    throw std::string("Invalid request (too long)");

  if(c=='\n' && ends_with(buffer,"\r\n")) {

    if(line==0) {

      std::vector<std::string> fields;
      split(fields,buffer,' ');
      if(fields.size()!=3)
        throw std::string("Invalid HTTP request, expect 3 fields");
      type = fields[0];
      url = fields[1];
      version = fields[2];
      rtrim(version);
      buffer.clear();
      buffer.reserve(256);
      line++;

    } else {

      if( buffer.length()==2 ) {

        complete = true;

      } else {

        size_t pos = buffer.find(':',0);
        if( pos==std::string::npos ) {
          throw std::string("Invalid request header");
        }
        HEADER h;
        h.name = buffer.substr(0,pos);
        h.value = buffer.substr(pos+1);
        trim(h.value);
        headers.push_back(h);
        buffer.clear();
        buffer.reserve(256);
        line++;

      }

    }

  }

}

void Request::addHeader(std::string& name,std::string& value) {
  headers.push_back({name,value});
}

void Request::createBadResponse(Tango::DevError &e) {

  version = "HTTP/1.1";
  status = "404";
  headers.clear();
  content.clear();
  content.reserve(256);
  content.append("<html><body>");
  content.append("<div>Origin" + std::string(e.origin) + "</div>");
  content.append("<div>Desc" + std::string(e.desc) + "</div>");
  content.append("<div>Reason" + std::string(e.reason) + "</div>");
  content.append("</body></html>");

  headers.push_back({"Access-Control-Allow-Origin","*"});
  headers.push_back({"Content-Type","text/html;charset=utf-8"});
  headers.push_back({"Content-Length",std::to_string(content.size())});

}

void Request::createUnauthorizedResponse(const std::string& message) {

  version = "HTTP/1.1";
  status = "401";
  headers.clear();
  content.clear();
  content.reserve(512);
  content.append(R"({"errors":"Unauthorized"})");

  headers.push_back({"WWW-Authenticate","Basic realm=\"TangoGraphQL Access\""});
  headers.push_back({"Access-Control-Allow-Origin","*"});
  headers.push_back({"Access-Control-Allow-Credentials","true"});
  headers.push_back({"Access-Control-Allow-Headers","Content-Type, Authorization"});
  headers.push_back({"Content-Type","text/html;charset=utf-8"});
  headers.push_back({"Content-Length",std::to_string(content.size())});

}

void Request::createForbiddenResponse() {

  version = "HTTP/1.1";
  status = "403";
  headers.clear();
  content.clear();
  content.reserve(512);
  content.append(R"({"errors":"Forbidden"})");

  headers.push_back({"Content-Type","text/html;charset=utf-8"});
  headers.push_back({"Content-Length",std::to_string(content.size())});

}

void Request::createBadResponse(const std::string& message) {

  version = "HTTP/1.1";
  status = "404";
  headers.clear();
  content.clear();
  content.reserve(512);
  content.append("<html><body>");
  content.append("<div>" + message + "</div>");
  content.append("</body></html>");

  headers.push_back({"Access-Control-Allow-Origin","*"});
  headers.push_back({"Content-Type","text/html;charset=utf-8"});
  headers.push_back({"Content-Length",std::to_string(content.size())});

}

void Request::createJSONResponse(const std::string& content) {

  version = "HTTP/1.1";
  status = "200";
  headers.clear();
  this->content = content;

  headers.push_back({"Access-Control-Allow-Origin","*"});
  headers.push_back({"Content-Type","application/json; charset=utf-8"});
  headers.push_back({"Content-Length",std::to_string(content.size())});

}

void Request::createHelloResponse() {

  version = "HTTP/1.1";
  status = "200";
  headers.clear();
  content.append("<html><body>");
  content.append("<div><h1>Hello</h1></div>");
  content.append("</body></html>");

  headers.push_back({"Access-Control-Allow-Origin","*"});
  headers.push_back({"Content-Type","text/html;charset=utf-8"});
  headers.push_back({"Content-Length",std::to_string(content.size())});

}

void Request::createOptionResponse(const std::string& allowedRequests,bool auth) {

  version = "HTTP/1.1";
  status = "204"; // No content
  headers.clear();
  content.clear();
  headers.push_back({"Access-Control-Allow-Methods",allowedRequests});
  if(auth) {
    headers.push_back({"WWW-Authenticate","Basic realm=\"TangoGraphQL Access\""});
    headers.push_back({"Access-Control-Allow-Headers", "Content-Type,Authorization"});
    headers.push_back({"Access-Control-Allow-Credentials","true"});
  } else {
    headers.push_back({"Access-Control-Allow-Headers", "Content-Type"});
  }
  headers.push_back({"Access-Control-Allow-Origin", "*"});
  headers.push_back({"Content-Type","application/octet-stream"});

}

void Request::createArbResponse(void *buffer,int buffLength,const std::string& contentType) {

  version = "HTTP/1.1";
  status = "200";
  headers.clear();
  content.clear();
  content.reserve(buffLength);
  content.append((char *)buffer,buffLength);
  headers.push_back({"Access-Control-Allow-Origin","*"});
  headers.push_back({"Content-Type",contentType});
  headers.push_back({"Content-Length",std::to_string(content.size())});

}

void Request::createWebsocketHandshakeResponse(const std::string& key) {

  uint8_t digest[20];
  std::string rep = key + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
  Encoder::sha1(rep.c_str(),rep.size(),digest);
  std::string wsKEy = Encoder::base64_encode(digest,20);

  version = "HTTP/1.1";
  status = "101";
  headers.clear();
  content.clear();
  headers.push_back({"Upgrade","websocket"});
  headers.push_back({"Connection","Upgrade"});
  headers.push_back({"Access-Control-Allow-Origin","*"});
  headers.push_back({"Sec-WebSocket-Accept",wsKEy});
  headers.push_back({"Sec-WebSocket-Protocol","graphql-transport-ws"});

}

void Request::to_buffer(std::string& buff) {

  buff.reserve(2048 + content.length());
  buff.append(version);
  buff.append(" ");
  buff.append(status);
  buff.append("\r\n");
  for(auto & header : headers) {
    buff.append(header.name);
    buff.append(": ");
    buff.append(header.value);
    buff.append("\r\n");
  }
  buff.append("\r\n");
  buff.append(content);

}

std::string Request::toString(bool withHeader) {

  std::string ret;
  ret.reserve(2048);

  if(!type.empty()) {
    ret.append(type);
    ret.push_back(' ');
  }
  if(!url.empty()) {
    ret.append(url);
    ret.push_back(' ');
  }
  if(!version.empty()) {
    ret.append(version);
    ret.push_back(' ');
  }
  if(!status.empty()) {
    ret.append(status);
    ret.push_back(' ');
  }
  if( withHeader && !headers.empty() ) {
    ret.push_back('{');
    for (size_t i = 0; i < headers.size(); i++) {
      ret.push_back('\"');
      ret.append(headers[i].name);
      ret.push_back('\"');
      ret.push_back(':');
      ret.push_back('\"');
      ret.append(headers[i].value);
      ret.push_back('\"');
      if(i<headers.size()-1) ret.push_back(',');
    }
    ret.push_back('}');
  }

  ret.append(" Content: ");
  ret.append(std::to_string(content.size()));
  ret.append(" bytes");
  //ret.append(content);

  return ret;

}

void Request::getContentType(std::string& fileName,std::string &type) {

  int idx = fileName.rfind('.');
  std::string ext = fileName.substr(idx+1,std::string::npos);
  transform(ext.begin(), ext.end(), ext.begin(),
            [](unsigned char c){ return std::tolower(c); });

  // List of few MIME type
  if(ext=="htm" || ext=="html" || ext=="txt" ) {
    type = "text/html; charset=UTF-8";
  } else if(ext=="css") {
    type = "text/css; charset=UTF-8";
  } else if(ext=="json") {
    type = "application/json; charset=UTF-8";
  } else if(ext=="js") {
    type = "text/javascript; charset=UTF-8";
  } else if(ext=="gz") {
    type = "application/gzip";
  } else if(ext=="ico") {
    type = "image/vnd.microsoft.icon";
  } else if(ext=="gif") {
    type = "image/gif";
  } else if(ext=="jpeg" || ext=="jpg") {
    type = "image/jpeg";
  } else if(ext=="png") {
    type = "image/png";
  } else {
    type = "application/octet-stream";
  }

}

} // class HTTPServer
