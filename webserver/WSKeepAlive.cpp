// -------------------------------------------------------------------------------
// WSKeepAlive  class
// -------------------------------------------------------------------------------

#include "WSKeepAlive.h"
#include "WSFrame.h"

namespace TangoGQL_ns {

// ----------------------------------------------------------------------------------------
// Constructor
// ----------------------------------------------------------------------------------------
WSKeepAlive::WSKeepAlive(TangoGQL *tangoGql,Connection *conn, int refreshPeriod) :
        Tango::LogAdapter(tangoGql), conn(conn), refreshPeriod(refreshPeriod) {

  start_undetached();

}

// ----------------------------------------------------------------------------------------
// Main KeepAlive loop
// ----------------------------------------------------------------------------------------
void *WSKeepAlive::run_undetached(void *arg) {

  INFO_STREAM << "WSKeepAlive::WSKeepAlive(): Start thread" << std::endl;

  int s = refreshPeriod;

  while(!conn->isAborted()) {
    usleep(50000);
    s -= 50;
    if(s<0 && !conn->isAborted()) {
      WSFrame ping;
      ping.PingFrame();
      conn->Write(ping,500);
      s = refreshPeriod;
    }
  }

  INFO_STREAM << "WSKeepAlive::WSKeepAlive(): End thread" << std::endl;

  return nullptr;

}

}

