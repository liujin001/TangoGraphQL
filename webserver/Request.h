//
// HTTP request
//

#ifndef TANGOGQL_REQUEST_H
#define TANGOGQL_REQUEST_H

#include <TangoGQL.h>

namespace TangoGQL_ns {

typedef struct {
  std::string name;
  std::string value;
} HEADER;

class Request {

public:

  Request();

  /**
   * Get the specifed header of the request
   * @param name Header name
   * @param header Header value
   * @return true if the header exists, false otherwise
   */
  bool getHeader(const std::string& name,std::string &header);

  // Input request
  bool isComplete() const;
  std::string getURL() { return url; };
  void addChar(char c);
  void clear();
  std::string getType() { return type; };
  int getContentLength();
  std::string toString(bool withHeader);

  // Output request
  void addHeader(std::string& name,std::string& value);
  void createBadResponse(Tango::DevError &e);
  void createBadResponse(const std::string& message);
  void createUnauthorizedResponse(const std::string& message);
  void createForbiddenResponse();
  void createJSONResponse(const std::string& content);
  void createOptionResponse(const std::string& allowedRequests,bool auth);
  void createHelloResponse();
  void createArbResponse(void *buffer,int buffLength,const std::string& contentType);
  void createWebsocketHandshakeResponse(const std::string& key);
  void to_buffer(std::string& buff);

  static void getContentType(std::string& fileName,std::string &type);

private:

  std::string status;
  std::string content;
  std::string type;
  std::string url;
  std::string version;
  std::vector<HEADER> headers;

  std::string buffer;
  bool complete;
  int line;

}; // class{} Request

} // namespace TangoGQL_ns

#endif //TANGOGQL_REQUEST_H
