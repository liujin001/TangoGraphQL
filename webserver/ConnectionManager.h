// -------------------------------------------------------------------------------
// Connection manager class
// -------------------------------------------------------------------------------

#ifndef TANGOGQL_CONNECTIONMANAGER_H
#define TANGOGQL_CONNECTIONMANAGER_H
#include <string>
#include <vector>

namespace TangoGQL_ns {

class ConnectionManager;

// ConnectionManager Information
typedef struct {

  std::string ip;
  std::string name;
  int totalConn;
  int totalWSConn;
  uint64_t bytesRecv;
  uint64_t bytesSent;

} CLIENTSTAT;

typedef struct {

  int totalConn;
  int totalWSConn;
  std::vector<CLIENTSTAT> clients;

} CONMNECTIONINFOS;

typedef struct {

  std::string ip;
  int socket;
  uint64_t nbCall;
  std::string status;
  bool isWS;

} SOCKETINFO;

}

#include <TangoGQL.h>
#include "Connection.h"

namespace TangoGQL_ns {

#define NO_CMD     0  // Exit connection manager
#define EXIT_CMD   1  // Exit connection manager
#define START_CMD  2  // Start a new connection
#define END_CMD    3  // Close a connection


// Incoming client info
typedef struct {

  std::string name; // Client name (empty std::string if not resolved)
  std::string ip;   // IP address
  int    port;      // Client port
  int    socket;    // Client socket (returned by accept)
  Connection *conn; // The connection

} CLIENTINFO;


// Command interface
typedef struct {

  int command;
  CLIENTINFO client;
  Connection *connToEnd;

} COMMAND;

class ConnectionManager : public omni_thread, public Tango::LogAdapter {

public:

  // Constructor
  ConnectionManager(TangoGQL *, omni_mutex &);
  void *run_undetached(void *);

  // Commands
  void start(CLIENTINFO& client);
  void stop();
  void end(Connection *conn);

  // Infos
  void getInfos(CONMNECTIONINFOS& infos);
  void getSocketInfos(std::vector<SOCKETINFO>& infos);

private:

  std::vector<CLIENTINFO> connections;
  std::vector<COMMAND> commandToExecute;
  TangoGQL *ds;
  omni_mutex &mutex;
  pthread_cond_t command_cond;
  pthread_mutex_t command_mutex;


}; // class ConnectionManager

} // namespace TangoGQL_ns

#endif //TANGOGQL_CONNECTIONMANAGER_H
