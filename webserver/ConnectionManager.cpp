// -------------------------------------------------------------------------------
// Connection manager class
// -------------------------------------------------------------------------------

namespace TangoGQL_ns {
class ConnectionManager;
}

#include "ConnectionManager.h"
#include <algorithm>

namespace TangoGQL_ns {

// ----------------------------------------------------------------------------------------
// Constructor
// ----------------------------------------------------------------------------------------
ConnectionManager::ConnectionManager(TangoGQL *tangoGql, omni_mutex &m) :
        Tango::LogAdapter(tangoGql), mutex(m), ds(tangoGql) {

  commandToExecute.clear();
  command_cond = PTHREAD_COND_INITIALIZER;
  command_mutex = PTHREAD_MUTEX_INITIALIZER;
  start_undetached();

}

// ----------------------------------------------------------------------------------------
// Main loop
// ----------------------------------------------------------------------------------------
void *ConnectionManager::run_undetached(void *arg) {

  INFO_STREAM << "ConnectionManager::run(): Start thread" << std::endl;

  bool exit = false;
  while(!exit) {

    // Wait for command
    pthread_mutex_lock(&command_mutex);
    pthread_cond_wait(&command_cond, &command_mutex);

    while(!commandToExecute.empty()) {

      COMMAND commandToExec = commandToExecute[commandToExecute.size()-1];

      switch (commandToExec.command) {

        case EXIT_CMD: {
          for (auto &connection: connections)
            connection.conn->Abort(false);
          for (auto &connection: connections)
            connection.conn->join(nullptr);
          exit = true;
        }
          break;

        case START_CMD: {
          std::string clientInfo = commandToExec.client.ip + ":" + std::to_string(commandToExec.client.port);
          commandToExec.client.conn = new Connection(ds, this, commandToExec.client.socket, ds->idleTimeout,
                                                        clientInfo);
          connections.push_back(commandToExec.client);
        }
          break;

        case END_CMD: {
          bool found = false;
          size_t i = 0;
          while (!found && i < connections.size()) {
            found = connections[i].conn == commandToExec.connToEnd;
            if (!found) i++;
          }
          if (!found) {
            ERROR_STREAM << "Connection for socket " << commandToExec.connToEnd->getSock() << " not found !" << std::endl;
          } else {
            INFO_STREAM << "ConnectionManager::run(): Remove connection " << commandToExec.connToEnd->getSock() << std::endl;
            connections.erase(connections.begin() + i);
            commandToExec.connToEnd->join(nullptr);
          }
        }
        break;

      }

      commandToExecute.pop_back();

    }

    pthread_mutex_unlock(&command_mutex);

  }

  INFO_STREAM << "ConnectionManager::ConnectionManager(): End thread" << std::endl;
  return nullptr;

}

// ----------------------------------------------------------------------------------------

void ConnectionManager::getInfos(CONMNECTIONINFOS& infos) {

  pthread_mutex_lock(&command_mutex);

  size_t connNum = connections.size();
  infos.totalConn = connNum;
  infos.totalWSConn = 0;
  infos.clients.clear();

  for (auto &connection : connections) {
    std::string& ipToSearch = connection.ip;
    bool found = false;
    size_t i = 0;
    while(!found && i<infos.clients.size()) {
      found = ipToSearch == infos.clients[i].ip;
      if(!found) i++;
    }
    if(found) {
      infos.clients[i].totalConn ++;
      infos.clients[i].bytesRecv += connection.conn->getNbRecv();
      infos.clients[i].bytesSent += connection.conn->getNbSent();
      if(connection.conn->isWS()) {
        infos.clients[i].totalWSConn++;
        infos.totalWSConn++;
      }
    } else {
      CLIENTSTAT cs;
      cs.ip = connection.ip;
      cs.name = connection.name;
      cs.totalConn = 1;
      cs.totalWSConn = 0;
      cs.bytesRecv = connection.conn->getNbRecv();
      cs.bytesSent = connection.conn->getNbSent();
      if(connection.conn->isWS()) {
        cs.totalWSConn++;
        infos.totalWSConn++;
      }
      infos.clients.push_back(cs);
    }
  }

  pthread_mutex_unlock(&command_mutex);

}

void ConnectionManager::getSocketInfos(std::vector<SOCKETINFO>& infos) {

  pthread_mutex_lock(&command_mutex);

  infos.clear();

  for (auto &connection : connections) {
    SOCKETINFO sinfo;
    sinfo.ip = connection.ip;
    sinfo.socket = connection.conn->getSock();
    sinfo.nbCall = connection.conn->getNbCall();
    sinfo.status = connection.conn->getStatus();
    sinfo.isWS = connection.conn->isWS();
    infos.push_back(sinfo);
  }

  pthread_mutex_unlock(&command_mutex);

}

// ----------------------------------------------------------------------------------------
// Start a new connection
// ----------------------------------------------------------------------------------------
void ConnectionManager::start(CLIENTINFO& client) {

  pthread_mutex_lock(&command_mutex);
  COMMAND cmd;
  cmd.command = START_CMD;
  cmd.client = client;
  commandToExecute.push_back(cmd);
  pthread_cond_signal(&command_cond);
  pthread_mutex_unlock(&command_mutex);

}

// ----------------------------------------------------------------------------------------
// Abort all pending connection and exit
// ----------------------------------------------------------------------------------------
void ConnectionManager::stop() {

  pthread_mutex_lock(&command_mutex);
  COMMAND cmd;
  cmd.command = EXIT_CMD;
  commandToExecute.push_back(cmd);
  pthread_cond_signal(&command_cond);
  pthread_mutex_unlock(&command_mutex);

}

// ----------------------------------------------------------------------------------------
// Wait for end of a connection and remove it from the list
// ----------------------------------------------------------------------------------------
void ConnectionManager::end(Connection *conn) {

  pthread_mutex_lock(&command_mutex);
  COMMAND cmd;
  cmd.command = END_CMD;
  cmd.connToEnd = conn;
  commandToExecute.push_back(cmd);
  pthread_cond_signal(&command_cond);
  pthread_mutex_unlock(&command_mutex);

}


} // namespace TangoGQL_ns
