// -------------------------------------------------------------------------------
// Connection  class
// -------------------------------------------------------------------------------
#include "Connection.h"
#include "Request.h"
#include <sys/socket.h>
#include <unistd.h>
#include "WSFrame.h"
#include "WSKeepAlive.h"
#include "StringUtils.h"
#include "fs.h" // Embedded file system

#define PROCESSING     0
#define WAIT_FOR_READ  1
#define WAIT_FOR_WRITE 2
#define CLOSE_WAIT     3

std::string CONN_STATUS[] = {
    "PROCESSING",
    "WAIT_FOR_READ",
    "WAIT_FOR_WRITE",
    "CLOSE_WAIT"
};

namespace TangoGQL_ns {


// Close the connection
#define CLOSE() {Close();return NULL;}

// ----------------------------------------------------------------------------------------
// Constructor
// ----------------------------------------------------------------------------------------
Connection::Connection(TangoGQL *tangoGql, ConnectionManager *c, int s,int t,std::string cI) :
        Tango::LogAdapter(tangoGql),ds(tangoGql), parent(c), socket(s), idleTimeout(t), clientInfo(cI) {

  signalEnd = true;
  factory = new DeviceFactory(tangoGql,this);
  gqlEngine = new GQLEngine(tangoGql,factory);
  isWebSocket = false;
  aborted = false;
  signalWSEnd = true;
  keepAlive = nullptr;
  bytesRecv = 0;
  bytesSent = 0;
  nbCall = 0;
  connStatus = PROCESSING;

  if( ds->httpsEnable ) {
    ssl = SSL_new(ds->ctx);
    SSL_set_fd(ssl, socket);
    // SSL handshake
    if (SSL_accept(ssl) <= 0)
      // If the handshake failed, the first read (or write) connection will fail
      lastError = "SSL handshake failed";
  }

  start_undetached();

}

// ----------------------------------------------------------------------------------------
// Conection destructor
// ----------------------------------------------------------------------------------------
Connection::~Connection() {
  INFO_STREAM << "Connection::~Connection("<<socket<<") free resources" << std::endl;
  delete gqlEngine;
  delete factory;
}

// ----------------------------------------------------------------------------------------
// Close the connection
// ----------------------------------------------------------------------------------------
void Connection::Close() {

  INFO_STREAM << "Connection::close("<<socket<<") Close " << (isWebSocket?"WebSocket":"normal") <<
              " connection with " << clientInfo << " (" << lastError << ")" << std::endl;

  connStatus = CLOSE_WAIT;
  aborted = true;
  if (signalEnd) parent->end(this);
  if (isWebSocket && UUID.length()>0) gqlEngine->unregisterSubscription(this,signalWSEnd);
  if (keepAlive) keepAlive->join(NULL);
  if(ds->httpsEnable) {
    SSL_shutdown(ssl);
    SSL_free(ssl);
  }
  close(socket);

}

// ----------------------------------------------------------------------------------------

bool Connection::isWS() {
  return isWebSocket;
}

// ----------------------------------------------------------------------------------------

int Connection::getSock() const {
  return socket;
}

// ----------------------------------------------------------------------------------------

uint64_t Connection::getNbSent() {
  return bytesSent;
}

// ----------------------------------------------------------------------------------------

uint64_t Connection::getNbRecv() {
  return bytesRecv;
}

// ----------------------------------------------------------------------------------------

uint64_t Connection::getNbCall() {
  return nbCall;
}

// ----------------------------------------------------------------------------------------

std::string& Connection::getStatus() {
  return CONN_STATUS[connStatus];
}


// ----------------------------------------------------------------------------------------
// Main HTTP loop
// ----------------------------------------------------------------------------------------
void *Connection::run_undetached(void *arg) {

  INFO_STREAM <<  "Connection::run("<<socket<<") Start connection thread with " << clientInfo << std::endl;

  char firstByte;
  Request req;
  time_t t0;
  time_t t1;

  while(true) {

    // Wait for request first byte
    req.clear();
    connStatus = WAIT_FOR_READ;
    int nbRead = _Read(socket, &firstByte, 1, idleTimeout*1000);
    connStatus = PROCESSING;
    if (nbRead <= 0) CLOSE();
    bytesRecv += nbRead;
    nbCall++;

    //INFO_STREAM << "Connection:run() ------------> Get First Byte: " << toHex(firstByte) << " from " << clientInfo << std::endl;

    t0 = GetTicks();

    // Possible HTTP request:  GET, HEAD, POST, PUT, DELETE, CONNECT, OPTIONS, TRACE
    if( firstByte=='G' || firstByte=='H' || firstByte=='P' || firstByte=='D' ||
        firstByte=='C' || firstByte=='O' || firstByte=='T' ) {

      req.addChar(firstByte);

      // Wait for request to complete
      char c;
      while (!req.isComplete()) {

        try {
          connStatus = WAIT_FOR_READ;
          nbRead = _Read(socket, &c, 1, idleTimeout * 1000);
          connStatus = PROCESSING;
          if (nbRead <= 0) CLOSE();
          bytesRecv += nbRead;
          req.addChar(c);
        } catch (std::string& err) {
          lastError = err;
          CLOSE();
        }

      }


      t1 = GetTicks();
      INFO_STREAM << "Connection::run("<<socket<<") HTTP Request " << (double)(t1 - t0) / 10.0 << " ms, " << req.toString(true) << std::endl;

      bool auth;

      if(req.getType()=="GET") {

        if(!HandleAuth(req,auth)) CLOSE();
        if(auth && !HandleGet(req)) CLOSE();

      } else if (req.getType()=="POST") {

        if(!HandleAuth(req,auth)) CLOSE();
        if(!auth) {
          // Flush the post content
          lastError = "Post not authenticated";
          CLOSE();
        } else {
           if (!HandlePost(req)) CLOSE();
        }

      } else if (req.getType()=="OPTIONS") {

        if(!HandleOption(req)) CLOSE();

      } else {

        Request rep;
        rep.createBadResponse("Unsupported request " + req.getType());
        usleep(1000);
        int nbWrite = Write(rep,WRITE_TIMEOUT);
        if(nbWrite<=0) CLOSE();

      }

    } else {

      if(!HandleWebSocket(firstByte)) CLOSE();

    }

  }

}

// -------------------------------------------------------------------------------------------------------
// HTTP Get request
// -------------------------------------------------------------------------------------------------------
bool Connection::HandleGet(Request& req) {

  std::string url = req.getURL();
  if(ends_with(url,"/"))
    url.append("index.html");

  if(url=="/graphql-ws") {

    std::string key;
    bool hasKey = req.getHeader("Sec-WebSocket-Key",key);
    if(hasKey) {
      // WebSocket Handshake
      Request rep;
      rep.createWebsocketHandshakeResponse(key);
      int nbWrite = Write(rep, WRITE_TIMEOUT);
      if (nbWrite <= 0) return false;
      bytesSent += nbWrite;
    } else {
      lastError = "Sec-WebSocket-Key not found";
      return false;
    }

  } else if (url=="/hello") {

    Request rep;
    rep.createHelloResponse();
    int nbWrite = Write(rep, WRITE_TIMEOUT);
    if (nbWrite <= 0) return false;
    bytesSent += nbWrite;

  } else if (url=="/schema.graphql" ) {

    Request rep;
    std::string gqlSchema = gqlEngine->getSchemaAsString();
    rep.createArbResponse((void *)gqlSchema.data(),gqlSchema.length(),"application/octet-stream");
    int nbWrite = Write(rep, WRITE_TIMEOUT);
    if (nbWrite <= 0) return false;
    bytesSent += nbWrite;

  } else {

    // Search in the embedded file system
    bool found = false;
    int i = 0;
    while(!found && i<FILE_SYSTEM_NB) {
      found = url==_file_system[i].fileName;
      if(!found) i++;
    }

    if( found && ds->exportGraphiQL ) {

      Request rep;
      std::string mimeType;
      Request::getContentType(url,mimeType);
      rep.createArbResponse((void *)_file_system[i].data,_file_system[i].size,mimeType);
      int nbWrite = Write(rep, WRITE_TIMEOUT);
      if (nbWrite <= 0) return false;
      bytesSent += nbWrite;

    } else {

      Request rep;
      rep.createBadResponse("Error 404: Not found");
      int nbWrite = Write(rep, WRITE_TIMEOUT);
      if (nbWrite <= 0) return false;
      bytesSent += nbWrite;

    }

  }

  return true;

}

// -------------------------------------------------------------------------------------------------------
// HTTP POST request
// -------------------------------------------------------------------------------------------------------
bool Connection::HandlePost(Request& req) {

  // Read the post content
  time_t t0 = GetTicks();
  int toRead = req.getContentLength();
  if(toRead<=0) {
    lastError = "POST Content-Length header not found or invalid in POST request";
    return false;
  }
  if(toRead>MAX_POST_CONTENT) {
    lastError = "POST Content-Length too big";
    return false;
  }

  char *buffer = (char *)malloc(toRead+1);
  connStatus = WAIT_FOR_READ;
  int nbRead = _Read(socket, buffer, toRead, 3000);
  connStatus = PROCESSING;
  if (nbRead != toRead) {
    free(buffer);
    lastError = "POST Content-Length wrong size";
    return false;
  }
  bytesRecv += nbRead;
  buffer[toRead]=0;
  std::string content(buffer);
  free(buffer);
  time_t t1 = GetTicks();

  INFO_STREAM << "Connection::HandlePost(" << socket << ") POST CONTENT: " << content.length() << " bytes , "
              << (double) (t1 - t0) / 10.0 << " ms , " /*<< content*/ << std::endl;

  if(req.getURL()=="/graphql") {

    std::string result;
    gqlEngine->execute(content, result, ds->allowMutation, ds->allowMultiTangoHost);
    Request rep;
    rep.createJSONResponse(result);
    int nbWrite = Write(rep, WRITE_TIMEOUT);
    if (nbWrite > 0) bytesSent += nbWrite;
    return (nbWrite > 0);

  } else {

    Request rep;
    rep.createBadResponse("Error 404: Not found");
    int nbWrite = Write(rep, WRITE_TIMEOUT);
    if (nbWrite > 0) bytesSent += nbWrite;
    if (nbWrite <= 0) return false;

  }

  return true;

}

// -------------------------------------------------------------------------------------------------------
// Authentication stuff
// -------------------------------------------------------------------------------------------------------

bool Connection::HandleAuth(Request &req,bool& authenticationOK) {

  authenticationOK = false;

  if( ds->hasAuthentication ) {

    std::string auth ;
    if( !req.getHeader("Authorization",auth) ) {
      Request rep;
      rep.createUnauthorizedResponse("Not authorized");
      usleep(1000);
      int nbWrite = Write(rep,WRITE_TIMEOUT);
      if (nbWrite > 0) bytesSent += nbWrite;
      return (nbWrite>0);
    }

    std::vector<std::string> fields;
    split(fields,auth,' ');
    if(fields.size()!=2 || fields[0] != "Basic") {
      Request rep;
      rep.createBadResponse("Invalid Authorization");
      usleep(1000);
      int nbWrite = Write(rep,WRITE_TIMEOUT);
      if (nbWrite > 0) bytesSent += nbWrite;
      return (nbWrite>0);
    }

    std::string userPass = Encoder::base64_decode(fields[1]);
    split(fields,userPass,':');

    if(!ds->checkUser(fields[0],fields[1])) {
      Request rep;
      rep.createForbiddenResponse();
      usleep(1000);
      int nbWrite = Write(rep,WRITE_TIMEOUT);
      if (nbWrite > 0) bytesSent += nbWrite;
      return (nbWrite>0);
    }

  }

  authenticationOK = true;
  return true;

}

// -------------------------------------------------------------------------------------------------------
// HTTP OPTION request
// -------------------------------------------------------------------------------------------------------
bool Connection::HandleOption(Request &req) {

  Request rep;
  rep.createOptionResponse("POST",ds->hasAuthentication);
  INFO_STREAM << "Connection::HandleOption(" <<socket<< ") " << rep.toString(true) << std::endl;
  usleep(1000);
  int nbWrite = Write(rep,WRITE_TIMEOUT);
  if (nbWrite > 0) bytesSent += nbWrite;
  return (nbWrite>0);

}

// -------------------------------------------------------------------------------------------------------
// HTTP WebSocket
// -------------------------------------------------------------------------------------------------------

bool Connection::HandleWebSocket(char firstByte) {

  // WebSocket frame (GraphQL ws protocol)
  isWebSocket = true;
  WSFrame frame;
  JSON json;
  if(!DecodeWSFrame(frame,firstByte))
    return false;

  INFO_STREAM << "Connection::run("<<socket<<") WebSocket request " << frame.toString() << std::endl;

  switch( frame.getOpCode() ) {

    case OPCODE_TEXT: {

      try {
        GQLEngine::parseJSON(frame.getPayLoad(), json);
      } catch (std::string& err) {
        INFO_STREAM << "Connection::HandleWebSocket("<<socket<<") Invalid frame: " << frame.toString() << std::endl;
        lastError = "WebSocket: " + err;
        return false;
      }

      if (json.find("type") == json.end()) {
        lastError = "WebSocket: type not found";
        return false;
      }

      if (json["type"].items[0] == "connection_init") {

        // Send back connection_ack
        WSFrame rep;
        rep.TextFrame(R"({"type":"connection_ack"})");
        int nbWrite = Write(rep, WRITE_TIMEOUT);
        if (nbWrite <= 0) return false;

      } else if (json["type"].items[0] == "subscribe") {

        if (!UUID.empty() && json["id"].items[0] != UUID) {
          WSFrame rep;
          rep.CloseFrame(4409,"Subscriber for UUID already exists");
          ERROR_STREAM << "Connection:run() Invalid subscription for " + json["id"].items[0] << std::endl;
          Write(rep, WRITE_TIMEOUT);
          return false;
        }

        UUID = json["id"].items[0];

        time_t t0 = GetTicks();
        std::string queryResult;
        if (!gqlEngine->executeSubscription(json["payload"].items[0], queryResult)) {

          std::string result;
          result.append(R"({"id":")" + json["id"].items[0] + "\",");
          result.append(R"("type":"error",)");
          result.append("\"payload\":");
          result.append(queryResult);
          result.append("}");
          WSFrame rep;
          rep.TextFrame(result);
          int nbWrite = Write(rep, WRITE_TIMEOUT);
          if (nbWrite <= 0) return false;

        } else {

          // Successful subscription
          time_t t1 = GetTicks();
          INFO_STREAM << "Connection::run("<<socket<<") subscription " << (double)(t1 - t0) / 10.0 << " ms" << std::endl;
          // Launch a WSKeepAlice thread
          keepAlive = new WSKeepAlive(ds, this, 10000);

        }

      } else if (json["type"].items[0] == "complete") {

        // Unregister (should be followed by a OP_CLOSE (1000 Normal closure)
        gqlEngine->unregisterSubscription(this,false);

      }

    } break;

    case OPCODE_CLOSE: {
      if(frame.getCloseCode()!=1000) {
        // Abnormal termination
        ERROR_STREAM << "Connection::run("<<socket<<") Abnormal WebSocket termination, " << frame.toString() << std::endl;
      }
      usleep(10000);
      return false;
    }

    case OPCODE_PONG: {
    } break;

    default:
      lastError = "WebSocket: Unexpected response or not implemented";
      INFO_STREAM << frame.toString() << std::endl;
      return false;

  }

  return true;

}


std::string Connection::getUUID() {
  return UUID;
}

void Connection::sendComplete() {

  std::string message = R"({"id":")" + UUID + R"(","type":"complete"})";
  WSFrame rep;
  rep.TextFrame(message);
  Write(rep, WRITE_TIMEOUT);

}

// ----------------------------------------------------------------------------------------
// Decode WebSocket frame
// ----------------------------------------------------------------------------------------

int Connection::DecodeWSFrame(WSFrame &frame,char firstByte) {

  char c2;
  connStatus = WAIT_FOR_READ;
  int nbRead = _Read(socket, &c2, 1, idleTimeout * 1000);
  connStatus = PROCESSING;
  if (nbRead <= 0) {
    lastError = "Incomplete WS frame";
    return 0;
  }
  bytesRecv += nbRead;

  int toRead = frame.addHeader(firstByte,c2);
  if(toRead>0) {
    char *buffer = (char *) malloc(toRead);
    connStatus = WAIT_FOR_READ;
    nbRead = _Read(socket, buffer, toRead, idleTimeout * 1000);
    connStatus = PROCESSING;
    if (nbRead != toRead) {
      free(buffer);
      lastError = "Incomplete WS frame";
      return 0;
    }
    toRead = frame.finalizeHeader(buffer,toRead);
    free(buffer);
  }
  bytesRecv += nbRead;

  if(toRead>MAX_WS_FRAME) {
    lastError = "WS frame too big";
    return 0;
  }

  if(toRead>0) {
    char *buffer = (char *) malloc(toRead);
    connStatus = WAIT_FOR_READ;
    nbRead = _Read(socket, buffer, toRead, idleTimeout * 1000);
    connStatus = PROCESSING;
    if (nbRead != toRead) {
      free(buffer);
      lastError = "Incomplete WS frame";
      return 0;
    }
    frame.addPayload(buffer,toRead);
    free(buffer);
  }
  bytesRecv += nbRead;

  return 1;

}

// ----------------------------------------------------------------------------------------
// Abort connection
// ----------------------------------------------------------------------------------------
void Connection::Abort(bool _signalEnd,bool _signalWSEnd) {
  signalEnd = _signalEnd;
  signalWSEnd = _signalWSEnd;
  aborted = true;
  shutdown(socket,SHUT_RDWR);
}

bool Connection::isAborted() const {
  return aborted;
}

// ----------------------------------------------------------------------------------------
// Low level socket op
// ----------------------------------------------------------------------------------------

int Connection::_WaitFor(int sock,int timeout,int mode) {

  fd_set fdset;
  fd_set *rd = nullptr,*wr = nullptr;
  struct timeval tmout{};
  int result;

  FD_ZERO(&fdset);
  FD_SET(sock,&fdset);
  if(mode == WAIT_FOR_READ)
    rd = &fdset;
  if(mode == WAIT_FOR_WRITE)
    wr = &fdset;

  tmout.tv_sec = (int)(timeout / 1000);
  tmout.tv_usec = (int)(timeout % 1000) * 1000;

  do
    result = select((int)sock + 1,rd,wr,nullptr,&tmout);
#ifdef WIN64
  while(result < 0 && WSAGetLastError() == WSAEINTR);
#else
  while(result < 0 && errno == EINTR);
#endif

  if(result == 0) {
    lastError = "The operation timed out";
  } else if(result < 0) {
    lastError = std::string(strerror(errno));
    return 0;
  }

  return result;

}

int Connection::Write(Request &request,int timeout) {
  std::string buff;
  time_t t0 = GetTicks();
  request.to_buffer(buff);
  connStatus = WAIT_FOR_WRITE;
  int status =  _Write(socket,(char *)buff.c_str(),buff.length(),timeout);
  connStatus = PROCESSING;
  time_t t1 = GetTicks();
  INFO_STREAM << "Connection::Write("<<socket<<") HTTP Response :" << (double)(t1 - t0) / 10.0 << " ms, " << request.toString(false) << std::endl;
  return status;
}

int Connection::Write(WSFrame &frame,int timeout) {
  wsWriteMutex.lock();
  INFO_STREAM << "Connection::Write("<<socket<<") Send " << frame.toString() << std::endl;
  std::string buff;
  frame.to_buffer(buff);
  connStatus = WAIT_FOR_WRITE;
  int nbWrite = _Write(socket,(char *)buff.c_str(),buff.length(),timeout);
  connStatus = PROCESSING;
  if (nbWrite > 0) bytesSent += nbWrite;
  wsWriteMutex.unlock();
  nbCall++;
  return nbWrite;
}

int Connection::_Write(int sock,char *buf,int bufsize,int timeout) {

  if(ds->httpsEnable) {
    int sslStatus = SSL_write(ssl, (void *) buf, bufsize);
    if(sslStatus<=0) {
      if(lastError.empty())
        lastError = "SSL_write error code: " + std::to_string(sslStatus);
    }
    return sslStatus;
  }

  int total_written = 0;
  int written = 0;

  while(bufsize > 0)
  {
    // Wait
    if(!_WaitFor(sock,timeout,WAIT_FOR_WRITE))
      return -1;

    // Write
    do
      written = send(sock,buf,bufsize,0);
#ifdef WIN64
    while(written == -1 && WSAGetLastError() == WSAEINTR);
#else
    while(written == -1 && errno == EINTR);
#endif

    if(written <= 0)
      break;

    buf += written;
    total_written += written;
    bufsize -= written;
  }

  if(written < 0) {
    lastError = std::string(strerror(errno));
    return -1;
  }

  if(bufsize != 0) {
    lastError = "Failed to send entire buffer";
    return -1;
  }

  return total_written;

}

int Connection::_Read(int sock,char *buf,int bufsize,int timeout) { // Timeout in millisec

  if(ds->httpsEnable) {
    int sslStatus = SSL_read(ssl, (void *) buf, bufsize);
    if(sslStatus<=0) {
      if(lastError.empty())
        lastError = "SSL_Read error code " + std::to_string(sslStatus);
    }
    return sslStatus;
  }

  int rd = 0;
  int total_read = 0;

  while( bufsize>0 ) {

    // Wait
    if(!_WaitFor(sock,timeout,WAIT_FOR_READ)) {
      return -1;
    }

    // Read
    do
      rd = recv(sock,buf,bufsize,0);
#ifdef WIN64
    while(rd == -1 && WSAGetLastError() == WSAEINTR);
#else
    while(rd == -1 && errno == EINTR);
#endif
    if( rd <= 0 )
      break;

    buf += rd;
    total_read += rd;
    bufsize -= rd;

  }

  if(rd < 0) {
    lastError = std::string(strerror(errno));
    return -1;
  }

  if(rd == 0) {
    lastError = "Connection closed";
    return -1;
  }

  return total_read;

}
} // namespace TangoGQL_ns
