//
// String utils
//

#ifndef TANGOGQL_STRINGUTILS_H
#define TANGOGQL_STRINGUTILS_H

#include <algorithm>
#include <string>
#include <vector>

inline bool ends_with(std::string const & value, std::string const & ending) {
  if (ending.size() > value.size()) return false;
  return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}

inline void split(std::vector<std::string> &tokens, const std::string &text, char sep) {

  size_t start = 0, end = 0;
  tokens.clear();

  while ((end = text.find(sep, start)) != std::string::npos) {
    tokens.push_back(text.substr(start, end - start));
    start = end + 1;
  }

  tokens.push_back(text.substr(start));

}

inline void ltrim(std::string &s) {
  s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
    return !isspace(ch);
  }));
}

inline void rtrim(std::string &s) {
  s.erase(find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
    return !isspace(ch);
  }).base(), s.end());
}

inline void trim(std::string &s) {
  ltrim(s);
  rtrim(s);
}

inline std::string toHex(char c) {
  char tmp[32];
  uint32_t C32 = ((uint32_t)c) & 0xFF;
  ::sprintf(tmp,"'%c' 0x%02X",(c>=32)?c:'.',C32);
  return std::string(tmp);
}

#endif //TANGOGQL_STRINGUTILS_H
