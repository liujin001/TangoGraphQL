//
// Device Factory
//

#ifndef TANGOGQL_DEVICEFACTORY_H
#define TANGOGQL_DEVICEFACTORY_H

namespace TangoGQL_ns {

class Connection;

class Device: public Tango::LogAdapter {

public:
  Device(TangoGQL *,  Connection *, Tango::DeviceProxy *);
  ~Device();

  /** Get DeviceProxy */
  Tango::DeviceProxy *ds();

  /** Get AttributeProperties, throw DevFailed in case of failure */
  void getAttributeProperties(std::vector<std::string>& attNames,std::vector<Tango::AttributeInfoEx>& props);

  /** Get CommandInfo, throw DevFailed in case of failure */
  void getCommandInfo(std::vector<std::string>& cmdNames,std::vector<Tango::CommandInfo>& infos);

  /** Get AttributeProperties, throw DevFailed in case of failure */
  void getAttributeProperties(std::string& attName,Tango::AttributeInfoEx &props);

  /** Get command info, throw DevFailed in case of failure */
  void getCommandInfo(std::string& cmdName,Tango::CommandInfo &infos);

  /** Get AttributeProperties of all attributes, throw DevFailed in case of failure */
  void getAllAttributeProperties(std::vector<Tango::AttributeInfoEx>& props);

  /** Get command info of all commands, throw DevFailed in case of failure */
  void getAllCommandInfo(std::vector<Tango::CommandInfo>& info);

  /** Get attribute list, throw DevFailed in case of failure */
  void getAttributeList(std::vector<std::string>& list);

  /** Get command list, throw DevFailed in case of failure */
  void getCommandList(std::vector<std::string>& list);

  /** Force attribute config reset */
  void resetAttConfig();

private:

  void updateProps();
  void updateCmdInfos();
  size_t getAttIdx(std::string &attName);
  size_t getCmdIdx(std::string &cmdName);
  Tango::DeviceProxy *_ds;
  std::vector<Tango::AttributeInfoEx> *attProps;
  std::vector<Tango::CommandInfo> *cmdInfos;
  Connection *conn;

};

class DeviceFactory: public Tango::LogAdapter {

public:

  DeviceFactory(TangoGQL *,Connection *conn);
  ~DeviceFactory();

  /** Get Device, throw DevFailed in case of failure */
  Device *get(std::string &devName,bool allowMultiTangoHost);

  /** Returns the connection attached to this factory */
  Connection *getConn();

  /** dump the device names */
  void dump(std::string& result);

private:

  void reset();
  bool find(std::string& devname,int *pos);
  int getIdx(std::string &devName);

  std::vector<Device *> devices;
  Connection *conn;
  TangoGQL *tangoGql;

};

} // namespace TangoGQL_ns

#endif //TANGOGQL_DEVICEFACTORY_H
