//
// Device factory
//
#include <TangoGQL.h>
#include <iostream>
#include <algorithm>
#include <vector>


namespace TangoGQL_ns {

const char * qualities[] = { "ATTR_VALID", "ATTR_INVALID", "ATTR_ALARM", "ATTR_CHANGING", "ATTR_WARNING" };
const char * formats[] = { "SCALAR", "SPECTRUM", "IMAGE", "FMT_UNKNOWN" };
const char * severities[] = { "WARN", "ERR", "PANIC" };
const char * writables[] = { "READ", "READ_WITH_WRITE", "WRITE", "READ_WRITE" };
const char * displevels[] = { "OPERATOR", "EXPERT" };

// -------------------------------------------------------------------------------------
// Device
// -------------------------------------------------------------------------------------
Device::Device(TangoGQL *tangoQGL,Connection *conn,Tango::DeviceProxy *ds): Tango::LogAdapter(tangoQGL),_ds(ds),conn(conn) {
  attProps = nullptr;
  cmdInfos = nullptr;
}

Device::~Device() {
  INFO_STREAM << "Device::~Device("<<conn->getSock()<<") free resources " << _ds->dev_name() << std::endl;
  delete _ds;
  delete attProps;
  delete cmdInfos;
}

void Device::resetAttConfig() {
  delete attProps;
  attProps = nullptr;
}

Tango::DeviceProxy *Device::ds() {
  return _ds;
}

size_t Device::getAttIdx(std::string &attName) {

  bool found = false;
  size_t i = 0;
  while(!found && i < attProps->size()) {
    found = ::strcasecmp(attName.c_str(),(*attProps)[i].name.c_str()) == 0;
    if(!found) i++;
  }
  if(!found)
    Tango::Except::throw_exception(
            "AttrNotFound",
            attName + " not found",
            "DeviceFactory::getAttIdx"
    );
  return i;

}

size_t Device::getCmdIdx(std::string &cmdName) {

  bool found = false;
  size_t i = 0;
  while(!found && i<cmdInfos->size()) {
    found = ::strcasecmp(cmdName.c_str(),(*cmdInfos)[i].cmd_name.c_str())==0;
    if(!found) i++;
  }
  if(!found)
    Tango::Except::throw_exception(
            "CommandNotFound",
            cmdName + " not found",
            "DeviceFactory::getCmdIdx"
    );
  return i;

}

void Device::getAttributeList(std::vector<std::string>& list) {

  updateProps();
  for(auto & attProp : *attProps)
    list.push_back(attProp.name);

}

void Device::getCommandList(std::vector<std::string>& list) {

  updateCmdInfos();
  for(auto & cmdInfo : *cmdInfos)
    list.push_back(cmdInfo.cmd_name);

}

void Device::getAttributeProperties(std::vector<std::string>& attNames,std::vector<Tango::AttributeInfoEx>& props) {

  updateProps();
  for(auto & attName : attNames)
    props.push_back((*(this->attProps))[getAttIdx(attName)]);

}

void Device::getCommandInfo(std::vector<std::string>& cmdNames,std::vector<Tango::CommandInfo>& infos) {

  updateCmdInfos();
  for(auto & cmdName : cmdNames)
    infos.push_back((*(this->cmdInfos))[getCmdIdx(cmdName)]);

}

void Device::getAttributeProperties(std::string& attName,Tango::AttributeInfoEx &props) {

  updateProps();
  props = ((*(this->attProps))[getAttIdx(attName)]);

}


void Device::getCommandInfo(std::string& cmdName,Tango::CommandInfo &infos) {

  updateCmdInfos();
  infos = ((*(this->cmdInfos))[getCmdIdx(cmdName)]);

}

void Device::getAllAttributeProperties(std::vector<Tango::AttributeInfoEx>& props) {

  updateProps();
  for(auto & attProp : *this->attProps)
    props.push_back(attProp);

}

void Device::getAllCommandInfo(std::vector<Tango::CommandInfo>& infos) {

  updateCmdInfos();
  for(auto & cmdInfo : *this->cmdInfos)
    infos.push_back(cmdInfo);

}

void Device::updateProps() {

  if(attProps == nullptr) {
    std::vector<std::string> allNames;
    allNames.push_back(Tango::AllAttr);
    INFO_STREAM << "Device::updateProps("<<conn->getSock()<<") GetAttributeProperties " << _ds->dev_name() << std::endl;
    attProps = _ds->get_attribute_config_ex(allNames);
  }

}

void Device::updateCmdInfos() {

  if(cmdInfos == nullptr) {
    std::vector<std::string> allCmd;
    allCmd.push_back(Tango::AllCmd);
    INFO_STREAM << "Device::updateProps(" << conn->getSock() << ") GetCommandInfo " << _ds->dev_name() << std::endl;
    cmdInfos = _ds->get_command_config(allCmd);
  }

}

// -------------------------------------------------------------------------------------
// DeviceFactory
// -------------------------------------------------------------------------------------


DeviceFactory::DeviceFactory(TangoGQL *tangoGql,Connection *conn):Tango::LogAdapter(tangoGql),tangoGql(tangoGql),conn(conn) {
}
DeviceFactory::~DeviceFactory() {
  reset();
}

void DeviceFactory::reset() {
  for(auto & device : devices)
    delete device;
  devices.clear();
}

Connection *DeviceFactory::getConn() {
  return conn;
}

bool DeviceFactory::find(std::string& devname,int *pos) {

  int st, ed, mi;
  st=0; ed = (int)devices.size()-1;
  while(st<=ed) {
    mi = (st+ed)/2;
    int comp =  devname.compare(devices[mi]->ds()->dev_name());
    if(comp<0) {
      ed = mi - 1;
    } else if(comp==0) {
      *pos = mi;
      return true;
    } else {
      st = mi + 1;
    }
  }

  *pos = st;
  return false;

}

void DeviceFactory::dump(std::string& result) {
  result.push_back('[');
  for(size_t i=0;i<devices.size();i++) {
    result.push_back('\"');
    result.append(devices[i]->ds()->dev_name());
    result.push_back('\"');
    if(i<devices.size()-1) result.push_back(',');
  }
  result.push_back(']');
}

int DeviceFactory::getIdx(std::string &devName) {

  std::string devname = devName;
  transform(devname.begin(), devname.end(), devname.begin(),
            [](unsigned char c){ return std::tolower(c); });

  int pos;
  if( !find(devname,&pos) ) {

    Tango::DeviceProxy *ds;
    try {
      ds = new Tango::DeviceProxy(devname);
    } catch (Tango::DevFailed &e) {
      throw e;
    }

    INFO_STREAM << "DeviceFactory::getIdx("<<conn->getSock()<<") Import device " << devname << std::endl;
    Device *d = new Device(tangoGql,conn,ds);
    devices.insert(devices.begin()+pos,d);

  }

  return pos;

}

Device *DeviceFactory::get(std::string &devName,bool allowMultiTangoHost) {

  if(!allowMultiTangoHost) {
    // check that device name does not use multi tango host
    // Allow only domain/family/member syntax
    if( std::count(devName.begin(), devName.end(), '/')!=2 ||
        std::count(devName.begin(), devName.end(), ':')>=1 ) {
      Tango::Except::throw_exception(
              "DeviceNotAllowed",
              devName + " not allowed syntax, domain/family/member only",
              "GQLEvalDevice");
    }
  }

  return devices[getIdx(devName)];

}




} // namespace TangoGQL_ns