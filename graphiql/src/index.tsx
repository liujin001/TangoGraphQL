import { render } from "react-dom";
import { GraphiQL } from "graphiql";
import { createGraphiQLFetcher } from "@graphiql/toolkit";
import "graphiql/graphiql.min.css";

import "./index.css";

let isSecure = document.location.protocol === "https:"
let wsProtocol = (isSecure)?"wss://":"ws://";
let httpProtocol = (isSecure)?"https://":"http://";

// GraphiQL is served by the same webserver than GraphQL
let URL = httpProtocol+document.location.host+"/graphql";
let URLws = wsProtocol+document.location.host+"/graphql-ws";
if(window.location.port=="3000") {
  // GraphiQL runs in the development server, overide port
  URL = httpProtocol+document.location.hostname+":8000/graphql";
  URLws = wsProtocol+document.location.hostname+":8000/graphql-ws";
}

// Customize fetcher to pass credential when auth is used
let myfetcher:typeof fetch = (url,init) => {
  return fetch(url,{
    ...init,
    credentials: 'same-origin'})
};

var fetcher = createGraphiQLFetcher( {url:URL,subscriptionUrl:URLws,fetch:myfetcher} );

const container = document.getElementById("graphiql");

const defaultQuery = `
{
  database {
    info
  }
}
`

render(
  <GraphiQL fetcher={fetcher} defaultQuery={defaultQuery} />,
  container
);
