# GraphiQL for TangoGraphQL

To build and embed the files into the TangoGraphQL server, you need to:
(You may need to edit package.json to change path to AppBuilder)

Make AppBuilder in the TangoGraphQL directory
```
make AppBuilder
```
Go to the graphiql directory and install dependencies in nodes_modules.
```
npm i
```
Build GraphiQL
```
npm run build
```
Genrates the fs.h and fs.cpp files which contains the app
```
npm run build-src
```
Build the TangoGraphQL server
