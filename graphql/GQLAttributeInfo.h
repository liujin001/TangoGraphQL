//
// GQLAttributeInfo eval functions
//

#ifndef TANGOGQL_GQLATTRIBUTEINFO_H
#define TANGOGQL_GQLATTRIBUTEINFO_H

#include "TangoGQL.h"
#include "GQLDevice.h"


namespace TangoGQL_ns {

extern const char * formats[];
extern const char * writables[];
extern const char * displevels[];

class GQLEngine;

class GQLAttributeInfo : public GQLBase {

public:

  GQLAttributeInfo(GQLEngine *root,Device *dev,const Tango::AttributeInfoEx& p,const std::string& type):
    GQLBase(root),dev(dev),props(p),type(type) {}

  Device                       *dev;
  const Tango::AttributeInfoEx& props;
  const std::string&            type;

  std::string __typename() {
    return type;
  }

};

BEGIN_EVAL(GQLEvalAttributeInfoName)
    GQLAttributeInfo *p = (GQLAttributeInfo *)node;
    GQLUtils::quote(p->props.name,evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalAttributeInfoWritable)
    GQLAttributeInfo *p = (GQLAttributeInfo *)node;
    GQLUtils::quote(writables[p->props.writable],evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalAttributeInfoDataFormat)
    GQLAttributeInfo *p = (GQLAttributeInfo *)node;
    GQLUtils::quote(formats[p->props.data_format], evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalAttributeInfoDataType)
    GQLAttributeInfo *p = (GQLAttributeInfo *)node;
    GQLUtils::quote(std::string(Tango::CmdArgTypeName[p->props.data_type]), evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalAttributeInfoMaxDimX)
    GQLAttributeInfo *p = (GQLAttributeInfo *)node;
    evalStr.append(std::to_string(p->props.max_dim_x));
END_EVAL

BEGIN_EVAL(GQLEvalAttributeInfoMaxDimY)
    GQLAttributeInfo *p = (GQLAttributeInfo *)node;
    evalStr.append(std::to_string(p->props.max_dim_y));
END_EVAL

BEGIN_EVAL(GQLEvalAttributeInfoDescription)
    GQLAttributeInfo *p = (GQLAttributeInfo *)node;
    GQLUtils::quote(p->props.description,evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalAttributeInfoLabel)
    GQLAttributeInfo *p = (GQLAttributeInfo *)node;
    GQLUtils::quote(p->props.label,evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalAttributeInfoUnit)
    GQLAttributeInfo *p = (GQLAttributeInfo *)node;
    GQLUtils::quote(p->props.unit,evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalAttributeInfoStandardUnit)
    GQLAttributeInfo *p = (GQLAttributeInfo *)node;
    double d = 1.0;
    try {
      d = GQLUtils::parseDouble(p->props.standard_unit);
    } catch (std::string &err) {
    }
    GQLUtils::Double(d,evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalAttributeInfoDisplayUnit)
    GQLAttributeInfo *p = (GQLAttributeInfo *)node;
    double d = 1.0;
    try {
      d = GQLUtils::parseDouble(p->props.display_unit);
    } catch (std::string &err) {
    }
    GQLUtils::Double(d,evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalAttributeInfoFormat)
    GQLAttributeInfo *p = (GQLAttributeInfo *)node;
    GQLUtils::quote(p->props.format,evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalAttributeInfoMinValue)
    GQLAttributeInfo *p = (GQLAttributeInfo *)node;
    try {
      double d = GQLUtils::parseDouble(p->props.min_value);
      GQLUtils::Double(d,evalStr);
    } catch (std::string &err) {
      GQLUtils::quote("NaN",evalStr);
    }
END_EVAL

BEGIN_EVAL(GQLEvalAttributeInfoMaxValue)
    GQLAttributeInfo *p = (GQLAttributeInfo *)node;
    try {
      double d = GQLUtils::parseDouble(p->props.max_value);
      GQLUtils::Double(d,evalStr);
    } catch (std::string &err) {
      GQLUtils::quote("NaN",evalStr);
    }
END_EVAL

BEGIN_EVAL(GQLEvalAttributeInfoMinAlarm)
    GQLAttributeInfo *p = (GQLAttributeInfo *)node;
    try {
      double d = GQLUtils::parseDouble(p->props.min_alarm);
      GQLUtils::Double(d,evalStr);
    } catch (std::string &err) {
      GQLUtils::quote("NaN",evalStr);
    }
END_EVAL

BEGIN_EVAL(GQLEvalAttributeInfoMaxAlarm)
    GQLAttributeInfo *p = (GQLAttributeInfo *)node;
    try {
      double d = GQLUtils::parseDouble(p->props.max_alarm);
      GQLUtils::Double(d,evalStr);
    } catch (std::string &err) {
      GQLUtils::quote("NaN",evalStr);
    }
END_EVAL

BEGIN_EVAL(GQLEvalAttributeInfoDeltaT)
    GQLAttributeInfo *p = (GQLAttributeInfo *)node;
    try {
      double d = GQLUtils::parseDouble(p->props.alarms.delta_t);
      GQLUtils::Double(d,evalStr);
    } catch (std::string &err) {
      GQLUtils::quote("NaN",evalStr);
    }
END_EVAL

BEGIN_EVAL(GQLEvalAttributeInfoDeltaVal)
    GQLAttributeInfo *p = (GQLAttributeInfo *)node;
    try {
      double d = GQLUtils::parseDouble(p->props.alarms.delta_val);
      GQLUtils::Double(d,evalStr);
    } catch (std::string &err) {
      GQLUtils::quote("NaN",evalStr);
    }
END_EVAL

BEGIN_EVAL(GQLEvalAttributeInfoEnumLabels)
    GQLAttributeInfo *p = (GQLAttributeInfo *)node;
    GQLUtils::stringArr(p->props.enum_labels,evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalAttributeInfoDispLevel)
    GQLAttributeInfo *p = (GQLAttributeInfo *)node;
    GQLUtils::quote(displevels[p->props.disp_level],evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalAttributeInfoChangeRelChange)
    GQLAttributeInfo *p = (GQLAttributeInfo *)node;
    GQLUtils::quote(p->props.events.ch_event.rel_change,evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalAttributeInfoChangeAbsChange)
    GQLAttributeInfo *p = (GQLAttributeInfo *)node;
    GQLUtils::quote(p->props.events.ch_event.abs_change,evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalAttributeInfoPeriodicPeriod)
    GQLAttributeInfo *p = (GQLAttributeInfo *)node;
    GQLUtils::quote(p->props.events.per_event.period,evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalAttributeInfoArchiveRelChange)
    GQLAttributeInfo *p = (GQLAttributeInfo *)node;
    GQLUtils::quote(p->props.events.arch_event.archive_rel_change,evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalAttributeInfoArchiveAbsChange)
    GQLAttributeInfo *p = (GQLAttributeInfo *)node;
    GQLUtils::quote(p->props.events.arch_event.archive_abs_change,evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalAttributeInfoArchivePeriod)
    GQLAttributeInfo *p = (GQLAttributeInfo *)node;
    GQLUtils::quote(p->props.events.arch_event.archive_period,evalStr);
END_EVAL


DECLARE_EVAL(GQLEvalAttributeInfoWriteDescription)
END_DECLARE_EVAL

DECLARE_EVAL(GQLEvalAttributeInfoWriteLabel)
END_DECLARE_EVAL

DECLARE_EVAL(GQLEvalAttributeInfoWriteUnit)
END_DECLARE_EVAL

DECLARE_EVAL(GQLEvalAttributeInfoWriteStandardUnit)
END_DECLARE_EVAL

DECLARE_EVAL(GQLEvalAttributeInfoWriteDisplayUnit)
END_DECLARE_EVAL

DECLARE_EVAL(GQLEvalAttributeInfoWriteFormat)
END_DECLARE_EVAL

DECLARE_EVAL(GQLEvalAttributeInfoWriteMinValue)
END_DECLARE_EVAL

DECLARE_EVAL(GQLEvalAttributeInfoWriteMaxValue)
END_DECLARE_EVAL

DECLARE_EVAL(GQLEvalAttributeInfoWriteMinAlarm)
END_DECLARE_EVAL

DECLARE_EVAL(GQLEvalAttributeInfoWriteMaxAlarm)
END_DECLARE_EVAL

DECLARE_EVAL(GQLEvalAttributeInfoWriteDeltaT)
END_DECLARE_EVAL

DECLARE_EVAL(GQLEvalAttributeInfoWriteDeltaVal)
END_DECLARE_EVAL

DECLARE_EVAL(GQLEvalAttributeInfoWriteChangeRelChange)
END_DECLARE_EVAL

DECLARE_EVAL(GQLEvalAttributeInfoWriteChangeAbsChange)
END_DECLARE_EVAL

DECLARE_EVAL(GQLEvalAttributeInfoWritePeriodicPeriod)
END_DECLARE_EVAL

DECLARE_EVAL(GQLEvalAttributeInfoWriteArchiveRelChange)
END_DECLARE_EVAL

DECLARE_EVAL(GQLEvalAttributeInfoWriteArchiveAbsChange)
END_DECLARE_EVAL

DECLARE_EVAL(GQLEvalAttributeInfoWriteArchivePeriod)
END_DECLARE_EVAL

} // namespace TangoGQL_ns

#endif //TANGOGQL_GQLATTRIBUTEINFO_H
