//
// GQLDatabase object
//

#ifndef TANGOGQL_GQLDATABASE_H
#define TANGOGQL_GQLDATABASE_H

#include <utility>


#include "utils/GQLUtils.h"
#include "GQLDbDatum.h"
#include "GQLDbDeviceInfo.h"

namespace TangoGQL_ns {

class GQLEngine;

/** Database super class */
class GQLDatabaseBase : public GQLBase {

public:

  GQLDatabaseBase(GQLEngine *root, const std::string &type) : GQLBase(root), type(type), error(nullptr), db(nullptr) {
  }

  ~GQLDatabaseBase() {
    delete error;
  }

  Tango::DevFailed *error;
  Tango::Database *db;
  const std::string &type;

  std::string __typename() {
    return type;
  }

};

/** Database main node */
class GQLDatabase : public GQLDatabaseBase {

public:
  GQLDatabase(GQLEngine *root, const std::string &type) : GQLDatabaseBase(root, type) {
    needToDeleteDB = false;
  }

  ~GQLDatabase() {
    if (needToDeleteDB)
      delete db;
  }

  std::string host;
  int port;
  bool needToDeleteDB;

};

/** Database item node (browsing functions) */
class GQLDbItem : public GQLDatabaseBase {

public:
  GQLDbItem(GQLEngine *root, GQLDbItem *parent, Tango::Database *db, std::string& name, const std::string& type)
          : GQLDatabaseBase(root, type), parent(parent), name(name) {
    this->db = db;
  }

  const std::string name;
  GQLDbItem *parent;

};


/**
 * Main entry point of the database query or mutation
 */
DECLARE_EVAL(GQLEvalDatabase)
  GQLEvalDatabase(const std::string &type) : type(type) {}
private:
  const std::string type;
END_DECLARE_EVAL

// Work around issue https://gitlab.com/tango-controls/cppTango/-/issues/846
#define GET_FIELD_LIST(_call, _wildcard, _list) {                 \
  bool hasWildcard = _wildcard.find('*') != std::string::npos;         \
  if(hasWildcard) {                                               \
    _call(_wildcard) >> _list;                                    \
  } else {                                                        \
    std::string pWilcard = _wildcard + "*";                            \
    _call(pWilcard) >> _list;                                     \
    size_t fieldIdx = _wildcard.rfind('/');                       \
    std::string fieldName = (fieldIdx!=std::string::npos)?                  \
      _wildcard.substr(fieldIdx+1)                                \
     :_wildcard;                                                  \
    bool found = false;                                           \
    size_t i=0;                                                   \
    while(!found && i<_list.size()) {                             \
      found = ::strcasecmp(_list[i].c_str(),fieldName.c_str())==0;\
      if(!found) i++;                                             \
    }                                                             \
    _list.clear();                                                \
    if(found)                                                     \
      _list.push_back(fieldName);                                 \
  }                                                               \
}

/** Returns the host name */
BEGIN_EVAL(GQLEvalDatabaseHostName)
  GQLDatabase *d = (GQLDatabase *)node;
  GQLUtils::quote(d->host, evalStr);
END_EVAL

/** Returns the port */
BEGIN_EVAL(GQLEvalDatabasePort)
  GQLDatabase *d = (GQLDatabase *)node;
  evalStr.append(std::to_string(d->port));
END_EVAL

/** Returns the error */
BEGIN_EVAL(GQLEvalDatabaseError)
  GQLDatabaseBase *d = (GQLDatabaseBase *)node;
  if(d->error== nullptr)
    evalStr.append("null");
  else
    appendError(d->error->errors,args,evalStr);
END_EVAL

/** Returns the info string */
BEGIN_EVAL(GQLEvalDatabaseInfo)
  GQLDatabase *d = (GQLDatabase *)node;
  if(d->db == nullptr) {
    evalStr.append("null");
  } else {
    try {
      GQLUtils::quote(d->db->get_info(),evalStr,true);
    } catch (Tango::DevFailed &e) {
      evalStr.append("null");
      d->error = new Tango::DevFailed(e);
    }
  }
END_EVAL

/** Returns the list of HOST,SERVER,DEVICE,CLASS or OBJECT */
BEGIN_EVAL(GQLEvalDatabaseList)
  GQLDatabase *d = (GQLDatabase *)node;
  if(d->db == nullptr) {
    evalStr.append("null");
  } else {
    std::string& type = args->getString("type","list()");
    std::string& wildcard = args->getString("wildcard","list()");
    try {
      std::vector<std::string> list;
      if (type == "HOST") {
        d->db->get_host_list(wildcard) >> list;
      } else if (type == "SERVER") {
        // get_server_name_list does not have wildcard
        // Emulate using family of admin device !
        std::string admin = "dserver/" + wildcard;
        GET_FIELD_LIST(d->db->get_device_family, admin, list);
      } else if (type=="DEVICE") {
        // No call for querying device list !
        Tango::Except::throw_exception(
                "DBNotSupported",
                "Get DEVICE list not implemented",
                "GQLEvalDatabaseList");
      } else if (type=="CLASS") {
        d->db->get_class_list( wildcard) >> list;
      } else if (type=="OBJECT") {
        d->db->get_object_list(wildcard) >> list;
      } else if (type=="EXPORTED_DEVICE") {
        d->db->get_device_exported(wildcard) >> list;
      } else {
        Tango::Except::throw_exception(
                "DBWrongType",
                "Only HOST,SERVER,DEVICE,CLASS or OBJECT type are allowed for list",
                "GQLEvalDatabaseList");
      }
      GQLUtils::stringArr(list,evalStr, true);
    } catch (Tango::DevFailed &e) {
      evalStr.append("null");
      d->error = new Tango::DevFailed(e);
    }
  }
END_EVAL

/** Returns the DEVICE,CLASS or OBJECT property list */
BEGIN_EVAL(GQLEvalDatabasePropertyList)
    GQLDatabase *d = (GQLDatabase *)node;
    if(d->db == nullptr) {
      evalStr.append("null");
    } else {
      std::string& type = args->getString("type","property_list()");
      std::string& name = args->getString("name","property_list()");
      std::string& wildcard = args->getString("wildcard","property_list()");
      try {
        std::vector<std::string> list;
        if (type=="DEVICE") {
          d->db->get_device_property_list(name, wildcard) >> list;
        } else if (type=="CLASS") {
          d->db->get_class_property_list(name) >> list;
        } else if (type=="OBJECT") {
          d->db->get_object_property_list(name, wildcard) >> list;
        } else {
          Tango::Except::throw_exception(
                  "DBWrongType",
                  "Only DEVICE,CLASS or OBJECT type are allowed for property_list",
                  "GQLEvalDatabaseList");
        }
        GQLUtils::stringArr(list,evalStr, true);
      } catch (Tango::DevFailed &e) {
        evalStr.append("null");
        d->error = new Tango::DevFailed(e);
      }
    }
END_EVAL

/** SuperClass for items */
DECLARE_EVAL(GQLEvalDatabaseItems)
  virtual std::string getMethod() = 0;
  virtual bool isRoot() {return false;}
  virtual void getData(GQLEngine *root,Tango::Database *db,GQLDbItem *parent,std::string& wildcard,std::vector<GQLDbItem *>&items) = 0;
END_DECLARE_EVAL

/** Returns the item name */
BEGIN_EVAL(GQLEvalDatabaseItemsName)
    GQLDbItem *d = (GQLDbItem *)node;
    GQLUtils::quote(d->name, evalStr);
END_EVAL

/** Returns the domains */
class GQLEvalDatabaseDomains: public GQLEvalDatabaseItems {
  std::string getMethod() { return "domains()"; }
  virtual bool isRoot() { return true; }
  virtual void getData(GQLEngine *root,Tango::Database *db,GQLDbItem *parent,std::string& wildcard,std::vector<GQLDbItem *>&items) {
    bool hasWildcard = wildcard.find('*') != std::string::npos;
    if(hasWildcard) {
      if(root->logEnabled())
        root->log("GQLEvalDatabaseDomains","Call Database::get_device_domain("+wildcard+")");
      std::vector<std::string> list;
      db->get_device_domain(wildcard) >> list;
      for (size_t i = 0; i < list.size(); i++)
        items.push_back(new GQLDbItem(root, parent, db, list[i], tName));
    } else {
      items.push_back(new GQLDbItem(root, parent, db, wildcard, tName));
    }

  }
  const std::string tName = "DbDomain";
};

/** Returns the family list */
class GQLEvalDatabaseFamilies: public GQLEvalDatabaseItems {
  std::string getMethod() { return "families()"; }
  virtual void getData(GQLEngine *root,Tango::Database *db,GQLDbItem *parent,std::string& wildcard,std::vector<GQLDbItem *>&items) {
    bool hasWildcard = wildcard.find('*') != std::string::npos;
    if(hasWildcard) {
      std::string argin = parent->name + "/" + wildcard;
      if(root->logEnabled())
        root->log("GQLEvalDatabaseFamilies","Call Database::get_device_family("+argin+")");
      std::vector<std::string> list;
      db->get_device_family(argin) >> list;
      for (size_t i = 0; i < list.size(); i++)
        items.push_back(new GQLDbItem(root, parent, db, list[i], tName));
    } else {
      items.push_back(new GQLDbItem(root, parent, db, wildcard, tName));
    }
  }
  const std::string tName = "DbFamily";
};

/** Returns the members */
class GQLEvalDatabaseMembers: public GQLEvalDatabaseItems {
  std::string getMethod() { return "members()"; }
  virtual void getData(GQLEngine *root,Tango::Database *db,GQLDbItem *parent,std::string& wildcard,std::vector<GQLDbItem *>&items) {
    bool hasWildcard = wildcard.find('*') != std::string::npos;
    if(hasWildcard) {
      std::string argin = parent->parent->name + "/" + parent->name + "/" + wildcard;
      if(root->logEnabled())
        root->log("GQLEvalDatabaseMembers","Call Database::get_device_member("+argin+")");
      std::vector<std::string> list;
      db->get_device_member(argin) >> list;
      for (size_t i = 0; i < list.size(); i++)
        items.push_back(new GQLDbItem(root, parent, db, list[i], tName));
    } else {
      items.push_back(new GQLDbItem(root, parent, db, wildcard, tName));
    }
  }
  const std::string tName = "DbMember";
};

/** Returns the servers */
class GQLEvalDatabaseServers: public GQLEvalDatabaseItems {
  std::string getMethod() { return "servers()"; }
  virtual bool isRoot() { return true; }
  virtual void getData(GQLEngine *root,Tango::Database *db,GQLDbItem *parent,std::string& wildcard,std::vector<GQLDbItem *>&items) {
    bool hasWildcard = wildcard.find('*') != std::string::npos;
    if(hasWildcard) {
      std::string admin = "dserver/" + wildcard;
      if(root->logEnabled())
        root->log("GQLEvalDatabaseServers","Call Database::get_device_family("+admin+")");
      std::vector<std::string> list;
      db->get_device_family(admin) >> list;
      for (size_t i = 0; i < list.size(); i++)
        items.push_back(new GQLDbItem(root, parent, db, list[i], tName));
    } else {
      items.push_back(new GQLDbItem(root, parent, db, wildcard, tName));
    }
  }
  const std::string tName = "DbServer";
};

/** Returns the instance list */
class GQLEvalDatabaseInstances: public GQLEvalDatabaseItems {
  std::string getMethod() { return "instances()"; }
  virtual void getData(GQLEngine *root,Tango::Database *db,GQLDbItem *parent,std::string& wildcard,std::vector<GQLDbItem *>&items) {
    bool hasWildcard = wildcard.find('*') != std::string::npos;
    if(hasWildcard) {
      std::vector<std::string> list;
      std::string argin = parent->name;
      if(root->logEnabled())
        root->log("GQLEvalDatabaseInstances","Call Database::get_instance_name_list()");
      db->get_instance_name_list(argin) >> list;
      for (size_t i = 0; i < list.size(); i++)
        if (GQLUtils::match(list[i].c_str(), wildcard.c_str(), false))
          items.push_back(new GQLDbItem(root, parent, db, list[i], tName));
    } else {
      items.push_back(new GQLDbItem(root, parent, db, wildcard, tName));
    }
  }
  const std::string tName = "DbInstance";
};

/** Returns the class list */
class GQLEvalDatabaseClasses: public GQLEvalDatabaseItems {
  std::string getMethod() { return "classes()"; }
  virtual void getData(GQLEngine *root,Tango::Database *db,GQLDbItem *parent,std::string& wildcard,std::vector<GQLDbItem *>&items) {
    bool hasWildcard = wildcard.find('*') != std::string::npos;
    std::string argin = parent->parent->name + "/" + parent->name;
    if(hasWildcard) {
      std::vector<std::string> list;
      if(root->logEnabled())
        root->log("GQLEvalDatabaseClasses","Call Database::get_server_class_list()");
      db->get_server_class_list(argin) >> list;
      for (size_t i = 0; i < list.size(); i++)
        if(GQLUtils::match(list[i].c_str(),wildcard.c_str(),false))
          items.push_back(new GQLDbItem(root, parent, db, list[i], tName));
    } else {
      items.push_back(new GQLDbItem(root, parent, db, wildcard, tName));
    }
  }
  const std::string tName = "DbClass";
};

/** Returns the device list */
class GQLEvalDatabaseDevices: public GQLEvalDatabaseItems {
  std::string getMethod() { return "devices()"; }
  virtual void getData(GQLEngine *root,Tango::Database *db,GQLDbItem *parent,std::string& wildcard,std::vector<GQLDbItem *>&items) {
    bool hasWildcard = wildcard.find('*') != std::string::npos;
    std::string dsName = parent->parent->parent->name + "/" + parent->parent->name;
    std::string className = parent->name;
    if(hasWildcard) {
      std::vector<std::string> list;
      if(root->logEnabled())
        root->log("GQLEvalDatabaseClasses","Call Database::get_device_name("+dsName+","+className+")");
      db->get_device_name(dsName,className) >> list;
      for(size_t i=0;i<list.size();i++)
        if(GQLUtils::match(list[i].c_str(),wildcard.c_str(),false))
          items.push_back(new GQLDbItem(root,parent,db,list[i],tName));
    } else {
      items.push_back(new GQLDbItem(root, parent, db, wildcard, tName));
    }
  }
  const std::string tName = "DbClass";
};

/** Returns the DEVICE,CLASS or OBJECT properties */
BEGIN_EVAL(GQLEvalDatabaseProperties)
  GQLDatabase *d = (GQLDatabase *)node;
  if(d->db == nullptr) {
    evalStr.append("null");
  } else {

    std::string& type = args->getString("type","properties()");
    std::string& name = args->getString("name","properties()");
    JSONITEM propNames = args->get("propNames","properties()");

    try {

      std::vector<std::string> list;
      bool hasWildcard = propNames.items.size()==1 && propNames.items[0].find('*')!=std::string::npos;
      if(hasWildcard) {
        std::string& wildcard = propNames.items[0];
        if(type=="DEVICE") {
          d->db->get_device_property_list(name, wildcard) >> list;
        } else if (type=="CLASS") {
          if(wildcard!="*")
            Tango::Except::throw_exception(
                    "DBWrongWildcard",
                    "Only '*' can be used as wildcard for class property",
                    "GQLEvalDatabaseClassProperty");
          d->db->get_class_property_list(name) >> list;
        } else if (type=="OBJECT") {
          d->db->get_object_property_list(name,wildcard) >> list;
        } else {
          Tango::Except::throw_exception(
                  "DBWrongType",
                  "Only DEVICE,CLASS or OBJECT type are allowed for properties",
                  "GQLEvalDatabaseList");
        }
      }
      std::vector<std::string>& propList = (hasWildcard)?list:propNames.items;

      Tango::DbData dbData;
      for(size_t i=0;i<propList.size();i++)
        dbData.push_back(Tango::DbDatum(propList[i]));

      if(type=="DEVICE") {
        d->db->get_device_property(name, dbData);
      } else if (type=="CLASS") {
        d->db->get_class_property(name, dbData);
      } else if (type=="OBJECT") {
        d->db->get_property(name, dbData);
      } else {
        Tango::Except::throw_exception(
                "DBWrongType",
                "Only DEVICE,CLASS or OBJECT type are allowed for properties",
                "GQLEvalDatabaseList");
      }

      std::vector<GQLDbDatum *> gqlprops;
      for(size_t i=0;i<dbData.size();i++)
        gqlprops.push_back(new GQLDbDatum(d->root, "", dbData[i]));
      PTRARRAY(gqlprops);
      for(size_t i=0;i<gqlprops.size();i++)
        delete gqlprops[i];

    } catch (Tango::DevFailed &e) {
      evalStr.append("null");
      d->error = new Tango::DevFailed(e);
    }

  }
END_EVAL

/** Add a server */
DECLARE_EVAL(GQLEvalDatabaseAddServer)
END_DECLARE_EVAL

/** Remove a server */
DECLARE_EVAL(GQLEvalDatabaseDeleteServer)
END_DECLARE_EVAL

/** Put a DEVICE,CLASS or OBJECT property */
DECLARE_EVAL(GQLEvalDatabasePutProperty)
END_DECLARE_EVAL

/** Put a DEVICE or CLASS attribute property */
DECLARE_EVAL(GQLEvalDatabasePutAttributeProperty)
END_DECLARE_EVAL

/** Returns the device or class attribute property */
BEGIN_EVAL(GQLEvalDatabaseAttributeProperties)

    GQLDatabase *d = (GQLDatabase *)node;
    if(d->db == nullptr) {
      evalStr.append("null");
    } else {

      std::string& type = args->getString("type","attribute_properties()");
      std::string& name = args->getString("name","attribute_properties()");
      std::string& attName = args->getString("attName","attribute_properties()");

      try {

        Tango::DbData dbData;
        dbData.push_back(Tango::DbDatum(attName));
        if(type=="DEVICE") {
          d->db->get_device_attribute_property(name,dbData);
        } else if (type=="CLASS") {
          d->db->get_class_attribute_property(name,dbData);
        } else {
          Tango::Except::throw_exception(
                  "DBWrongType",
                  "Only DEVICE or CLASS type are allowed for attribute_properties",
                  "GQLEvalDatabaseAttributeProperties");
        }

        // Item 0 contains the number of properties
        long nbProp;
        dbData[0] >> nbProp;
        std::vector<GQLDbDatum *> gqlprops;
        for(size_t i=0;i<(size_t)nbProp;i++)
          gqlprops.push_back(new GQLDbDatum(d->root, attName, dbData[i + 1]));

        PTRARRAY(gqlprops);
        for(size_t i=0;i<gqlprops.size();i++)
          delete gqlprops[i];

      } catch (Tango::DevFailed &e) {
        evalStr.append("null");
        d->error = new Tango::DevFailed(e);
      }

    }

END_EVAL

BEGIN_EVAL(GQLEvalDatabaseDeviceInfo)

    GQLDatabase *d = (GQLDatabase *)node;
    if(d->db == nullptr) {
      evalStr.append("null");
    } else {

      std::string& name = args->getString("name","device_info()");

      try {

        Tango::DbDevFullInfo info = d->db->get_device_info(name);
        GQLDbDeviceInfo *gqldevinfo = new GQLDbDeviceInfo(d->root,name,info);
        children->execute(gqldevinfo,evalStr);
        delete gqldevinfo;

      } catch (Tango::DevFailed &e) {
        evalStr.append("null");
        d->error = new Tango::DevFailed(e);
      }

    }

END_EVAL

} // namespace TangoGQL_ns

#endif //TANGOGQL_GQLDATABASE_H
