//
// GQLRequest object
//

#include "../GQLEngine.h"


namespace TangoGQL_ns {

int GQLRequest::findFragment(std::vector<GQLRequest *> &fragments) {
  // Search a fragment without inner fragment
  bool found = false;
  size_t i = 0;
  while(!found && i<fragments.size()) {
    found = fragments[i]->fragmentNodes.empty();
    if(!found) i++;
  }
  return (found)?(int)i:(int)-1;
}

void GQLRequest::expandFragment(Parser* p,GQLRequest* req,GQLRequest *frag) {

  std::vector<GALFRAGMENTNODE>& dest = req->fragmentNodes;

  // Replace given fragment in all fragmentNode of the given request
  for(size_t i = 0;i<dest.size();) {

    if( dest[i].fragmentName==frag->name ) {

      // Check type
      if (dest[i].type->name != frag->fragmentSource) {
        throw std::string("Unexpected fragment type for fragment " + frag->name + ", " +
                     dest[i].type->name + " expected at " +
                     p->getErrorLocation(dest[i].pos));
      }

      // Expand
      GQLRequestFields *copy = frag->requests.clone();

      (*dest[i].node).insert((*dest[i].node).begin(),
                                   copy->items.begin(),
                                   copy->items.end());

      copy->items.clear();
      delete copy;

      // Remove reference
      dest.erase(dest.begin() + i);

    } else {
      i++;
    }

  }

}

void GQLRequest::expandFragments(Parser* p,std::vector<GQLRequest *> fragments) {

  bool end = fragmentNodes.empty();
  while( !end ) {

    // Search a fragment without inner fragments
    int idx = findFragment(fragments);
    if (idx == -1)
      throw std::string("Request having unsolvable fragment structure");

    // Remove the fragment from the array
    GQLRequest *frag = fragments[idx];
    fragments.erase(fragments.cbegin()+idx);

    // Replace all references to this fragment and expand them all
    expandFragment(p, this, frag);

    for (auto & fragment : fragments)
        expandFragment(p, fragment, frag);

    end = fragmentNodes.empty();

  }

}

std::string GQLRequest::fields_to_string(const std::vector<GQLRequestFields *> &gql, int level) const {

  std::string ret;

  for(auto fn : gql) {

    ret.append(level*2,' ');
    ret.append(fn->to_string());

    // Items
    if(!fn->items.empty()) {
      ret.append("{\n");
      ret.append(fields_to_string(fn->items,level+1));
      ret.append(level*2,' ');
      ret.push_back('}');
    }
    ret.push_back('\n');

  }

  return ret;

}

std::string GQLRequest::to_string() const {

  std::string ret;

  ret.append(type);
  ret.push_back(' ');
  if(name.length()>0) {
    ret.append(name);
    ret.push_back(' ');
  }
  if(fragmentSource.length()>0) {
    ret.append(" on ");
    ret.append(fragmentSource);
    ret.push_back(' ');
  }
  ret.append(" {\n");
  ret.append(fields_to_string(requests.items, 1));
  ret.append("}\n");

  return ret;

}

void GQLRequest::parse(GQLSchema *schema,Parser* p,JSON *variables) {


  p->jumpSpace();
  if (p->current() == '{') {

    // query shortcut
    GQLType *Query = schema->getQueryType();
    type = "query";
    p->jumpSpace();
    parseReqFields(p,Query,requests.items,variables);

  } else {

    size_t pos = p->getPosMarker();
    p->readWord(type);
    if (type=="query") {

      GQLType *Query = schema->getQueryType();
      p->jumpSpace();
      if (p->current() != '{') {
        p->readWord(name);
        inputArgs.parse(schema, p);
      }
      parseReqFields(p, Query, requests.items,variables);

    } else if (type=="mutation") {

      GQLType *Mutation = schema->getMutationType();
      p->jumpSpace();
      if (p->current() != '{') {
        p->readWord(name);
        inputArgs.parse(schema, p);
      }
      parseReqFields(p, Mutation, requests.items,variables);

    } else if (type=="subscription") {

      GQLType *Subscription = schema->getSubscriptionType();
      p->jumpSpace();
      if (p->current() != '{') {
        p->readWord(name);
        inputArgs.parse(schema, p);
      }
      parseReqFields(p, Subscription, requests.items,variables);

    } else if (type=="fragment") {

      p->readWord(name);
      p->jumpSep("on");
      p->jumpSpace();
      int pos2 = p->getPosMarker();
      p->readWord(fragmentSource);
      GQLType *typeSrc = schema->getType(fragmentSource);
      if (typeSrc == nullptr)
        throw std::string("fragment, type " + fragmentSource + " not defined " + p->getErrorLocation(pos2));

      parseReqFields(p, typeSrc, requests.items,variables);

    } else {
      throw std::string("Unexpected keyword " + type + " " + p->getErrorLocation(pos));
    }

  }

}

// Parse a list of items
void GQLRequest::parseReqFields(Parser* p,GQLType *nodeType,std::vector<GQLRequestFields *> &items,JSON *variables) {

  std::vector<int> fieldsPos;
  bool hasErrorField = false;
  int errorPos = 0;

  if(nodeType->hasFields()) {

    p->jumpSep('{');

    bool endItems = false;
    while (!endItems) {
      GQLRequestFields *f = new GQLRequestFields();
      GALFRAGMENTNODE fragNode;
      p->jumpSpace();
      int pos = p->getPosMarker();
      GQLType *fieldType = f->parse(p, nodeType, fragNode, variables);
      if(fieldType==nullptr) {
        // Fragment request
        fragNode.node = &items;
        fragmentNodes.push_back(fragNode);
        delete f;
      } else {
        f->eval = nodeType->getField(f->function)->evalFunction;
        parseReqFields(p, fieldType, f->items,variables);
        items.push_back(f);
        fieldsPos.push_back(pos);
        if(f->function == "error") {
          hasErrorField = true;
          errorPos = pos;
        }
      }
      endItems = p->endOf('}');
    }

    p->jumpSep('}');

    // Check for duplicate fields
    bool ok = true;
    size_t i = 0;
    size_t j;
    while(ok && i<items.size()) {
      j = i+1;
      while(ok && j<items.size()) {
        ok = items[i]->alias != items[j]->alias;
        if(ok) j++;
      }
      if(ok) i++;
    }

    if(!ok)
      throw std::string("Duplicate fields found in '"+nodeType->name+"': '" + items[i]->alias + "' at " +
                   p->getErrorLocation(fieldsPos[j]) +
                   ", Use alias 'alias:field' if you want to use several times the same field");

    // Check that error field is at the end
    if(hasErrorField && items[i-1]->function != "error")
      throw std::string("Error field at " + p->getErrorLocation(errorPos) + " in '" + nodeType->name +
      "' must be placed at the end.");

  }

}

void GQLRequest::execute(void *rootNode,std::string &result) {
  requests.execute(rootNode,result);
}

std::vector<GQLSubscription *> GQLRequest::executeSubscription(void *rootNode) {
  return requests.executeSubscription(rootNode);
}


} // namespace TangoGQL_ns
