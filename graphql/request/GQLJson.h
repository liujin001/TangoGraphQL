//
// GQLJson object
//

#ifndef TANGOGQL_GQLJSON_H
#define TANGOGQL_GQLJSON_H
#include "GQLRequest.h"

namespace TangoGQL_ns {

class GQLEngine;

class GQLJson {

public:

  GQLJson();
  ~GQLJson();

  // JSON fields of a GraphQL query
  std::string operationName;
  JSON variables;
  GQLRequest *request;

  void parse(GQLSchema *schema,std::string& request);
  void execute(void *rootNode,std::string& result) const;
  std::vector<GQLSubscription *>executeSubscription(void *rootNode) const;

};

} // namespace TangoGQL_ns
#endif //TANGOGQL_GQLJSON_H
