//
// GQLRequestArgs object
//

#ifndef TANGOGQL_GQLREQUESTARGS_H
#define TANGOGQL_GQLREQUESTARGS_H
#include <iostream>
#include <vector>
#include <map>

namespace TangoGQL_ns {

class GQLEngine;


class GQLRequestArgs {

public:

  bool empty();
  bool has(const std::string& name);
  void add(std::string& name,JSONITEM& value);

  /** Get input arguments of request */
  JSONITEM get(const std::string& name,const std::string& fieldName);
  std::string& getString(const std::string& name,const std::string& fieldName);

  std::string to_string() const;
  void parse(Parser* parser,JSON *variables);

private:

  bool isVar(const std::string& varName);
  JSONITEM& getVar(const std::string& varName,const std::string& fieldName);
  JSONITEM& getArg(const std::string& name,const std::string& fieldName);
  JSON args; // arguments
  JSON *vars; // query variables

};

} // namespace TangoGQL_ns

#endif //TANGOGQL_GQLREQUESTARGS_H
