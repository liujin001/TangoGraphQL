//
// GQLRequestFields Object
//

#include "../GQLEngine.h"


namespace TangoGQL_ns {

//static int count = 0;
GQLRequestFields::GQLRequestFields() {
  //count++;
  //std::cout << "GQLRequestFields:" << count << std::endl;
  eval = nullptr;
}
GQLRequestFields::~GQLRequestFields() {
  //count--;
  //std::cout << "GQLRequestFields:" << count << std::endl;
  for(auto & item : items)
    delete item;
}

GQLRequestFields *GQLRequestFields::clone() const {

  GQLRequestFields *newObj = new GQLRequestFields();
  newObj->inputArgs = this->inputArgs;
  newObj->alias = this->alias;
  newObj->function = this->function;
  newObj->eval = this->eval;
  for(auto & item : items)
    newObj->items.push_back(item->clone());
  return newObj;

}

void GQLRequestFields::checkArgs(GQLInputArgs& argDefinition) {

  if(argDefinition.empty() && inputArgs.empty())
    return;

  // GraphQL param order are random, they are indexed by the name

  for(auto & a : argDefinition.args) {

    if( !inputArgs.has(a->name) ) {

      if( !a->defaultValue.empty() ) {
        // Add param with default value
        JSONITEM arg;
        arg.items.insert( arg.items.begin() ,
                          a->defaultValue.begin() ,
                          a->defaultValue.end() );
        arg.type = (a->type->name == "String")?JSON_STRING:JSON_SCALAR;
        inputArgs.add(a->name,arg);
      } else {
        throw std::string("Missing parameter '" + a->name + "'");
      }

    }

  }

}


std::string GQLRequestFields::to_string() const {

  std::string ret;

  if( alias!=function ) {
    ret.append(alias);
    ret.push_back(':');
  }
  ret.append(function);

  // Args
  ret.append(inputArgs.to_string());

  return ret;

}

GQLType *GQLRequestFields::parse(Parser* p, GQLType *nodeType, GALFRAGMENTNODE& fragNode,JSON *variables) {

  // function name and alias
  p->jumpSpace();
  int pos = p->getPosMarker();

  p->readWord(alias);
  p->jumpSpace();
  if(p->current() == ':') {
    p->jumpSep(':');
    p->readWord(function);
  } else {
    function = alias;
  }

  try {

    std::string fragmentName;
    if(function=="...") {
      p->readWord(fragmentName);
    } else if(function.substr(0,3)=="...") {
      fragmentName = function.substr(3,function.length()-3);
    }
    if(!fragmentName.empty()) {
      // We have a fragment request
      fragNode.fragmentName = fragmentName;
      fragNode.type = nodeType;
      fragNode.pos = pos;
      return nullptr;
    }

    GQLField *f = nodeType->getField(function);

    if(f==nullptr)
      throw std::string("Field '" + function + "' not found on type " + nodeType->name);

    // function arguments (if any)
    pos = p->getPosMarker();
    inputArgs.parse(p,variables);

    checkArgs(f->args);

    return f->type->toRoot();

  } catch (std::string& message) {
    throw std::string(message + " " + p->getErrorLocation(pos));
  }

}

std::vector<GQLSubscription *> GQLRequestFields::executeSubscription(void *node) {
  // GraphQL impose one root node per subscription
  // Support more than one here
  std::vector<GQLSubscription *> subs;
  for(auto a : items)
    subs.emplace_back(a->eval->subscription(node, a, &a->inputArgs));
  return subs;
}

void GQLRequestFields::execute(void *node,std::string &result) {


  if(node==nullptr) {

    result.append("null");

  } else {

    // Eval tree
    result.push_back('{');
    for (size_t i = 0; i < items.size(); i++) {
      GQLRequestFields *a = items[i];
      result.append("\"");
      result.append(a->alias);
      result.append("\":");
      if(a->eval)
        a->eval->to_string(node, result, a, &a->inputArgs);
      else
        result.append("null");
      if (i < items.size() - 1) result.append(",");
    }
    result.push_back('}');

  }

}


} // namespace TangoGQL_ns
