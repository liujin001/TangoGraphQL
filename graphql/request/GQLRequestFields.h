//
// GQLRequestFields Object
//

#ifndef TANGOGQL_GQLREQUESTFIELDS_H
#define TANGOGQL_GQLREQUESTFIELDS_H
#include "GQLRequestArgs.h"
#include "graphql/schema/GQLEval.h"

namespace TangoGQL_ns {

class GQLEngine;
class GQLRequestFields;
class GQLEval;
class GQLType;
class GQLSubscription;

typedef struct {
  std::vector<GQLRequestFields *> *node;
  GQLType *type;
  std::string fragmentName;
  int pos;
} GALFRAGMENTNODE;

class GQLRequestFields {

public:
  GQLRequestFields();
  ~GQLRequestFields();

  std::string function;
  std::string alias;
  GQLRequestArgs inputArgs;
  GQLEval *eval;
  std::vector<GQLRequestFields *> items;

  std::string to_string() const;
  GQLType *parse(Parser* parser,GQLType *nodeType,GALFRAGMENTNODE& fragNode,JSON *variables);


  GQLRequestFields *clone() const;
  void execute(void *node,std::string &result);
  std::vector<GQLSubscription *> executeSubscription(void *node);

private:

  void checkArgs(GQLInputArgs& args);

};

} // namespace TangoGQL_ns


#endif //TANGOGQL_GQLREQUESTFIELDS_H
