//
// GQLRequest object
//

#ifndef TANGOGQL_GQLREQUEST_H
#define TANGOGQL_GQLREQUEST_H
#include "GQLRequestFields.h"
#include <iostream>
#include <vector>

namespace TangoGQL_ns {

class GQLEngine;

class GQLRequest {

public:

  std::string type;  // Query,Mutation,Subscription or fragment
  std::string name;
  std::string fragmentSource;
  GQLInputArgs inputArgs;
  GQLRequestFields requests;

  std::string to_string() const;
  void parse(GQLSchema *schema,Parser* parser,JSON *variables);
  void expandFragments(Parser* p,std::vector<GQLRequest *> fragments);
  void execute(void *rootNode,std::string &result);
  std::vector<GQLSubscription *> executeSubscription(void *rootNode);

private:
  std::vector<GALFRAGMENTNODE> fragmentNodes;

  int findFragment(std::vector<GQLRequest *> &fragments);
  void parseReqFields(Parser* p,GQLType *nodeType,std::vector<GQLRequestFields *> &items,JSON *variables);
  std::string fields_to_string(const std::vector<GQLRequestFields *> &gql, int level) const;
  void expandFragment(Parser* p,GQLRequest* req,GQLRequest *frag);

};

} // namespace TangoGQL_ns

#endif //TANGOGQL_GQLREQUEST_H
