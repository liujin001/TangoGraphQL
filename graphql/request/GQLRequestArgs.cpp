//
// GQLRequestArgs object
//

#include "../GQLEngine.h"


namespace TangoGQL_ns {

bool GQLRequestArgs::empty() {
  return args.empty();
}

bool GQLRequestArgs::has(const std::string& name) {
  return args.find(name) != args.end();
}

std::string& GQLRequestArgs::getString(const std::string& name,const std::string& fieldName) {

  JSONITEM& item = getArg(name,fieldName);

  // Replace variables
  if(item.type!=JSON_STRING && !vars->empty()) {
    if(isVar(item.items[0])) {
      JSONITEM& v = getVar(item.items[0],fieldName);
      return v.items[0];
    }
  }

  return item.items[0];

}

bool GQLRequestArgs::isVar(const std::string& varName) {
  return varName.length()>1 && varName[0]=='$';
}

JSONITEM& GQLRequestArgs::getVar(const std::string& varName,const std::string& fieldName) {

  auto it = (*vars).find(varName.substr(1));
  if(it==(*vars).end())
    throw std::string(fieldName + ", variable " + varName + " not found");
  return it->second;

}

JSONITEM& GQLRequestArgs::getArg(const std::string& name,const std::string& fieldName) {

  auto it = args.find(name);
  if (it == args.end())
    throw std::string(fieldName + ", argument " + name + " not found");
  if(it->second.items.empty())
    throw std::string(fieldName + ", argument " + name + " is empty");
  return it->second;

}

JSONITEM GQLRequestArgs::get(const std::string& name,const std::string& fieldName) {

  // Do a copy there, item may mutate
  JSONITEM item = getArg(name,fieldName);

  // Replace variables
  if(item.type!=JSON_STRING && !vars->empty()) {
    std::vector<std::string> newItems;
    for(size_t i=0;i<item.items.size();i++) {
      if(isVar(item.items[i])) {
        JSONITEM& v = getVar(item.items[i],fieldName);
        item.type = v.type; // Assume type are compatible
        for(auto & _item : v.items)
          newItems.push_back(_item);
      } else {
        newItems.push_back(item.items[i]);
      }
    }
    item.items = newItems;
  }

  return item;

}

void GQLRequestArgs::add(std::string& name,JSONITEM& value) {
  args.insert(std::pair<std::string, JSONITEM>(name, value));
}

std::string GQLRequestArgs::to_string() const {

  std::string ret;

  if(!args.empty()) {
    ret.push_back('(');
    size_t i = 0;
    for (auto it = args.begin(); it != args.end(); it++, i++) {

      ret.append(it->first);
      size_t sz = it->second.items.size();
      if(sz>1) ret.push_back('[');
      for(size_t j=0;j<sz;j++) {
        if (it->second.type==JSON_STRING) {
          ret.push_back('\"');
          ret.append(it->second.items[j]);
          ret.push_back('\"');
        } else {
          ret.append(it->second.items[j]);
        }
        if (j < sz - 1) ret.push_back(',');
      }
      if(sz>1) ret.push_back(']');
      if (i < args.size() - 1) ret.push_back(',');

    }
    ret.push_back(')');
  }

  return ret;

}

void GQLRequestArgs::parse(Parser* p,JSON *variables) {

  vars = variables;
  p->jumpSpace();
  if (p->current() == '(') {
    // Parse args
    try {
      p->parseJSON(args, '(', ')', true);
    } catch (std::string &error) {
      throw std::string(error+":parseArgs");
    }
  }

}


} // namespace TangoGQL_ns