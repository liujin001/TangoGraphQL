//
// CommandInfo object
//

#ifndef TANGOGQL_GQLCOMMANDINFO_H
#define TANGOGQL_GQLCOMMANDINFO_H

namespace TangoGQL_ns {

extern const char * formats[];
extern const char * writables[];
extern const char * displevels[];

class GQLEngine;

class GQLCommandInfo : public GQLBase {

public:

  GQLCommandInfo(GQLEngine *root,const Tango::CommandInfo& p):GQLBase(root),cmdInfos(p) {
  }

  const Tango::CommandInfo& cmdInfos;

  std::string __typename() {
    return "CommandInfo";
  }

};

BEGIN_EVAL(GQLEvalCommandInfoName)
    GQLCommandInfo *p = (GQLCommandInfo *)node;
    GQLUtils::quote(p->cmdInfos.cmd_name,evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalCommandInfoTag)
    GQLCommandInfo *p = (GQLCommandInfo *)node;
    evalStr.append(std::to_string(p->cmdInfos.cmd_tag));
END_EVAL

BEGIN_EVAL(GQLEvalCommandInfoInType)
    GQLCommandInfo *p = (GQLCommandInfo *)node;
    GQLUtils::quote(Tango::CmdArgTypeName[p->cmdInfos.in_type], evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalCommandInfoInTypeDesc)
    GQLCommandInfo *p = (GQLCommandInfo *)node;
    GQLUtils::quoteOrNull(p->cmdInfos.in_type_desc, evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalCommandInfoOutType)
    GQLCommandInfo *p = (GQLCommandInfo *)node;
    GQLUtils::quote(Tango::CmdArgTypeName[p->cmdInfos.out_type], evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalCommandInfoOutTypeDesc)
    GQLCommandInfo *p = (GQLCommandInfo *)node;
    GQLUtils::quoteOrNull(p->cmdInfos.out_type_desc, evalStr);
END_EVAL

} // namespace TangoGQL_ns


#endif //TANGOGQL_GQLCOMMANDINFO_H
