//
// GQLMutation status
//

#ifndef TANGOGQL_GQLMUTATIONSTATUS_H
#define TANGOGQL_GQLMUTATIONSTATUS_H

#include "utils/GQLUtils.h"

namespace TangoGQL_ns {

class GQLEngine;

#define CMD_OUT(typeCase, type, convFunc)    \
case Tango::typeCase:{                       \
type value;                                  \
(*(d->dd)) >> value;                         \
GQLUtils::convFunc(value,evalStr);           \
} break;

#define CMD_OUT_ARR(typeCase, superType, ctype, convFunc)  \
case Tango::typeCase:{                                     \
std::vector<ctype> v;                                      \
(*(d->dd)) >> v;                                           \
evalStr.push_back('[');                                    \
for(size_t i=0;i<v.size();i++) {                           \
  superType vST = (superType) v[i];                        \
  GQLUtils::convFunc(vST,evalStr);                         \
  if(i<v.size()-1) evalStr.push_back(',');                 \
}                                                          \
evalStr.push_back(']');                                    \
} break;

class GQLMutationStatus: public GQLBase {

public:

  explicit GQLMutationStatus(GQLEngine *root):GQLBase(root) {
    error = nullptr;
    dd = nullptr;
  }
  ~GQLMutationStatus() {
    delete error;
  }

  Tango::DevFailed   *error;
  Tango::DeviceData *dd;

  std::string __typename() {
    return "MutationStatus";
  }

};

/** Returns the error */
BEGIN_EVAL(GQLEvalMutationStatusError)
    GQLMutationStatus *d = (GQLMutationStatus *)node;
    if(d->error== nullptr)
      evalStr.append("null");
    else
      appendError(d->error->errors,args,evalStr);
END_EVAL

/** Command output */
BEGIN_EVAL(GQLEvalMutationStatusCommandOutput)

    GQLMutationStatus *d = (GQLMutationStatus *)node;

    if(d->dd==nullptr) {
      // no command output
      evalStr.append("null");
      return;
    }

    try {

      switch (d->dd->get_type()) {
        CMD_OUT(DEV_BOOLEAN, Tango::DevBoolean, Bool);
        CMD_OUT(DEV_SHORT, Tango::DevShort, Int64);
        CMD_OUT(DEV_USHORT, Tango::DevUShort, UInt64);
        CMD_OUT(DEV_LONG, Tango::DevLong, Int64);
        CMD_OUT(DEV_ULONG, Tango::DevULong, UInt64);
        CMD_OUT(DEV_LONG64, Tango::DevLong64, Int64);
        CMD_OUT(DEV_ULONG64, Tango::DevULong64, UInt64);
        CMD_OUT(DEV_FLOAT, Tango::DevFloat, Double);
        CMD_OUT(DEV_DOUBLE, Tango::DevDouble, Double);
        CMD_OUT(DEV_STRING,std::string,String);
        CMD_OUT_ARR(DEVVAR_CHARARRAY,uint64_t,unsigned char,UInt64);
        CMD_OUT_ARR(DEVVAR_SHORTARRAY,int64_t,Tango::DevShort ,Int64);
        CMD_OUT_ARR(DEVVAR_USHORTARRAY,uint64_t,Tango::DevUShort,UInt64);
        CMD_OUT_ARR(DEVVAR_LONGARRAY,int64_t,Tango::DevLong ,Int64);
        CMD_OUT_ARR(DEVVAR_ULONGARRAY,uint64_t,Tango::DevULong,UInt64);
        CMD_OUT_ARR(DEVVAR_LONG64ARRAY,int64_t,Tango::DevLong64 ,Int64);
        CMD_OUT_ARR(DEVVAR_ULONG64ARRAY,uint64_t,Tango::DevULong64,UInt64);
        CMD_OUT_ARR(DEVVAR_FLOATARRAY,double,Tango::DevFloat,Double);
        CMD_OUT_ARR(DEVVAR_DOUBLEARRAY,double,Tango::DevDouble,Double);
        CMD_OUT_ARR(DEVVAR_STRINGARRAY,std::string,std::string,String);
        case Tango::DEV_STATE: {
          std::string& resolveState = args->getString("resolveState","command_output()");
          bool resolve = GQLUtils::parseBoolean(resolveState);
          Tango::DevState value;
          (*(d->dd)) >> value;
          if(resolve)
            GQLUtils::quote(Tango::DevStateName[value],evalStr);
          else
            GQLUtils::UInt64(value,evalStr);
        } break;
        case Tango::DEVVAR_LONGSTRINGARRAY: {

          const Tango::DevVarLongStringArray *value;
          (*(d->dd)) >> value;

          evalStr.append("{\"long\":");
          evalStr.push_back('[');
          for(size_t i=0;i<(*value).lvalue.length();i++) {
            GQLUtils::Int64((*value).lvalue[i],evalStr);
            if(i<(*value).lvalue.length()-1) evalStr.push_back(',');
          }
          evalStr.append("],\"std::string\":[");
          for(size_t i=0;i<(*value).svalue.length();i++) {
            GQLUtils::String(std::string((*value).svalue[i]),evalStr);
            if(i<(*value).svalue.length()-1) evalStr.push_back(',');
          }
          evalStr.append("]}");

        } break;
        default:
          Tango::Except::throw_exception(
                  "AttrTypeNotSupported",
                  "Command out_type " + std::string(Tango::CmdArgTypeName[d->dd->get_type()]) + " not supported",
                  "GQLEvalMutationStatusCommandOutput");
      }

    } catch (Tango::DevFailed e) {

      evalStr.append("null");
      if(d->error == nullptr)
        d->error = new Tango::DevFailed(e);

    }

END_EVAL

} // namespace TangoGQL_ns

#endif //TANGOGQL_GQLMUTATIONSTATUS_H
