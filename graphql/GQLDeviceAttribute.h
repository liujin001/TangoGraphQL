//
// GQLDeviceAttribute object
//

#ifndef TANGOGQL_GQLDEVICEATTRIBUTE_H
#define TANGOGQL_GQLDEVICEATTRIBUTE_H

#include "TangoGQL.h"
#include "GQLDevice.h"

namespace TangoGQL_ns {

extern const char * qualities[];
extern const char * formats[];

class GQLEngine;

typedef union {

  std::vector<Tango::DevUChar> *u8;
  std::vector<Tango::DevShort> *i16;
  std::vector<Tango::DevUShort> *u16;
  std::vector<Tango::DevLong> *i32;
  std::vector<Tango::DevULong> *u32;
  std::vector<Tango::DevLong64> *i64;
  std::vector<Tango::DevULong64> *u64;
  std::vector<Tango::DevFloat> *floats;
  std::vector<Tango::DevDouble> *doubles;
  std::vector<std::string> *strings;
  std::vector<Tango::DevEnum> *enums;
  std::vector<Tango::DevState> *states;
  std::vector<Tango::DevBoolean> *bools;
  std::vector<void *> *ptr;

} ATTRIBUTEDATA;


class GQLDeviceAttribute : public GQLBase {

public:

  GQLDeviceAttribute(GQLEngine *root,Device *dev,Tango::DeviceAttribute& da,bool isHistory);
  ~GQLDeviceAttribute();

  ATTRIBUTEDATA data;
  Tango::DevEncoded *encodedData;
  Tango::DeviceAttribute &da;
  Device *dev;
  bool isHistory;

  std::string __typename() {
    return "DeviceAttribute";
  }

  void formatValue(std::string &evalStr,bool isWrite,bool resolveEnum);

private:

  void formatScalarValue(std::string &evalStr,bool isWrite,bool resolveEnum);
  void formatArrayValue(std::string &evalStr,int start,int length,bool resolveEnum);

  template<typename inType>
  void serializeIntScalar(std::vector<inType> *value,int id,std::string &evalStr);

  template<typename inType>
  void serializeFloatArray(std::vector<inType> *value,int start,int length,std::string &evalStr);

  template<typename inType>
  void serializeUIntScalar(std::vector<inType> *value,int id,std::string &evalStr);

  template<typename inType>
  void serializeFloatScalar(std::vector<inType> *value,int id,std::string &evalStr);

  template<typename inType>
  void serializeIntArray(std::vector<inType> *value,int start,int length,std::string &evalStr);

  template<typename inType>
  void serializeUIntArray(std::vector<inType> *value,int start,int length,std::string &evalStr);

  void serializeState(std::vector<Tango::DevState> *value,int id,std::string &evalStr);
  void serializeEnum(std::vector<Tango::DevEnum> *value,int id,std::string &evalStr);
  void serializeString(std::vector<std::string> *value,int id,std::string &evalStr);
  void serializeBool(std::vector<Tango::DevBoolean> *value,int id,std::string &evalStr);
  void serializeEncoded(Tango::DevEncoded *value,std::string &evalStr);

  void serializeStateArray(std::vector<Tango::DevState> *value,int start,int length,std::string &evalStr);
  void serializeEnumArray(std::vector<Tango::DevEnum> *value,int start,int length,std::string &evalStr);
  void serializeStringArray(std::vector<std::string> *value,int start,int length,std::string &evalStr);
  void serializeBoolArray(std::vector<Tango::DevBoolean> *value,int start,int length,std::string &evalStr);

};

BEGIN_EVAL(GQLEvalDeviceAttributeName)
    GQLDeviceAttribute *g = (GQLDeviceAttribute *)node;
    GQLUtils::quote(g->da.name,evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalDeviceAttributeError)
    GQLDeviceAttribute *g = (GQLDeviceAttribute *)node;
    if(!g->da.has_failed()) {
      try {
        if(g->da.is_empty()) {
          Tango::Except::throw_exception("EmptyData","Empty data","GQLEvalDeviceAttributeError");
        } else {
          // No error
          evalStr.append("null");
        }
      } catch (Tango::DevFailed &e) {
        appendError(e.errors,args,evalStr);
      }
    } else
      appendError(g->da.get_err_stack(),args,evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalDeviceAttributeQuality)
    GQLDeviceAttribute *g = (GQLDeviceAttribute *)node;
    if(!g->da.has_failed())
      GQLUtils::quote(qualities[g->da.get_quality()],evalStr);
    else
      GQLUtils::quote("ATTR_ERROR",evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalDeviceAttributeValue)
    GQLDeviceAttribute *g = (GQLDeviceAttribute *) node;
    std::string& resolveEnum = args->getString("resolveEnum","value()");
    g->formatValue(evalStr,false,resolveEnum=="true");
END_EVAL

BEGIN_EVAL(GQLEvalDeviceAttributeWriteValue)
    GQLDeviceAttribute *g = (GQLDeviceAttribute *) node;
    std::string& resolveEnum = args->getString("resolveEnum","value()");
    g->formatValue(evalStr,true,resolveEnum=="true");
END_EVAL

BEGIN_EVAL(GQLEvalDeviceAttributeTimeStamp)
    GQLDeviceAttribute *g = (GQLDeviceAttribute *) node;
    std::string& formatAsDate = args->getString("formatAsDate","value()");
    char tmp[64];
    std::string timestamp;
    if(formatAsDate=="true") {
      time_t ts = g->da.time.tv_sec;
      struct tm *timeinfo = localtime (&ts);
      ::sprintf(tmp, "%02d/%02d/%04d %02d:%02d:%02d.%06d",
                timeinfo->tm_mday,
                timeinfo->tm_mon+1,
                timeinfo->tm_year + 1900,
                timeinfo->tm_hour,
                timeinfo->tm_min,
                timeinfo->tm_sec,
                g->da.time.tv_usec);
      timestamp.append(tmp);
      GQLUtils::quote(timestamp,evalStr);
    } else {
      timestamp.append(std::to_string(g->da.time.tv_sec));
      timestamp.push_back('.');
      ::sprintf(tmp, "%06d", g->da.time.tv_usec);
      timestamp.append(tmp);
      evalStr.append(timestamp);
    }
END_EVAL

BEGIN_EVAL(GQLEvalDeviceAttributeDimX)
    GQLDeviceAttribute *g = (GQLDeviceAttribute *) node;
    evalStr.append(std::to_string(g->da.get_dim_x()));
END_EVAL

BEGIN_EVAL(GQLEvalDeviceAttributeDimY)
    GQLDeviceAttribute *g = (GQLDeviceAttribute *) node;
    evalStr.append(std::to_string(g->da.get_dim_y()));
END_EVAL

BEGIN_EVAL(GQLEvalDeviceAttributeWDimX)
    GQLDeviceAttribute *g = (GQLDeviceAttribute *) node;
    evalStr.append(std::to_string(g->da.get_written_dim_x()));
END_EVAL

BEGIN_EVAL(GQLEvalDeviceAttributeWDimY)
    GQLDeviceAttribute *g = (GQLDeviceAttribute *) node;
    evalStr.append(std::to_string(g->da.get_written_dim_y()));
END_EVAL

BEGIN_EVAL(GQLEvalDeviceAttributeDataType)
    GQLDeviceAttribute *g = (GQLDeviceAttribute *) node;
    if(!g->da.has_failed()) {
      GQLUtils::quote(std::string(Tango::CmdArgTypeName[g->da.get_type()]), evalStr);
    } else {
      evalStr.append("null");
    }
END_EVAL

BEGIN_EVAL(GQLEvalDeviceAttributeDataFormat)
    GQLDeviceAttribute *g = (GQLDeviceAttribute *) node;
    if(!g->da.has_failed()) {
      GQLUtils::quote(formats[g->da.get_data_format()], evalStr);
    } else {
      evalStr.append("null");
    }
END_EVAL

BEGIN_EVAL(GQLEvalDeviceAttributeEncodedFormat)
    GQLDeviceAttribute *g = (GQLDeviceAttribute *) node;
    if(g->da.has_failed() || g->da.get_type()!=Tango::DEV_ENCODED) {
      evalStr.append("null");
    } else {
      GQLUtils::quote(g->encodedData->encoded_format.in(), evalStr);
    }
END_EVAL

} // namespace TangoGQL_ns


#endif //TANGOGQL_GQLDEVICEATTRIBUTE_H
