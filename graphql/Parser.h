//
// Parser object
//

#ifndef TANGOGQL_PARSER_H
#define TANGOGQL_PARSER_H

#include <iostream>
#include <vector>
#include <map>

namespace TangoGQL_ns {

#define JSON_OBJECT 1
#define JSON_STRING 2
#define JSON_SCALAR 3

typedef struct {
  int type; // items type
  std::vector<std::string> items;
} JSONITEM;

typedef std::map<std::string,JSONITEM> JSON;

class Parser {

public:

  Parser(const std::string &toParse,const std::string &section);

  /** Parse a JSON file */
  void parseJSON(JSON& json,char openChar='{',char closeChar='}',bool allowEmptySeparator=false);
  /** Jump scape and comments */
  void jumpSpace();
  /** Reads the next word */
  void readWord(std::string& word);
  /** Jump word and throw error if the current word is not the expected one */
  void jumpSep(const std::string& sep);
  /** Jump word and throw error if the current word is not the expected one */
  void jumpSep(char sep);
  /** Parse an array */
  void parseArray(std::vector<std::string> &ret,bool *isStringArray= nullptr,bool allowEmptySeparator=false);
  /** Get current position */
  int getPosMarker() const;
  /** Returns a message containing the error location */
  std::string getErrorLocation(int pos);
  /** Return current char being parsed */
  char current()  const;
  /** Return last comment occurrence */
  std::string getLastComment();
  /** Return true if the next char to be parsed if c or eof has been reach */
  bool endOf(char c);
  /** Return true if eof has been reach */
  bool eof();

private:
  const std::string& reqToParse;
  const std::string  sectionName;
  std::string lastComment;
  size_t currentPos;
  char nextNextChar;
  char nextChar;
  char currentChar;
  bool backSlashed;

  bool isSpecialChar(char c);
  char read();
  void readChar();
  void toNextChar();
  std::string getCoord(int pos);

};

} // namespace TangoGQL_ns

#endif //TANGOGQL_PARSER_H
