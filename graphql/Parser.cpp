//
// Parser object
//

#include "utils/GQLUtils.h"
#include "Parser.h"
#include <algorithm>


namespace TangoGQL_ns {

Parser::Parser(const std::string &toParse,const std::string &section):reqToParse(toParse),sectionName(section) {

  currentChar = 0;
  nextChar = 0;
  nextNextChar = 0;
  currentPos = 0;
  backSlashed = false;

  // Done 3 times to initialise nextchars and currentChar
  readChar();
  readChar();
  readChar();

}

void Parser::parseJSON(JSON& json,char openChar,char closeChar,bool allowEmptySeparator) {

  std::string w;
  jumpSpace();
  size_t pos = getPosMarker();
  readWord(w);
  if(w=="null")
    return;

  if (!(w.length()==1 && openChar==w[0])) {
    std::string sepAsStr;
    sepAsStr.push_back(openChar);
    throw std::string("'" + sepAsStr + "' expected but got '" + w + "' " + getErrorLocation(pos));
  }

  bool eof = (current()==closeChar);
  if(eof)  {
    // Empty json
    jumpSep(closeChar);
    return;
  }

  while(!eof) {

    std::string key;
    readWord(key);
    jumpSep(':');
    jumpSpace();
    JSONITEM value;
    if (current() == '{') {

      // Parse sub json
      JSON sub;
      parseJSON(sub);
      std::string objValue;
      objValue.push_back('{');
      size_t i = 0;
      for (auto it = sub.begin(); it != sub.end(); it++, i++) {

        objValue.push_back('\"');
        objValue.append(it->first);
        objValue.append("\":");
        size_t sz = it->second.items.size();

        if(sz>1) objValue.push_back('[');
        for(size_t j=0;j<sz;j++) {
          if (it->second.type==JSON_STRING) {
            objValue.push_back('\"');
            std::string escValue;
            GQLUtils::escape(it->second.items[j], escValue);
            objValue.append(escValue);
            objValue.push_back('\"');
          } else {
            objValue.append(it->second.items[j]);
          }
          if (j < sz - 1) objValue.push_back(',');
        }
        if(sz>1) objValue.push_back(']');
        if (i < sub.size() - 1) objValue.push_back(',');

      }
      objValue.push_back('}');

      value.type = JSON_OBJECT;
      value.items.push_back(objValue);

    } else if (current() == '[') {

      bool isStringArray;
      parseArray(value.items,&isStringArray,allowEmptySeparator);
      value.type = (isStringArray)?JSON_STRING:JSON_SCALAR;

    } else {

      bool isString = current() == '\"';
      std::string objValue;
      readWord(objValue);
      value.type = (isString)?JSON_STRING:JSON_SCALAR;
      value.items.push_back(objValue);

    }
    json.insert(std::pair<std::string, JSONITEM>(key, value));
    jumpSpace();
    if (current() == closeChar) {
      jumpSep(closeChar);
      eof = true;
    } else if (current() == 0) {
      std::string errStr;
      errStr.append("Unexpected end of file '");
      errStr.push_back(closeChar);
      errStr.append("' missing, field:");
      errStr.append(key);
      throw std::string(errStr);
    } else {
      if( allowEmptySeparator ) {
        if(current() == ',') jumpSep(',');
      } else {
        jumpSep(',');
      }
    }

  }

}

// Return char char
char Parser::current() const {
  return currentChar;
}

std::string Parser::getLastComment() {
  return lastComment;
}

// Get a char from the input string
char Parser::read() {
  if (currentPos < reqToParse.size())
    return reqToParse[currentPos++];
  else
    return 0;
}

// Go to next char
void Parser::toNextChar() {

  char c = read();
  currentChar = nextChar;
  nextChar = nextNextChar;
  nextNextChar = c;

}

// Read a char and handle backslash
void Parser::readChar() {

  backSlashed = false;

  toNextChar();

  /* Escape sequence */
  if (currentChar == '\\') {
    switch (nextChar) {
      case '"':
        toNextChar();          // return '"'
        backSlashed = true;  // For string handling
        break;
      case 'n':
        toNextChar();
        currentChar = '\n';  // return '\n'
        break;
      case 'r':
        toNextChar();
        currentChar = '\r';  // return '\r'
        break;
      case 't':
        toNextChar();
        currentChar = '\t'; // return '\t'
        break;
      case '\\':
        toNextChar(); // return '\'
        break;
    }
  }

}

// Jump space and comments
void Parser::jumpSpace() {
  if(currentChar>32 && currentChar!='#')
    return;
  while (currentChar <= 32 && currentChar > 0) readChar();

  // Keep a trace of last comments
  lastComment.clear();
  while(currentChar=='#') {
    readChar(); // Jump the #
    // Comment up to a \n
    while (currentChar >= 32 && currentChar!=0) {
      lastComment.push_back(currentChar);
      readChar();
    }
    if(currentChar=='\r') readChar();
    if(currentChar=='\n') readChar();
    while (currentChar <= 32 && currentChar > 0) {
      // Clear last comment if we have a line break
      if(currentChar=='\n') lastComment.clear();
      readChar();
    }
  }

}

// Special character
bool Parser::isSpecialChar(char c) {
  return c == '=' || c == '!' || c == ':' || c == ',' || c == '[' || c == ']' || c == '(' || c == ')' || c == '{' || c == '}';
}

// Read a word
void Parser::readWord(std::string& word) {

  word.clear();

  /* Jump space and comments */
  jumpSpace();

  /* Treat special character */
  if (isSpecialChar(currentChar)) {
    word.push_back(currentChar);
    readChar();
    return;
  }

  /* Treat char */
  if (currentChar == '\'' && nextNextChar == '\'') {
    word.push_back(currentChar);
    readChar();
    word.push_back(currentChar);
    readChar();
    word.push_back(currentChar);
    readChar();
    return;
  }

  /* Treat string */
  if (currentChar == '"' && !backSlashed) {
    readChar();
    while ((currentChar != '"' || backSlashed) && currentChar != 0) {
      word.push_back(currentChar);
      readChar();
    }
    if (currentChar == 0) {
      throw std::string("Unterminated std::string");
    }
    readChar();
    return;
  }

  /* Treat other word */
  while (currentChar > 32 && !isSpecialChar(currentChar)) {
    word.push_back(currentChar);
    readChar();
  }

}

int Parser::getPosMarker() const {
  return (int)currentPos-((currentChar==0)?0:1)-((nextNextChar==0)?0:1)-((nextNextChar==0)?0:1);
}

std::string Parser::getErrorLocation(int pos) {
  return "at " + getCoord(pos) + " in " + sectionName;
}

// Jump a separator
void Parser::jumpSep(const std::string& sep) {
  jumpSpace();
  size_t pos = getPosMarker();
  std::string w;
  readWord(w);
  if (w!=sep)
    throw std::string("'" + sep + "' expected but got '" + w + "' " + getErrorLocation(pos));
}

void Parser::jumpSep(const char sep) {
  jumpSpace();
  size_t pos = getPosMarker();
  std::string w;
  readWord(w);
  if (!(w.length()==1 && sep==w[0])) {
    std::string sepAsStr;
    sepAsStr.push_back(sep);
    throw std::string("'" + sepAsStr + "' expected but got '" + w + "' " + getErrorLocation(pos));
  }
}

// Parse array
void Parser::parseArray(std::vector<std::string> &ret, bool *isStringArray,bool allowEmptySeparator) {

  bool isClosed = false;

  jumpSpace();
  if (currentChar == '[') {
    isClosed = true;
    jumpSep('[');
    jumpSpace();
  }
  if(isStringArray)
    *isStringArray = (currentChar == '\"');

  while (!endOf(']')) {
    std::string w;
    readWord(w);
    ret.push_back(w);
    if (!endOf(']')) {
      if( allowEmptySeparator ) {
        if(current() == ',') jumpSep(',');
      } else {
        jumpSep(',');
      }
    }
  }

  if (isClosed) jumpSep(']');

}

std::string Parser::getCoord(int pos) {

  // Find line and offset of a position
  size_t p = 0;
  size_t line = 1;
  size_t column = 1;

  if(pos>=(int)reqToParse.length()-1)
    pos = (int)reqToParse.length()-1;

  while(p<(size_t)pos) {
    if(reqToParse[p]=='\n') {
      line++;
      column = 1;
    } else {
      column++;
    }
    p++;
  }

  return std::to_string(line)+":"+std::to_string(column);

}

bool Parser::endOf(char c) {
  jumpSpace();
  return (currentChar==c) || (currentChar==0);

}

bool Parser::eof() {
  jumpSpace();
  return (currentChar==0);
}

} // namespace TangoGQL_ns
