//
//  GQLUtils utility class
//

#ifndef TANGOGQL_GQLUTILS_H
#define TANGOGQL_GQLUTILS_H

#include <string>
#include <vector>

namespace TangoGQL_ns {

class GQLUtils {
public:
  static void quote(const std::string& value,std::string& evalStr,bool doEscape=false);
  static void quoteOrNull(const std::string& value,std::string& evalStr,bool doEscape=false);
  static void noQuoteOrNull(const std::string& value,std::string& evalStr,bool doEscape=false);
  static void unescape(const std::string &in,std::string& evalStr);
  static void escape(const std::string& in,std::string& evalStr);
  static void stringArr(const std::vector<std::string>& strArr,std::string& evalStr,bool doEscape=false);

  static void UInt64(uint64_t value,std::string& evalStr);
  static void Int64(int64_t value,std::string& evalStr);
  static void Bool(bool value,std::string& evalStr);
  static void Double(double value,std::string& evalStr);
  static void String(const std::string& in,std::string& evalStr);

  static int64_t parseInt(const std::string& valueStr);
  static uint64_t parseUInt(const std::string& valueStr);
  static double parseDouble(const std::string& valueStr);
  static bool parseBoolean(const std::string& valueStr);

  static bool match(const char *str, const char *pattern, bool caseSensitive);

  template<typename T>
  static void parseBooleanArray(std::vector<std::string>& in,std::vector<T>& out) {
    for(size_t i=0;i<in.size();i++) {
      try {
        bool v = parseBoolean(in[i]);
        out.push_back((T)v);
      } catch (std::string& err) {
        throw std::string(err + " at index " + std::to_string(i));
      }
    }
  }
  template<typename T>
  static void parseIntArray(std::vector<std::string>& in,std::vector<T>& out) {
    for(size_t i=0;i<in.size();i++) {
      try {
        int64_t v = parseInt(in[i]);
        out.push_back((T)v);
      } catch (std::string& err) {
        throw std::string(err + " at index " + std::to_string(i));
      }
    }
  }
  template<typename T>
  static void parseUIntArray(std::vector<std::string>& in,std::vector<T>& out) {
    for(size_t i=0;i<in.size();i++) {
      try {
        int64_t v = parseUInt(in[i]);
        out.push_back((T)v);
      } catch (std::string& err) {
        throw std::string(err + " at index " + std::to_string(i));
      }
    }
  }
  template<typename T>
  static void parseFloatArray(std::vector<std::string>& in,std::vector<T>& out) {
    for(size_t i=0;i<in.size();i++) {
      try {
        double v = parseDouble(in[i]);
        out.push_back((T)v);
      } catch (std::string& err) {
        throw std::string(err + " at index " + std::to_string(i));
      }
    }
  }

};

} // namespace TangoGQL_ns

#endif //TANGOGQL_GQLUTILS_H
