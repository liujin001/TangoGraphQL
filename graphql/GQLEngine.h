//
// GQL Engine
//

#ifndef TANGOGQL_GQLENGINE_H
#define TANGOGQL_GQLENGINE_H

//#define GQLPERF

#include "Parser.h"
#include "graphql/schema/GQLSchema.h"
#include "graphql/schema/GQLTypeEval.h"
#include "graphql/request/GQLJson.h"
#include "GQLSubscription.h"

namespace TangoGQL_ns {


class GQLEngine: public Tango::LogAdapter {

public:

  static void init();

  GQLEngine(TangoGQL *,DeviceFactory *);

  void test();

  /** Execute a json request, return true if completion was successful  */
  bool execute(std::string& jsonRequest,std::string& result,std::string& mutationMode,bool allowMultiTangoHost);

  /** Execute a json subscription, return true if completion was successful  */
  bool executeSubscription(std::string& jsonRequest,std::string& result);

  /** Unregister subscription belonging to conn */
  void unregisterSubscription(Connection *conn,bool signalWSEnd);

  /** Parse a JSON file and returns a map */
  static void parseJSON(std::string& jsonText,JSON& json);

  /** Returns the DeviceFactory associated to this engine */
  DeviceFactory *getFactory() {
    return factory;
  }

  /** Returns true if MultiTangoHost allowed */
  bool isMultiTangoHostAllowed() {
    return allowMultiTangoHost;
  }

  /** Log a message */
  bool logEnabled();
  void log(const std::string& method,const std::string& message);
  /** Force log */
  static void enableLog();

  /** GraphQL schema */
  static GQLSchema *mainSchema;

  /** Returns the graphql schema as std::string */
  std::string getSchemaAsString();

private:

  DeviceFactory *factory;
  std::vector<GQLSubscription *> subscriptions;
  bool allowMultiTangoHost;
  static bool forceLog;


};

} // namespace TangoGQL_ns

#endif //TANGOGQL_GQLENGINE_H
