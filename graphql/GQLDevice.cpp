//
// GQLDevice class
//

#include "TangoGQL.h"
#include "GQLDevice.h"
#include "GQLDeviceAttribute.h"
#include "GQLMutationStatus.h"
#include "GQLAttributeInfo.h"
#include "GQLCommandInfo.h"


#define WRITE_SCALAR(typeCase,type,convFunc)                          \
case Tango::typeCase:{                                                \
Tango::DeviceAttribute da;                                            \
da.set_name(attName);                                                 \
da << (Tango::type) (convFunc);                                       \
d->dev->ds()->write_attribute(da);                                    \
}break;

#define WRITE_ARRAY(typeCase,type,convFunc)                           \
case Tango::typeCase:{                                                \
Tango::DeviceAttribute da;                                            \
std::vector<Tango::type> v;                                                \
da.set_name(attName);                                                 \
GQLUtils::convFunc<Tango::type>(value,v);                             \
da << v;                                                              \
d->dev->ds()->write_attribute(da);                                    \
}break;

#define WRITE_IMAGE(typeCase,type,convFunc,dimX,dimY)                 \
case Tango::typeCase:{                                                \
std::vector<Tango::type> v;                                                \
GQLUtils::convFunc<Tango::type>(value,v);                             \
Tango::DeviceAttribute da(attName,v,dimX,dimY);                       \
d->dev->ds()->write_attribute(da);                                    \
}break;

#define EXEC_CMD(typeCase,convFunc)                       \
case Tango::typeCase:{                                    \
Tango::DeviceData _argin;                                 \
_argin << (convFunc);                                     \
argout = d->dev->ds()->command_inout(cmdName,_argin);     \
}break;

#define EXEC_CMD_ARR(typeCase,type,convFunc)              \
case Tango::typeCase:{                                    \
Tango::DeviceData _argin;                                 \
type v;                                                   \
v.length(arginStr.size());                                \
for(size_t i=0;i<arginStr.size();i++)                     \
  v[i] = convFunc;                                        \
_argin << v;                                              \
argout = d->dev->ds()->command_inout(cmdName,_argin);     \
}break;

namespace TangoGQL_ns {

void GQLEvalDevice::to_string(void *node,std::string &evalStr,GQLRequestFields *children,GQLRequestArgs *args) {

  GQLEngine *root = (GQLEngine *)node;
  std::string& name = args->getString("name","device()");
  GQLDevice d(root,name,type);

  try {
    d.dev = root->getFactory()->get(name,root->isMultiTangoHostAllowed());
  } catch (Tango::DevFailed &e) {
    // Happens only when the device is not defined
    d.error = new Tango::DevFailed(e);
  }

  children->execute((void *) &d, evalStr);

}

void GQLEvalDeviceReadAttributes::to_string(void *node,std::string &evalStr,GQLRequestFields *children,GQLRequestArgs *args) {

  GQLDevice *d = (GQLDevice *) node;

  if (d->dev == nullptr) {
    // attributes is null is case of major device failure
    evalStr.append("null");
    return;
  }

  JSONITEM names = args->get("names", "attributes()");
  std::vector<GQLDeviceAttribute *> gqldas;
  size_t nbAtt = names.items.size();

  try {

    time_t t0 = GetTicks();
    std::vector<Tango::DeviceAttribute> *das = d->dev->ds()->read_attributes(names.items);
    time_t t1 = GetTicks();
    if (d->root->logEnabled()) {
      std::string logStr;
      logStr.append("read_attributes(" + std::to_string(names.items.size())+" att) call: ");
      GQLUtils::Double((double)(t1 - t0) / 10.0, logStr);
      logStr.append("ms");
      d->root->log("GQLEvalDeviceAttributes::to_string", logStr);
    }

    for (auto & da : *das) {
      GQLDeviceAttribute *gqlda = new GQLDeviceAttribute(d->root, d->dev, da, false);
      gqldas.push_back(gqlda);
    }

    t0 = GetTicks();
    // Spread result(s) over children
    PTRARRAY(gqldas);
    t1 = GetTicks();
    if (d->root->logEnabled()) {
      std::string logStr;
      logStr.append("Serialisation: ");
      GQLUtils::Double((double)(t1 - t0) / 10.0, logStr);
      logStr.append("ms");
      d->root->log("GQLEvalDeviceAttributes::to_string", logStr);
    }

    // Free
    for (auto & gqlda : gqldas)
      delete gqlda;
    delete das;

  } catch (Tango::DevFailed &e) {

    // attributes is null in case of major device failure
    if (d->error == nullptr)
      d->error = new Tango::DevFailed(e);
    evalStr.append("null");

  }

}

void GQLEvalDeviceReadAttributeHist::to_string(void *node,std::string &evalStr,GQLRequestFields *children,GQLRequestArgs *args) {

  GQLDevice *d = (GQLDevice *) node;

  if (d->dev == nullptr) {
    // attributes is null is case of major device failure
    evalStr.append("null");
    return;
  }

  std::string attName = args->getString("name", "attribute_hist()");
  int depth = (int)GQLUtils::parseInt(args->getString("depth", "attribute_hist()"));

  std::vector<GQLDeviceAttribute *> gqldas;

  try {

    time_t t0 = GetTicks();
    std::vector<Tango::DeviceAttributeHistory> *hist = d->dev->ds()->attribute_history(attName,depth);
    time_t t1 = GetTicks();
    if (d->root->logEnabled()) {
      std::string logStr;
      logStr.append("read_attribute_hist(" + attName +" att) call: ");
      GQLUtils::Double((double)(t1 - t0) / 10.0, logStr);
      logStr.append("ms");
      d->root->log("GQLEvalDeviceReadAttributeHist::to_string", logStr);
    }

    for (auto & da : *hist) {
      GQLDeviceAttribute *gqlda = new GQLDeviceAttribute(d->root, d->dev, da, true);
      gqldas.push_back(gqlda);
    }

    t0 = GetTicks();
    // Spread result(s) over children
    PTRARRAY(gqldas);
    t1 = GetTicks();
    if (d->root->logEnabled()) {
      std::string logStr;
      logStr.append("Serialisation: ");
      GQLUtils::Double((double)(t1 - t0) / 10.0, logStr);
      logStr.append("ms");
      d->root->log("GQLEvalDeviceReadAttributeHist::to_string", logStr);
    }

    // Free
    for (auto & gqlda : gqldas)
      delete gqlda;
    delete hist;

  } catch (Tango::DevFailed &e) {

    // attributes is null in case of major device failure
    if (d->error == nullptr)
      d->error = new Tango::DevFailed(e);
    evalStr.append("null");

  }

}

void GQLEvalDeviceWriteAttribute::to_string(void *node, std::string &evalStr, GQLRequestFields *children, GQLRequestArgs *args) {

  GQLDevice *d = (GQLDevice *) node;

  if (d->dev == nullptr) {
    // write_attribute is null is case of major device failure
    evalStr.append("null");
    return;
  }

  std::string& attName = args->getString("name","write_attribute()");
  JSONITEM valueParam = args->get("value","write_attribute()");

  GQLMutationStatus *status = new GQLMutationStatus(d->root);
  try {

    try {

      Tango::AttributeInfoEx props;
      d->dev->getAttributeProperties(attName, props);
      switch (props.data_format) {
        case Tango::SCALAR: {
          std::string& v0 = valueParam.items[0];
          switch (props.data_type) {
            WRITE_SCALAR(DEV_BOOLEAN, DevBoolean, GQLUtils::parseBoolean(v0));
            WRITE_SCALAR(DEV_UCHAR, DevUChar, GQLUtils::parseUInt(v0));
            WRITE_SCALAR(DEV_SHORT, DevShort, GQLUtils::parseInt(v0));
            WRITE_SCALAR(DEV_USHORT, DevShort, GQLUtils::parseUInt(v0));
            WRITE_SCALAR(DEV_LONG, DevLong, GQLUtils::parseInt(v0));
            WRITE_SCALAR(DEV_ULONG, DevULong, GQLUtils::parseUInt(v0));
            WRITE_SCALAR(DEV_LONG64, DevLong64, GQLUtils::parseInt(v0));
            WRITE_SCALAR(DEV_ULONG64, DevULong64, GQLUtils::parseUInt(v0));
            WRITE_SCALAR(DEV_FLOAT, DevFloat, GQLUtils::parseDouble(v0));
            WRITE_SCALAR(DEV_DOUBLE, DevDouble, GQLUtils::parseDouble(v0));
            WRITE_SCALAR(DEV_STRING, DevString, v0.c_str());
            case Tango::DEV_ENUM:
              if(valueParam.type == JSON_STRING) {
                size_t e = findEnum(attName,v0,props.enum_labels);
                Tango::DeviceAttribute da(attName,(Tango::DevEnum)e);
                d->dev->ds()->write_attribute(da);
              } else {
                Tango::DeviceAttribute da(attName,(Tango::DevEnum)GQLUtils::parseInt(v0));
                d->dev->ds()->write_attribute(da);
              }
              break;
            case Tango::DEV_STATE:
              if(valueParam.type == JSON_STRING) {
                size_t s = findState(attName,v0);
                Tango::DeviceAttribute da(attName,(Tango::DevState)s);
                d->dev->ds()->write_attribute(da);
              } else {
                Tango::DeviceAttribute da(attName,(Tango::DevState)GQLUtils::parseInt(v0));
                d->dev->ds()->write_attribute(da);
              }
              break;
            default:
              Tango::Except::throw_exception(
                      "AttrTypeNotSupported",
                      attName + " " + std::string(Tango::CmdArgTypeName[props.data_type]) + "Scalar not supported",
                      "GQLEvalDeviceWriteAttribute");
          }
        } break;

        case Tango::SPECTRUM: {
          std::vector<std::string>& value = valueParam.items;
          switch (props.data_type) {
            WRITE_ARRAY(DEV_BOOLEAN, DevBoolean, parseBooleanArray);
            WRITE_ARRAY(DEV_UCHAR, DevUChar, parseUIntArray);
            WRITE_ARRAY(DEV_SHORT, DevShort, parseIntArray);
            WRITE_ARRAY(DEV_USHORT, DevUShort, parseUIntArray);
            WRITE_ARRAY(DEV_LONG, DevLong, parseIntArray);
            WRITE_ARRAY(DEV_ULONG, DevULong, parseUIntArray);
            WRITE_ARRAY(DEV_LONG64, DevLong64, parseIntArray);
            WRITE_ARRAY(DEV_ULONG64, DevULong64, parseUIntArray);
            WRITE_ARRAY(DEV_FLOAT, DevFloat, parseFloatArray);
            WRITE_ARRAY(DEV_DOUBLE, DevDouble, parseFloatArray);
            case Tango::DEV_STATE: {
              std::vector<Tango::DevState> v;
              if (valueParam.type == JSON_STRING) {
                for (auto & val : value) {
                  size_t s = findState(attName, val);
                  v.push_back((Tango::DevState) s);
                }
              } else {
                for (auto & val : value)
                  v.push_back((Tango::DevState) GQLUtils::parseInt(val));
              }
              Tango::DeviceAttribute da(attName, v);
              d->dev->ds()->write_attribute(da);
            } break;
            case Tango::DEV_ENUM: {
              std::vector<Tango::DevEnum> v;
              if (valueParam.type == JSON_STRING) {
                for (auto & val : value) {
                  size_t e = findEnum(attName, val,props.enum_labels);
                  v.push_back((Tango::DevEnum) e);
                }
              } else {
                for (auto & val : value)
                  v.push_back((Tango::DevEnum) GQLUtils::parseInt(val));
              }
              Tango::DeviceAttribute da(attName, v);
              d->dev->ds()->write_attribute(da);
            } break;
            case Tango::DEV_STRING: {
              Tango::DeviceAttribute da(attName, value);
              d->dev->ds()->write_attribute(da);
            } break;

            default:
              Tango::Except::throw_exception(
                      "AttrTypeNotSupported",
                      attName + " " + std::string(Tango::CmdArgTypeName[props.data_type]) + "Spectrum not supported",
                      "GQLEvalDeviceWriteAttribute");
          }

        } break;

        case Tango::IMAGE: {
          std::string& dimXParam = args->getString("dimX","write_attribute()");
          std::string& dimYParam = args->getString("dimY","write_attribute()");
          std::vector<std::string>& value = valueParam.items;
          // Check dimension
          int dimX = GQLUtils::parseInt(dimXParam);
          int dimY = GQLUtils::parseInt(dimYParam);
          if((int)value.size()!=(dimX*dimY)) {
            Tango::Except::throw_exception(
                    "AttrWrongDimension",
                    attName + " invalid argument length, dimX*dimY=" + std::to_string(dimX*dimY) + " expected",
                    "GQLEvalDeviceWriteAttribute");
          }
          // Write image
          switch (props.data_type) {
            WRITE_IMAGE(DEV_BOOLEAN, DevBoolean, parseBooleanArray,dimX,dimY);
            WRITE_IMAGE(DEV_UCHAR, DevUChar, parseUIntArray,dimX,dimY);
            WRITE_IMAGE(DEV_SHORT, DevShort, parseIntArray,dimX,dimY);
            WRITE_IMAGE(DEV_USHORT, DevUShort, parseUIntArray,dimX,dimY);
            WRITE_IMAGE(DEV_LONG, DevLong, parseIntArray,dimX,dimY);
            WRITE_IMAGE(DEV_ULONG, DevULong, parseUIntArray,dimX,dimY);
            WRITE_IMAGE(DEV_LONG64, DevLong, parseIntArray,dimX,dimY);
            WRITE_IMAGE(DEV_ULONG64, DevULong64, parseUIntArray,dimX,dimY);
            WRITE_IMAGE(DEV_FLOAT, DevFloat, parseFloatArray,dimX,dimY);
            WRITE_IMAGE(DEV_DOUBLE, DevDouble, parseFloatArray,dimX,dimY);
            case Tango::DEV_STRING: {
              Tango::DeviceAttribute da(attName, value,dimX,dimY);
              d->dev->ds()->write_attribute(da);
            } break;
            default:
              Tango::Except::throw_exception(
                      "AttrTypeNotSupported",
                      attName + " " + std::string(Tango::CmdArgTypeName[props.data_type]) + "Image not supported",
                      "GQLEvalDeviceWriteAttribute");
          }

        } break;

        default:
          Tango::Except::throw_exception(
                  "AttrTypeNotSupported",
                  attName + " " + std::string(formats[props.data_format]) + " not supported",
                  "GQLEvalDeviceWriteAttribute");
      }

    } catch (std::string &convError) {
      Tango::Except::throw_exception(
              "AttrConversionError",
              attName + " " + convError,
              "GQLEvalDeviceWriteAttribute");
    }

  } catch (Tango::DevFailed &e) {

    status->error = new Tango::DevFailed(e);

  }

  // Spread result(s) over children
  children->execute((void *) status, evalStr);

  // Free
  delete status;

}


size_t GQLEvalDeviceWriteAttribute::findEnum(std::string& attName,std::string& value,std::vector<std::string>& enumValues) {

  bool found = false;
  size_t i = 0;
  while(!found && i<enumValues.size()) {
    found = strcasecmp(value.c_str(),enumValues[i].c_str())==0;
    if(!found) i++;
  }
  if(!found)
    Tango::Except::throw_exception(
            "AttrInvalidEnum",
            attName + " '" + value + "' not a valid enum value",
            "GQLEvalDeviceWriteAttribute");

  return i;

}

size_t GQLEvalDeviceWriteAttribute::findState(std::string& attName,std::string& value) {

  bool found = false;
  size_t i = 0;
  while(!found && i<14) {
    found = strcasecmp(value.c_str(),Tango::DevStateName[i])==0;
    if(!found) i++;
  }
  if(!found)
    Tango::Except::throw_exception(
            "AttrInvalidEnum",
            attName + " '" + value + "' not a valid state value",
            "GQLEvalDeviceWriteAttribute");

  return i;

}

/** Read attribute properties */
void GQLEvalDeviceAttributeInfo::to_string(void *node, std::string &evalStr, GQLRequestFields *children, GQLRequestArgs *args) {

  static const std::string _AttributeInfo = "AttributeInfo";

  GQLDevice *d = (GQLDevice *) node;

  if (d->dev == nullptr) {
    // properties is null is case of major device failure
    evalStr.append("null");
    return;
  }

  JSONITEM names = args->get("names", "attribute_info()");

  try {

    std::vector<Tango::AttributeInfoEx> props;
    if (names.items.size() == 1 && names.items[0] == "*")
      d->dev->getAllAttributeProperties(props);
    else
      d->dev->getAttributeProperties(names.items, props);

    std::vector<GQLAttributeInfo *> gqlprops;
    size_t nbAtt = props.size();
    for (size_t attIdx = 0; attIdx < nbAtt; attIdx++) {
      GQLAttributeInfo *gqlprop = new GQLAttributeInfo(d->root,d->dev,props[attIdx],_AttributeInfo);
      gqlprops.push_back(gqlprop);
    }

    time_t t0 = GetTicks();
    // Spread result(s) over children
    PTRARRAY(gqlprops);
    time_t t1 = GetTicks();
    if (d->root->logEnabled()) {
      std::string logStr;
      logStr.append("Serialisation: ");
      GQLUtils::Double((double)(t1 - t0) / 10.0, logStr);
      logStr.append("ms");
      d->root->log("GQLEvalDeviceAttributeInfo::to_string", logStr);
    }

    // Free
    for (size_t attIdx = 0; attIdx < nbAtt; attIdx++)
      delete gqlprops[attIdx];

  } catch (Tango::DevFailed &e) {

    // properties is null in case of major device failure
    if (d->error == nullptr)
      d->error = new Tango::DevFailed(e);
    evalStr.append("null");

  }

}

/** Write attribute properties */
void GQLEvalDeviceWriteAttributeInfo::to_string(void *node, std::string &evalStr, GQLRequestFields *children, GQLRequestArgs *args) {

  static const std::string _SetAttributeInfo = "SetAttributeInfo";

  GQLDevice *d = (GQLDevice *) node;

  if (d->dev == nullptr) {
    // properties is null is case of major device failure
    evalStr.append("null");
    return;
  }

  std::string& name = args->getString("name", "write_attribute_info()");

  try {

    Tango::AttributeInfoEx props;
    d->dev->getAttributeProperties(name, props);
    GQLAttributeInfo *gqlprop = new GQLAttributeInfo(d->root,d->dev,props,_SetAttributeInfo);

    // Get all props
    children->execute((void *) gqlprop, evalStr);

    time_t t0 = GetTicks();
    std::vector<Tango::AttributeInfoEx> v;
    v.push_back(props);
    d->dev->ds()->set_attribute_config(v);
    d->dev->resetAttConfig();
    time_t t1 = GetTicks();
    if(d->root->logEnabled()) {
      d->root->log("GQLEvalDeviceWriteAttributeInfo::to_string",
      "set_attribute_config(" + props.name + ") call: " +
      std::to_string((double) (t1 - t0) / 10.0) + "ms");
    }

    // Free
    delete gqlprop;

  } catch (Tango::DevFailed &e) {

    // properties is null in case of major device failure
    if (d->error == nullptr)
      d->error = new Tango::DevFailed(e);
    evalStr.append("null");

  }

}

void GQLEvalDeviceCommandInOut::to_string(void *node, std::string &evalStr, GQLRequestFields *children,
                                         GQLRequestArgs *args) {

  GQLDevice *d = (GQLDevice *) node;

  if (d->dev == nullptr) {
    // attributes is null is case of major device failure
    evalStr.append("null");
    return;
  }

  JSONITEM name = args->get("name", "exec_command()");
  JSONITEM argin = args->get("argin", "exec_command()");
  std::string& cmdName = name.items[0];
  std::vector<std::string>& arginStr = argin.items;
  GQLMutationStatus *status = new GQLMutationStatus(d->root);
  Tango::DeviceData argout;

  time_t t0 = GetTicks();

  try {

    try {

      Tango::CommandInfo cInfo;
      d->dev->getCommandInfo(cmdName, cInfo);
      std::string& v0 = arginStr[0];

      switch (cInfo.in_type) {
        case Tango::DEV_VOID: {
          argout = d->dev->ds()->command_inout(cmdName);
        }  break;
        EXEC_CMD(DEV_BOOLEAN, (Tango::DevBoolean)GQLUtils::parseBoolean(v0));
        EXEC_CMD(DEV_SHORT, (Tango::DevShort)GQLUtils::parseInt(v0));
        EXEC_CMD(DEV_USHORT, (Tango::DevUShort)GQLUtils::parseUInt(v0));
        EXEC_CMD(DEV_LONG, (Tango::DevLong)GQLUtils::parseInt(v0));
        EXEC_CMD(DEV_ULONG, (Tango::DevULong)GQLUtils::parseUInt(v0));
        EXEC_CMD(DEV_LONG64, (Tango::DevLong64)GQLUtils::parseInt(v0));
        EXEC_CMD(DEV_ULONG64, (Tango::DevULong64)GQLUtils::parseUInt(v0));
        EXEC_CMD(DEV_FLOAT, (Tango::DevFloat)GQLUtils::parseDouble(v0));
        EXEC_CMD(DEV_DOUBLE, (Tango::DevDouble)GQLUtils::parseDouble(v0));
        EXEC_CMD(DEV_STRING, (Tango::DevString)v0.c_str());
        EXEC_CMD_ARR(DEVVAR_CHARARRAY, Tango::DevVarCharArray, (unsigned char)GQLUtils::parseUInt(arginStr[i]));
        EXEC_CMD_ARR(DEVVAR_SHORTARRAY, Tango::DevVarLongArray, (Tango::DevShort)GQLUtils::parseInt(arginStr[i]));
        EXEC_CMD_ARR(DEVVAR_USHORTARRAY, Tango::DevVarULongArray, (Tango::DevUShort)GQLUtils::parseUInt(arginStr[i]));
        EXEC_CMD_ARR(DEVVAR_LONGARRAY, Tango::DevVarLongArray, (Tango::DevLong)GQLUtils::parseInt(arginStr[i]));
        EXEC_CMD_ARR(DEVVAR_ULONGARRAY, Tango::DevVarULongArray, (Tango::DevULong)GQLUtils::parseUInt(arginStr[i]));
        EXEC_CMD_ARR(DEVVAR_LONG64ARRAY, Tango::DevVarLong64Array, (Tango::DevLong64)GQLUtils::parseInt(arginStr[i]));
        EXEC_CMD_ARR(DEVVAR_ULONG64ARRAY, Tango::DevVarULong64Array, (Tango::DevULong64)GQLUtils::parseUInt(arginStr[i]));
        EXEC_CMD_ARR(DEVVAR_FLOATARRAY, Tango::DevVarFloatArray, (Tango::DevFloat)GQLUtils::parseDouble(arginStr[i]));
        EXEC_CMD_ARR(DEVVAR_DOUBLEARRAY, Tango::DevVarDoubleArray, (Tango::DevDouble)GQLUtils::parseDouble(arginStr[i]));
        EXEC_CMD_ARR(DEVVAR_STRINGARRAY, Tango::DevVarStringArray, (Tango::DevString)::strdup(arginStr[i].c_str()));
        case Tango::DEVVAR_LONGSTRINGARRAY: {

          if(argin.type!=JSON_OBJECT)
            Tango::Except::throw_exception(
                    "AttrWrongType",
                    cmdName + " Invalid " + std::string(Tango::CmdArgTypeName[cInfo.in_type]) + ", use {long:[1,2],std::string:['a','b','c']} syntax",
                    "GQLEvalDeviceExecCommand");

          JSON json;
          try {
            Parser p(v0, cmdName + " input argument");
            p.parseJSON(json);
          } catch (std::string& err) {
            Tango::Except::throw_exception(
                    "AttrWrongType",
                    cmdName + " Invalid " + std::string(Tango::CmdArgTypeName[cInfo.in_type]) + " "+err+", use {long:[1,2],std::string:['a','b','c']} syntax",
                    "GQLEvalDeviceExecCommand");

          }

          if(json.find("long")==json.end() || json.find("std::string")==json.end())
            Tango::Except::throw_exception(
                    "AttrWrongType",
                    cmdName + " Invalid " + std::string(Tango::CmdArgTypeName[cInfo.in_type]) + ", use {long:[1,2],std::string:['a','b','c']} syntax",
                    "GQLEvalDeviceExecCommand");

          JSONITEM& lA = json["long"];
          JSONITEM& lS = json["std::string"];

          Tango::DeviceData _argin;
          Tango::DevVarLongStringArray v;

          v.lvalue.length(lA.items.size());
          for(size_t i=0;i<lA.items.size();i++)
            v.lvalue[i] = (Tango::DevLong)GQLUtils::parseInt(lA.items[i]);

          v.svalue.length(lS.items.size());
          for(size_t i=0;i<lS.items.size();i++)
            v.svalue[i] = strdup(lS.items[i].c_str());

          _argin << v;

          argout = d->dev->ds()->command_inout(cmdName,_argin);

        } break;

        default:
          Tango::Except::throw_exception(
                  "AttrTypeNotSupported",
                  cmdName + " in_type " + std::string(Tango::CmdArgTypeName[cInfo.in_type]) + " not supported",
                  "GQLEvalDeviceExecCommand");
      }

      status->dd = &argout;

    } catch (std::string &convError) {
      Tango::Except::throw_exception(
              "CommandConversionError",
              cmdName + " " + convError,
              "GQLEvalDeviceExecCommand");
    }

  } catch (Tango::DevFailed &e) {

    status->error = new Tango::DevFailed(e);

  }

  time_t t1 = GetTicks();
  if (d->root->logEnabled())
    d->root->log("GQLEvalDeviceExecCommand::to_string", "command_inout("+cmdName+") call: " + std::to_string((double)(t1 - t0) / 10.0) + "ms");

  t0 = GetTicks();
  // Spread result(s) over children
  children->execute((void *) status, evalStr);
  t1 = GetTicks();
  if (d->root->logEnabled()) {
    std::string logStr;
    logStr.append("Serialisation: ");
    GQLUtils::Double((double)(t1 - t0) / 10.0, logStr);
    logStr.append("ms");
    d->root->log("GQLEvalDeviceAttributes::to_string", logStr);
  }

  // Free
  delete status;

}

void GQLEvalDeviceCommandInfo::to_string(void *node, std::string &evalStr, GQLRequestFields *children,
                                             GQLRequestArgs *args) {

  GQLDevice *d = (GQLDevice *) node;

  if (d->dev == nullptr) {
    // properties is null is case of major device failure
    evalStr.append("null");
    return;
  }

  JSONITEM names = args->get("names", "cmd_info()");

  try {

    std::vector<Tango::CommandInfo> props;
    if (names.items.size() == 1 && names.items[0] == "*")
      d->dev->getAllCommandInfo(props);
    else
      d->dev->getCommandInfo(names.items, props);

    std::vector<GQLCommandInfo *> gqlprops;
    size_t nbCmd = props.size();
    for (size_t cmdIdx = 0; cmdIdx < nbCmd; cmdIdx++) {
      GQLCommandInfo *gqlprop = new GQLCommandInfo(d->root, props[cmdIdx]);
      gqlprops.push_back(gqlprop);
    }

    time_t t0 = GetTicks();
    // Spread result(s) over children
    PTRARRAY(gqlprops);
    time_t t1 = GetTicks();
    if (d->root->logEnabled()) {
      std::string logStr;
      logStr.append("Serialisation: ");
      GQLUtils::Double((double)(t1 - t0) / 10.0, logStr);
      logStr.append("ms");
      d->root->log("GQLEvalDeviceCommandInfo::to_string", logStr);
    }

    // Free
    for (size_t cmdIdx = 0; cmdIdx < nbCmd; cmdIdx++)
      delete gqlprops[cmdIdx];

  } catch (Tango::DevFailed &e) {

    // properties is null in case of major device failure
    if (d->error == nullptr)
      d->error = new Tango::DevFailed(e);
    evalStr.append("null");

  }

}

} // namespace TangoGQL_ns
