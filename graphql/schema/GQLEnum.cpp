//
// GQLEnum object
//

#include "../GQLEngine.h"


namespace TangoGQL_ns {

GQLEnum::GQLEnum():GQLBase(nullptr) {
}

std::string GQLEnum::to_string() const {

  std::string ret;

  if(description.length()>0) {
    ret.append("  #");
    ret.append(description);
    ret.push_back('\n');
  }
  ret.append("  ");
  ret.append(name);

  return ret;

}

void GQLEnum::parse(Parser* p) {

  p->jumpSpace();
  description = p->getLastComment();
  p->readWord(name);
  isDeprecated = false;

}


} // namespace TangoGQL_ns