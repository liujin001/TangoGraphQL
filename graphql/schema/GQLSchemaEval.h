//
// GQL Schema eval functions
//

#ifndef TANGOGQL_GQLSCHEMAEVAL_H
#define TANGOGQL_GQLSCHEMAEVAL_H

#include "GQLSchema.h"
#include "../utils/GQLUtils.h"

namespace TangoGQL_ns {

class GQLSchema;

BEGIN_EVAL(GQLEvalSchemaDescription)
    GQLSchema *schema = (GQLSchema *)node;
    GQLUtils::quoteOrNull(schema->description,evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalSchemaTypes)
    GQLSchema *schema = (GQLSchema *)node;
    PTRARRAY(schema->types);
END_EVAL

BEGIN_EVAL(GQLEvalSchemaQueryType)
    GQLSchema *schema = (GQLSchema *)node;
    children->execute((void *) schema->getQueryType(), evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalSchemaMutationType)
    GQLSchema *schema = (GQLSchema *)node;
    children->execute((void *) schema->getMutationType(), evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalSchemaSubscriptionType)
    GQLSchema *schema = (GQLSchema *)node;
    children->execute((void *) schema->getSubscriptionType(), evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalSchemaDirectives)
    evalStr.append("[]");
END_EVAL

BEGIN_EVAL(GQLEvalTyeName)
    GQLBase *b = (GQLBase *)node;
    GQLUtils::quote(b->__typename(),evalStr);
END_EVAL

// Meta query
// __schema: __Schema!
DECLARE_EVAL(GQLEvalQuery__schema)
END_DECLARE_EVAL

} // namespace TangoGQL_ns

#endif //TANGOGQL_GQLSCHEMAEVAL_H
