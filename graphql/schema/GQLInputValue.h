//
// GQLInputValue object
//

#ifndef TANGOGQL_GQLINPUTVALUE_H
#define TANGOGQL_GQLINPUTVALUE_H

#include "graphql/schema/GQLType.h"

namespace TangoGQL_ns {

class GQLType;
class GQLSchema;

class GQLInputValue: public GQLBase {

public:

  GQLInputValue();
  ~GQLInputValue();
  GQLInputValue *clone() const;

  GQLType                 *type;
  std::string              name;
  std::string              description;
  std::vector<std::string> defaultValue;

  std::string to_string() const;
  void parse(GQLSchema *schema,Parser* parser);

  std::string __typename() {
    return "__InputValue";
  }



};

} // namespace TangoGQL_ns


#endif //TANGOGQL_GQLINPUTVALUE_H
