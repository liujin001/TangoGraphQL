//
// GQLSchema object
//


#include "../GQLEngine.h"
#include "GQLSchemaEval.h"


namespace TangoGQL_ns {

GQLSchema::GQLSchema():GQLBase(nullptr) {
  queryType = nullptr;
  mutationType = nullptr;
  subscriptionType = nullptr;
}

GQLSchema::~GQLSchema() {
  // Destroy all root types
  for(auto & type : types) {
    delete type;
  }
}

GQLType *GQLSchema::getType(const std::string& typeName) const {
  bool found = false;
  size_t i = 0;
  while(!found && i<types.size()) {
    found = types[i]->name == typeName;
    if(!found) i++;
  }
  return (found)?types[i]: nullptr;
}

GQLType *GQLSchema::getQueryType() {
  if(queryType==nullptr) {
    queryType = getType("Query");
    if(queryType==nullptr)
      throw std::string("GraphQL schema cannot have a schema without Query");
  }
  return queryType;
}

GQLType *GQLSchema::getMutationType() {
  if(mutationType==nullptr)
    mutationType = getType("Mutation");
  return mutationType;
}

GQLType *GQLSchema::getSubscriptionType() {
  if(subscriptionType==nullptr)
    subscriptionType = getType("Subscription");
  return subscriptionType;
}

GQLField *GQLSchema::getQuery(const std::string& queryName) const {

  GQLType *qType = getType(" Query");

  bool found = false;
  size_t i = 0;
  while(!found && i<qType->fields.size()) {
    found = qType->fields[i]->name == queryName;
    if(!found) i++;
  }
  if(!found)
    throw std::string("Query " + queryName + " not defined");

  return qType->fields[i];

}

std::string GQLSchema::to_string() const {

  std::string ret;

  for(auto & type : types)
    ret.append(type->to_string(true));

  return ret;

}

void GQLSchema::parse(Parser* p) {

  // A schema is a list of type
  // For this GQL engine query type is Query, mutation is Mutation and subscription is Subscription

  bool eof = false;
  while(!eof) {

    p->jumpSpace();
    int pos = p->getPosMarker();
    std::string typeDef;
    p->readWord(typeDef);
    if( typeDef!="type" &&
        typeDef!="enum" &&
        typeDef!="scalar" &&
        typeDef!="interface")
      throw std::string("Unexpected type definition " + typeDef + " " + p->getErrorLocation(pos));

    std::string typeDesc = p->getLastComment();
    pos = p->getPosMarker();
    std::string typeName;
    p->readWord(typeName);

    GQLType *a = getType(typeName);
    if( a && a->kind!=GQLTYPEKIND::INCOMPLETE ) {
      // Type already defined
      throw std::string("Type " + typeName + " already defined " + p->getErrorLocation(pos));
    } else {
      if(!a) {
        a = new GQLType(typeName,GQLTYPEKIND::INCOMPLETE);
        types.push_back(a);
      }
    }
    a->description = typeDesc;
    a->parse(this,p,typeDef);

    eof = p->eof();

  }

  // Check for incomplete type
  bool ok = true;
  size_t i = 0;
  while(ok && i<types.size()) {
    ok = (types[i]->kind != GQLTYPEKIND::INCOMPLETE);
    if(ok) i++;
  }
  if( !ok )
    throw std::string("Type " + types[i]->name + " has incomplete definition");

  // Add hidden field __typeName keyword to non scalar field
  for(i=0;i<types.size();i++) {
    if(types[i]->kind != GQLTYPEKIND::SCALAR &&
       types[i]->kind != GQLTYPEKIND::ENUM) {
      GQLField *f = new GQLField();
      const std::string str = "String!";
      Parser pp(str, "GQLSchema::parse");
      f->isHidden = true;
      f->type = parseTypeUsage(&pp);
      f->name = "__typename";
      f->description = "The name of the current Object type at runtime.";
      f->evalFunction = new GQLEvalTyeName();
      types[i]->addField(f);
    }
    types[i]->updateVisibleFields();
  }

}


GQLType *GQLSchema::parseTypeUsage(Parser* p) {

  // TODO: Implements more complex type

  // Manage only one level of LIST and NON_NULL

  bool nonNull = false;
  bool isList = false;
  bool nonNullList = false;

  p->jumpSpace();
  if (p->current() == '[') {
    isList = true;
    p->jumpSep('[');
  }
  std::string typeName;
  p->readWord(typeName);
  if (p->current() == '!') {
    p->jumpSep('!');
    nonNull = true;
  }
  if (isList)
    p->jumpSep(']');
  p->jumpSpace();
  if (p->current() == '!') {
    p->jumpSep('!');
    nonNullList = true;
  }

  // Root type
  GQLType *root = getType(typeName);
  if (root == nullptr) {
    //Create an incomplete root type
    root = new GQLType(typeName,GQLTYPEKIND::INCOMPLETE);
    types.push_back(root);
  }

  GQLType *ret = root;
  int ofTypeLgth = 0;

  // Create the ofType chain
  if (nonNull) {
    ret = new GQLType("",GQLTYPEKIND::NON_NULL);
    ret->ofType = root;
    root = ret;
    ofTypeLgth++;
  }

  if (isList) {
    ret = new GQLType("",GQLTYPEKIND::LIST);
    ret->ofType = root;
    root = ret;
    ofTypeLgth++;
  }

  if (nonNullList) {
    ret = new GQLType("",GQLTYPEKIND::NON_NULL);
    ret->ofType = root;
    root = ret;
    ofTypeLgth++;
  }

  ret->ofTypeLength = ofTypeLgth;
  return ret;

}

void GQLEvalQuery__schema::to_string(void *node,std::string &evalStr,GQLRequestFields *children,GQLRequestArgs *args) {
  children->execute((void *) GQLEngine::mainSchema, evalStr);
}

} // namespace TangoGQL_ns
