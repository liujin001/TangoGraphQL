//
// GQLEnum object
//

#ifndef TANGOGQL_GQLENUM_H
#define TANGOGQL_GQLENUM_H
#include "GQLBase.h"
#include <iostream>
#include <vector>

namespace TangoGQL_ns {

class GQLEngine;

/** GraphQL definition of an enum type */
class GQLEnum: public GQLBase {

public:

  GQLEnum();

  std::string name;
  std::string description;
  bool isDeprecated;
  std::string deprecationReason;

  std::string to_string() const;
  void parse(Parser* parser);

  std::string __typename() {
    return "__EnumValue";
  }

};

} // namespace TangoGQL_ns

#endif //TANGOGQL_GQLENUM_H
