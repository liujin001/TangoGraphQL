//
// GQL InputValue eval functions
//

#ifndef TANGOGQL_GQLINPUTVALUEEVAL_H
#define TANGOGQL_GQLINPUTVALUEEVAL_H

#include "GQLType.h"
#include "../utils/GQLUtils.h"

namespace TangoGQL_ns {

class GQLInputValue;

BEGIN_EVAL(GQLEvalInputValueName)
    GQLInputValue *val = (GQLInputValue *)node;
    GQLUtils::quote(val->name,evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalInputValueDescription)
    GQLInputValue *val = (GQLInputValue *)node;
    GQLUtils::quoteOrNull(val->description,evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalInputValueType)
    GQLInputValue *val = (GQLInputValue *)node;
    children->execute((void *)  val->type, evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalInputValueDefaultValue)
    GQLInputValue *val = (GQLInputValue *)node;
    if(val->defaultValue.empty()) {
      evalStr.append("null");
    } else {
      bool isString = val->type->toRoot()->name == "String";
      bool isList = val->type->isList();
      evalStr.push_back('\"');
      if(isList) evalStr.push_back('[');
      for(size_t i=0;i<val->defaultValue.size();i++) {
        if(isString) evalStr.append(R"(\")");
        evalStr.append(val->defaultValue[i]);
        if(isString) evalStr.append(R"(\")");
        if(i<val->defaultValue.size()-1) evalStr.push_back(',');
      }
      if(isList) evalStr.push_back(']');
      evalStr.push_back('\"');
    }
END_EVAL

} // namespace TangoGQL_ns

#endif //TANGOGQL_GQLINPUTVALUEEVAL_H
