//
// GQLInputArgs object
//
#include "../GQLEngine.h"


namespace TangoGQL_ns {

GQLInputArgs::GQLInputArgs() = default;

GQLInputArgs::~GQLInputArgs() {
  for(auto & arg : args)
    delete arg;
}

bool GQLInputArgs::empty() const {
  return args.empty();
}

// Parse a input args of a field (if any)
void GQLInputArgs::parse(GQLSchema *schema,Parser* p) {

  p->jumpSpace();
  if (p->current() == '(') {
    // Parse args
    p->jumpSep('(');
    bool endArg = false;
    while (!endArg) {
      GQLInputValue *arg = new GQLInputValue();
      arg->parse(schema,p);
      args.push_back(arg);
      p->jumpSpace();
      endArg = (p->current() == ')') || (p->current() == 0);
      if (!endArg) if(p->current() == ',') p->jumpSep(','); // Allow args to be separated or not by a coma
    }
    p->jumpSep(')');
  }

}

// Print gql input args (if any)
std::string GQLInputArgs::to_string() const {

  std::string ret;
  if(!args.empty()) {
    ret.push_back('(');
    for(size_t i=0;i<args.size();i++) {
      ret.append(args[i]->to_string());
      if(i<args.size()-1)
        ret.append(",");
    }
    ret.push_back(')');
  }
  return ret;

}






} // namespace TangoGQL_ns
