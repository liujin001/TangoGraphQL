//
// GQLBase superclass object
// Handle common property ot node
//

#ifndef TANGOGQL_GQLBASE_H
#define TANGOGQL_GQLBASE_H

namespace TangoGQL_ns {
class GQLEngine;

class GQLBase {
public:
  explicit GQLBase(GQLEngine *engine):root(engine) {}
  virtual std::string __typename() = 0;
  GQLEngine *root;
};

} // namespace TangoGQL_ns

#endif //TANGOGQL_GQLBASE_H
