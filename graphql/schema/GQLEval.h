//
// GQLEval object
//

#ifndef TANGOGQL_GQLEVAL_H
#define TANGOGQL_GQLEVAL_H
#include "graphql/request/GQLRequestArgs.h"
#include "graphql/request/GQLRequestFields.h"
#include "GQLType.h"
#include "TangoGQL.h"
#include "graphql/utils/GQLUtils.h"

namespace TangoGQL_ns {

extern time_t GetTicks();
extern const char * severities[];

#define PTRARRAYORNULL(arr) {                            \
  if((arr).empty()) {                                    \
    evalStr.append("null");                              \
  } else {                                               \
    evalStr.push_back('[');                              \
    for (size_t i = 0; i < (arr).size(); i++) {          \
      children->execute((void *) (arr)[i], evalStr);     \
      if (i < (arr).size() - 1) evalStr.push_back(',');  \
    }                                                    \
    evalStr.push_back(']');                              \
  }                                                      \
}

#define PTRARRAY(arr) {                                  \
    evalStr.push_back('[');                              \
    for (size_t i = 0; i < (arr).size(); i++) {          \
      children->execute((void *) (arr)[i], evalStr);     \
      if (i < (arr).size() - 1) evalStr.push_back(',');  \
    }                                                    \
    evalStr.push_back(']');                              \
  }

#define BEGIN_EVAL(className)                           \
class className: public GQLEval {                       \
public:                                                 \
  void to_string(void *node,std::string &evalStr,       \
  GQLRequestFields *children,GQLRequestArgs *args) {

#define END_EVAL }};

#define DECLARE_EVAL(className)                         \
class className: public GQLEval {                       \
public:                                                 \
  void to_string(void *node,std::string &evalStr,       \
  GQLRequestFields *children,GQLRequestArgs *args);
#define END_DECLARE_EVAL };


class GQLEval {

public:

  /** Main evaluation function
   * @param node Parent node
   * @param evalStr The serialisation string
   * @param children Children list
   * @param args Arguments list
   */
  virtual void to_string(void *node,std::string &evalStr,GQLRequestFields *children,GQLRequestArgs *args) = 0;

  /**
   * Subscription evaluation function for subscription node
   * @param node Parent node
   * @param children Children list
   * @param args Arguments list
   */
  virtual GQLSubscription *subscription(void *node,GQLRequestFields *children,GQLRequestArgs *args) {
    return NULL;
  }

  void appendError(const Tango::DevErrorList &errors,GQLRequestArgs *args,std::string &evalStr) {

    std::string& getStack = args->getString("getStack", "error()");
    evalStr.push_back('[');
    if(getStack=="true") {
      for(size_t i=0;i<errors.length();i++) {
        GQLUtils::quote( "severity:"+std::string(severities[errors[i].severity]),evalStr,true);
        evalStr.push_back(',');
        GQLUtils::quote("reason:" + std::string(errors[i].reason),evalStr,true);
        evalStr.push_back(',');
        GQLUtils::quote("desc:" + std::string(errors[i].desc),evalStr,true);
        evalStr.push_back(',');
        GQLUtils::quote("origin:" + std::string(errors[i].origin),evalStr,true);
        if(i<errors.length()-1) evalStr.push_back(',');
      }
    } else {
      GQLUtils::quote(errors[0].desc.in(), evalStr, true);
    }
    evalStr.push_back(']');

  }

};

} // namespace TangoGQL_ns

#endif //TANGOGQL_GQLEVAL_H
