//
// GQLInputValue object
//

#include "../GQLEngine.h"


namespace TangoGQL_ns {

GQLInputValue::GQLInputValue():GQLBase(nullptr) {
  type = nullptr;
}

GQLInputValue::~GQLInputValue() {
  GQLType::DestroyOfTypeChain(type);
}

GQLInputValue *GQLInputValue::clone() const {

  GQLInputValue *newObject = new GQLInputValue();
  newObject->name = name;
  newObject->defaultValue = defaultValue;
  newObject->description = description;
  newObject->type = GQLType::CloneOfTypeChain(type);
  return newObject;

}

std::string GQLInputValue::to_string() const {

    std::string ret;
    ret.append(name);
    ret.push_back(':');
    ret.append(type->to_string(false));
    if(defaultValue.size()>1) {
      ret.push_back('=');
      GQLUtils::stringArr(defaultValue,ret);
    } else if(defaultValue.size()==1) {
      ret.push_back('=');
      ret.append(defaultValue[0]);
    }
    return ret;

}

void GQLInputValue::parse(GQLSchema *schema,Parser *p) {

  p->readWord(name);
  p->jumpSep(':');
  type = schema->parseTypeUsage(p);
  p->jumpSpace();
  if(p->current()=='=') {
    if(type->kind==GQLTYPEKIND::NON_NULL)
      throw std::string("Non reachable default value (NON NULL) at " + p->getErrorLocation(p->getPosMarker()));
    p->jumpSep('=');
    p->jumpSpace();
    if(p->current()=='[') {
      p->parseArray(defaultValue);
    } else {
      std::string v0;
      p->readWord(v0);
      defaultValue.push_back(v0);
    }
  }

}

} // namespace TangoGQL_ns
