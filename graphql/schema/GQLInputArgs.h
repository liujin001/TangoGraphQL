//
// GQLInputArgs object
//

#ifndef TANGOGQL_GQLINPUTARGS_H
#define TANGOGQL_GQLINPUTARGS_H
#include "graphql/schema/GQLInputValue.h"

namespace TangoGQL_ns {

class GQLInputArgs {

public:
  GQLInputArgs();
  ~GQLInputArgs();

  std::vector<GQLInputValue *> args;

  std::string to_string() const;
  void parse(GQLSchema *schema,Parser* parser);
  bool empty() const;

};

} // namespace TangoGQL_ns

#endif //TANGOGQL_GQLINPUTARGS_H
