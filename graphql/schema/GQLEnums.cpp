//
// GQLEnums object
//

#include "../GQLEngine.h"


namespace TangoGQL_ns {

GQLEnums::GQLEnums() = default;

GQLEnums::~GQLEnums() {
  for(auto & i : enums)
    delete i;
}

std::string GQLEnums::to_string() const {

  std::string ret;
  bool hasEnum = !enums.empty();

  if(hasEnum) ret.append("{\n");
  for(auto i : enums) {
    ret.append(i->to_string());
    ret.push_back('\n');
  }
  if(hasEnum) ret.append("}\n");

  return ret;

}

void GQLEnums::parse(Parser* p) {

  p->jumpSep('{');
  bool endItems = false;
  while(!endItems) {
    GQLEnum *e = new GQLEnum();
    e->parse(p);
    enums.push_back(e);
    endItems = p->endOf('}');
  }

  p->jumpSep('}');

}


} // namespace TangoGQL_ns