//
// GQL field eval functions
//

#ifndef TANGOGQL_GQLFIELDEVEL_H
#define TANGOGQL_GQLFIELDEVEL_H

#include "GQLType.h"
#include "../utils/GQLUtils.h"

namespace TangoGQL_ns {

class GQLField;

BEGIN_EVAL(GQLEvalFieldName)
    GQLField *field = (GQLField *)node;
    GQLUtils::quote(field->name,evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalFieldDescription)
    GQLField *field = (GQLField *)node;
    GQLUtils::quoteOrNull(field->description,evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalFieldArgs)
    GQLField *field = (GQLField *)node;
    PTRARRAY(field->args.args);
END_EVAL

BEGIN_EVAL(GQLEvalFieldType)
    GQLField *field = (GQLField *)node;
    children->execute((void *) field->type, evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalFieldIsDeprecated)
    GQLField *field = (GQLField *)node;
    if(field->isDeprecated)
      evalStr.append("true");
    else
      evalStr.append("false");
END_EVAL

BEGIN_EVAL(GQLEvalFieldDepreciationReason)
    GQLField *field = (GQLField *)node;
    GQLUtils::quoteOrNull(field->deprecationReason,evalStr);
END_EVAL


} // namespace TangoGQL_ns

#endif //TANGOGQL_GQLFIELDEVEL_H
