//
// GQLType object
//

#include "../GQLEngine.h"


namespace TangoGQL_ns {

const char* GQLTYPEKIND_STR[] = {"SCALAR","OBJECT","INTERFACE","UNION","ENUM","INPUT_OBJECT","LIST","NON_NULL"};

//static int count = 0;
GQLType::GQLType(const std::string& name,GQLTYPEKIND kind):GQLBase(nullptr) {
  //count++;
  //std::cout << "GQLType:" << count << std::endl;
  ofType = nullptr;
  ofTypeLength = 0;
  this->kind = kind;
  this->name = name;
}

GQLType::~GQLType() {
 //count--;
 //std::cout << "GQLType(" << name << "):" << count << " lgth:" << ofTypeLength << std::endl;
 for(auto & field : fields)
    delete field;
}

void GQLType::DestroyOfTypeChain(GQLType *type) {

  if(type) {
    int length = type->ofTypeLength;
    for (int i = 0; i <length ; i++) {
      GQLType *nt = type->ofType;
      delete type;
      type = nt;
    }
  }

}

GQLType * GQLType::CloneOfTypeChain(GQLType *type) {

  if(type) {
    if( type->ofTypeLength==0 ) {
      // Root type
      return type;
    } else {
      GQLType *ret = new GQLType("",type->kind);
      int length = type->ofTypeLength;
      ret->ofTypeLength = length;
      GQLType *t = ret;
      for (int i = 0; i < length-1; i++) {
        t->ofType = new GQLType("",type->ofType->kind);
        t = t->ofType;
        type = type->ofType;
      }
      t->ofType = type->toRoot();
      return ret;
    }
  } else {
    return nullptr;
  }

}

void GQLType::updateVisibleFields() {

  visibleFields.clear();
  for(auto & field : fields) {
    if(!field->isHidden)
      visibleFields.push_back(field);
  }

}

bool GQLType::hasFields() const {
  return !fields.empty();
}

bool GQLType::isList() const {

  const GQLType *t = this;
  bool found = false;
  while(!found && t!= nullptr) {
    found = t->kind == GQLTYPEKIND::LIST;
    if(!found) t = t->ofType;
  }
  return found;

}

std::string GQLType::to_string(bool isDefinition) const {

  std::string ret;

  if(isDefinition) {

    // Root type
    if(description.length()>0) {
      ret.push_back('#');
      ret.append(description);
      ret.push_back('\n');
    }

    if(kind==GQLTYPEKIND::ENUM) {
      ret.append("enum ");
    } else if (kind==GQLTYPEKIND::INTERFACE) {
      ret.append("interface ");
    } else {
      ret.append("type ");
    }
    ret.append(name);
    ret.append(enumValues.to_string());

    bool hasFields = !fields.empty();
    if(hasFields) ret.append(" {\n");
    for(auto & field : fields)
      ret.append(field->to_string());
    if(hasFields) ret.append("}\n");

    ret.push_back('\n');

  } else {

    // Non root type
    const GQLType *type = this;
    std::vector<GQLTYPEKIND> kinds;
    while(type->name.length()==0) {
      kinds.push_back(type->kind);
      type = type->ofType;
    }
    ret = type->name;

    int i = (int)kinds.size() - 1;
    while(i>=0) {
      switch(kinds[i]) {
        case GQLTYPEKIND::NON_NULL:
          ret.append("!");
          break;
        case GQLTYPEKIND::LIST:
          ret.insert(ret.begin(),'[');
          ret.append("]");
          break;
        default:
          break;
      }
      i--;
    }

    return ret;

  }

  return ret;

}

void GQLType::parse(GQLSchema *schema,Parser* p,std::string& def) {

  if( def=="type" || def=="interface" ) {

    if( def=="type" )
      kind=GQLTYPEKIND::OBJECT;
    else
      kind=GQLTYPEKIND::INTERFACE;

    std::vector<GQLField *> fieldToAdd;

    p->jumpSpace();
    if(p->current()=='e') {
      // Out of GraphQL standard
      p->jumpSep("extends");
      bool end = false;
      while(!end) {
        std::string superClass;
        p->readWord(superClass);
        GQLType *t = schema->getType(superClass);
        if(t== nullptr)
          throw std::string("Undefined super class " + superClass);

        // clone fields
        for(auto & f : t->fields)
          fieldToAdd.push_back(f->clone());

        p->jumpSpace();
        end = p->current()!=',';
        if(!end) p->jumpSep(',');
      }
    }

    // Parse fields
    p->jumpSep('{');
    bool endItems = false;
    while(!endItems) {
      auto *f = new GQLField();
      f->parse(schema,p);
      fields.push_back(f);
      endItems = p->endOf('}');
    }
    p->jumpSep('}');

    // Add super class fields at the end
    for(auto & f : fieldToAdd)
      fields.push_back(f);

  } else if ( def=="enum" ) {

    kind=GQLTYPEKIND::ENUM;
    enumValues.parse(p);

  } else if ( def=="scalar" ) {

    kind=GQLTYPEKIND::SCALAR;

  } else {

    throw std::string("GQLType: Unexpected (or unsupported) type definition " + def);

  }


}

bool GQLType::addField(GQLField *f) {

  bool found = false;
  size_t i = 0;
  while(!found && i<fields.size()) {
    found = fields[i]->name==f->name;
    if(!found) i++;
  }
  if(!found)
    fields.push_back(f);
  return !found;

}

GQLType *GQLType::toRoot() const {

  const GQLType *t = this;
  while(t->name.length()==0)
    t = t->ofType;
  return (GQLType *)t;

}

GQLField *GQLType::getField(const std::string& fieldName) const {

  bool found = false;
  size_t i = 0;
  while(!found && i<fields.size()) {
    found = fields[i]->name==fieldName;
    if(!found) i++;
  }
  if(found)
    return fields[i];
  else
    return nullptr;

}


std::string GQLType::GetKind(GQLTYPEKIND k) {
  return GQLTYPEKIND_STR[k];
}

void GQLEvalQuery__type::to_string(void *node,std::string &evalStr,GQLRequestFields *children,GQLRequestArgs *args) {
  GQLType *type = GQLEngine::mainSchema->getType(args->getString("name","__type"));
  children->execute((void *) type, evalStr);
}

} // namespace TangoGQL_ns