//
// GQLField object
//

#include "../GQLEngine.h"


namespace TangoGQL_ns {

GQLField::GQLField():GQLBase(nullptr) {
  type = nullptr;
  evalFunction = nullptr;
  isHidden = false;
  isDeprecated = false;
}

GQLField::~GQLField() {
  GQLType::DestroyOfTypeChain(type);
  delete evalFunction;
}

GQLField *GQLField::clone() const {
  GQLField *newObject = new GQLField();
  newObject->type = GQLType::CloneOfTypeChain(type);
  for(auto arg : args.args)
    newObject->args.args.push_back(arg->clone());
  newObject->description = description;
  newObject->name = name;
  newObject->isDeprecated = isDeprecated;
  newObject->deprecationReason = deprecationReason;
  newObject->isHidden = newObject->isHidden;
  return newObject;
}

std::string GQLField::to_string() const {

  std::string ret;

  if(description.length()>0) {
    ret.append("  #");
    ret.append(description);
    ret.push_back('\n');
  }
  ret.append("  ");
  ret.append(name);
  ret.append(args.to_string());
  ret.push_back(':');
  ret.append(type->to_string(false));
  ret.push_back('\n');

  return ret;

}

void GQLField::parse(GQLSchema *schema,Parser* p) {

  p->jumpSpace();
  description = p->getLastComment();
  p->readWord(name);
  // Special graphQL function
  if(name.substr(0,2)=="__")
    isHidden = true;
  isDeprecated = false;
  args.parse(schema,p);
  p->jumpSep(':');
  type = schema->parseTypeUsage(p);

}


} // namespace TangoGQL_ns