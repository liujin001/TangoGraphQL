//
// GQLField object
//

#ifndef TANGOGQL_GQLFIELD_H
#define TANGOGQL_GQLFIELD_H
#include "GQLType.h"
#include "GQLInputArgs.h"
#include "GQLEval.h"

namespace TangoGQL_ns {

class GQLEngine;
class GQLType;

/** GraphQL definition of a filed type, field are methods of an object */
class GQLField: public GQLBase {

public:

  GQLField();
  ~GQLField();
  GQLField *clone() const;

  GQLType *type; // field output type
  std::string name;
  std::string description;
  GQLInputArgs args;
  bool isDeprecated;
  std::string deprecationReason;
  GQLEval *evalFunction;
  bool isHidden;

  std::string to_string() const;
  void parse(GQLSchema *schema,Parser* parser);

  std::string __typename() {
    return "__Field";
  }

};

} // namespace TangoGQL_ns

#endif //TANGOGQL_GQLFIELD_H
