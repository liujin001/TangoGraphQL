//
// GQLSubscription object
//

#include "GQLSubscription.h"
#include "GQLDeviceAttributeFrame.h"
#include "../webserver/WSFrame.h"

namespace TangoGQL_ns {

// -------------------------------------------------------------------------------------
// GQLSubscription
// -------------------------------------------------------------------------------------

GQLSubscription::GQLSubscription(DeviceFactory *factory,GQLRequestFields *req) {
  this->factory = factory;
  this->req = req;
  this->mainRequest = nullptr;
}

GQLSubscription::~GQLSubscription() {
  for(auto & callback : callbacks)
    delete callback;
  delete mainRequest;
}

void GQLSubscription::unsubscribe() {
  for(auto & callback : callbacks)
    callback->unsubscribe();
}

std::string GQLSubscription::getUUID() {
  return factory->getConn()->getUUID();
}

void GQLSubscription::setMainRequest(GQLJson *request) {
  mainRequest = request;
}

DeviceFactory *GQLSubscription::getFactory() {
  return factory;
}

GQLRequestFields *GQLSubscription::getRequest() {
  return req;
}

void GQLSubscription::addCallback(TangoEventHandler *callback) {
  callbacks.push_back(callback);
}

// -------------------------------------------------------------------------------------
// TangoEventHandler
// -------------------------------------------------------------------------------------

TangoEventHandler::TangoEventHandler(GQLEngine *root,GQLSubscription *parent,int index,std::string& devName,std::string& attName) {
  this->dev = nullptr;
  this->subscribeId = -1;
  this->root = root;
  this->parent = parent;
  this->index = index;
  this->devName = devName;
  this->attName = attName;
}

void TangoEventHandler::setSubscriptionInfo(Device *d,int id) {
  this->dev = d;
  this->subscribeId = id;
}

void TangoEventHandler::sendSubscriptionError(std::string& evName,std::string &error) {

  // Send event to notify the subscription error
  GQLDeviceAttributeFrame *gqlda = new GQLDeviceAttributeFrame(root,dev,index, devName, evName,attName, error);
  sendFrame(gqlda);
  delete gqlda;

}

Connection *TangoEventHandler::getConn() {
  return parent->getFactory()->getConn();
}

void TangoEventHandler::unsubscribe() {

  if(subscribeId!=-1) {

    std::string name;
    name.append(devName);
    name.push_back('/');
    name.append(attName);

    try {
      // Unsubscribe from Tango event
      if(root->logEnabled())
        root->log("TangoEventHandler::unsubscribe","unsubscribe_event(" + name + ")");
      dev->ds()->unsubscribe_event(subscribeId);
      subscribeId = -1;
    } catch (Tango::DevFailed &e) {
      if(root->logEnabled())
        root->log("TangoEventHandler::unsubscribe","unsubscribe_event(" + name + ") failed: " + std::string(e.errors[0].desc));
    }

  }

}

void TangoEventHandler::push_event(Tango::AttrConfEventData *event) {

  if( getConn()->isAborted() )
    return;
  if(event->err)
    return;

  getConn()->wsProcessingMutex.lock();

  GQLDeviceAttributeFrame *gqlda;

  gqlda = new GQLDeviceAttributeFrame(root,dev,index, devName, event->event,attName, *(event->attr_conf));
  sendFrame(gqlda);
  delete gqlda;

  getConn()->wsProcessingMutex.unlock();

}

void TangoEventHandler::push_event(Tango::EventData *event) {

  if( getConn()->isAborted() )
    return;

  getConn()->wsProcessingMutex.lock();

  GQLDeviceAttributeFrame *gqlda;

  if (event->err)
    gqlda = new GQLDeviceAttributeFrame(root,dev,index, devName, event->event,attName, event->errors);
  else
    gqlda = new GQLDeviceAttributeFrame(root,dev,index, devName, event->event,attName, *(event->attr_value));

  sendFrame(gqlda);
  delete gqlda;

  getConn()->wsProcessingMutex.unlock();

}

void TangoEventHandler::sendFrame(GQLDeviceAttributeFrame *node) {

  std::string result;
  result.append(R"({"id":")");
  result.append(getConn()->getUUID());
  result.append(R"(","type":"next","payload":)");
  result.append(R"({"data":{)");
  GQLRequestFields *req = parent->getRequest();
  std::string sName = req->alias.empty()?req->function:req->alias;
  result.append("\"");
  result.append(sName);
  result.append("\":");
  req->execute(node,result);
  result.append("}}}");

  WSFrame frame;
  frame.TextFrame(result);
  if (getConn()->Write(frame, WRITE_TIMEOUT) <= 0)
    getConn()->Abort(true,false);

}



void GQLEvalSubscription::subscribe(GQLEngine *root,Device *dev,std::string& attShortName,Tango::EventType eventType,TangoEventHandler *handler) {
  if (root->logEnabled())
    root->log("GQLEvalSubscription", "Subscribe to " + std::string(Tango::EventName[eventType]) +
                       "_event " + dev->ds()->dev_name() + "/" + attShortName);
  int subscriber_id = dev->ds()->subscribe_event(attShortName, eventType, handler);
  handler->setSubscriptionInfo(dev, subscriber_id);
}

GQLSubscription *GQLEvalSubscription::subscription(void *node,GQLRequestFields *children,GQLRequestArgs *args) {

  GQLEngine *root = (GQLEngine *)node;
  DeviceFactory *factory = root->getFactory();

  JSONITEM attNames = args->get("attNames","subscribe()");
  JSONITEM modes = args->get("modes","subscribe()");
  if(attNames.items.size() != modes.items.size()) {
    if( modes.items.size()!=1 )
      throw std::string("modes subscribe input parameters must have the same length as attNames or one item.");
    auto m0 = modes.items[0];
    modes.items.resize(attNames.items.size(),m0);
  }

  GQLSubscription *subscription = new GQLSubscription(factory, children);

  for (size_t attIdx = 0; attIdx < attNames.items.size(); attIdx++) {

    std::string& attName = attNames.items[attIdx];

    size_t pos = attName.find_last_of('/');
    if (pos == std::string::npos)
      throw std::string("subscribe() Invalid attribute name");
    std::string devName = attName.substr(0, pos);
    std::string attShortName = attName.substr(pos + 1);

    TangoEventHandler *handler = new TangoEventHandler(root,subscription,attIdx,devName,attShortName);

    try {

      Device *dev = factory->get(devName,root->isMultiTangoHostAllowed());

      if( modes.items[attIdx]=="CHANGE" ) {

        subscribe(root,dev,attShortName,Tango::CHANGE_EVENT,handler);

      } else if ( modes.items[attIdx]=="PERIODIC" ) {

        subscribe(root,dev,attShortName,Tango::PERIODIC_EVENT,handler);

      } else if ( modes.items[attIdx]=="CHANGE_PERIODIC" ) {

        try {

          subscribe(root,dev,attShortName,Tango::CHANGE_EVENT,handler);

        } catch (Tango::DevFailed &e) {

          if(root->logEnabled())
            root->log("GQLEvalSubscription", "Subscribe to CHANGE_EVENT " + attName + " failed");
          subscribe(root,dev,attShortName,Tango::PERIODIC_EVENT,handler);

        }

      } else if ( modes.items[attIdx]=="ATTR_CONF" ) {

        subscribe(root,dev,attShortName,Tango::ATTR_CONF_EVENT,handler);

      } else
        Tango::Except::throw_exception(
                "AttrSubscriptionFailed",
                modes.items[0] + " unknown subscription mode",
                "GQLEvalSubscription");


    } catch (Tango::DevFailed &e) {

      std::string err = std::string(e.errors[0].desc.in());
      handler->sendSubscriptionError(modes.items[0],err);

    }

    subscription->addCallback(handler);

  }

  return subscription;

}

void GQLEvalSubscription::to_string(void *node,std::string &evalStr,GQLRequestFields *children,GQLRequestArgs *args) {
  evalStr.append("null");
}

} // namespace TangoGQL_ns
