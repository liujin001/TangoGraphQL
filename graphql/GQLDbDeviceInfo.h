//
// GQLDbDeviceInfo object
//

#ifndef TANGOGQL_GQLDBDEVICEINFO_H
#define TANGOGQL_GQLDBDEVICEINFO_H

namespace TangoGQL_ns {

class GQLEngine;

class GQLDbDeviceInfo : public GQLBase {

public:

  GQLDbDeviceInfo(GQLEngine *root, const std::string& devName, const Tango::DbDevFullInfo& info): GQLBase(root), info(info), devName(devName) {
  }

  const std::string& devName;
  const Tango::DbDevFullInfo& info;

  std::string __typename() {
    return "DbDeviceInfo";
  }

};

BEGIN_EVAL(GQLEvalDbDeviceInfoDevName)
    GQLDbDeviceInfo *d = (GQLDbDeviceInfo *) node;
    GQLUtils::quote(d->devName,evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalDbDeviceInfoClassName)
    GQLDbDeviceInfo *d = (GQLDbDeviceInfo *) node;
    GQLUtils::quote(d->info.class_name,evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalDbDeviceInfoDSFullNameName)
    GQLDbDeviceInfo *d = (GQLDbDeviceInfo *) node;
    GQLUtils::quote(d->info.ds_full_name,evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalDbDeviceInfoHost)
    GQLDbDeviceInfo *d = (GQLDbDeviceInfo *) node;
    GQLUtils::quote(d->info.host,evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalDbDeviceInfoStartedDate)
    GQLDbDeviceInfo *d = (GQLDbDeviceInfo *) node;
    GQLUtils::quote(d->info.started_date,evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalDbDeviceInfoStoppedDate)
    GQLDbDeviceInfo *d = (GQLDbDeviceInfo *) node;
    GQLUtils::quote(d->info.stopped_date,evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalDbDeviceInfoPID)
    GQLDbDeviceInfo *d = (GQLDbDeviceInfo *) node;
    GQLUtils::Int64(d->info.pid,evalStr);
END_EVAL

} // namespace TangoGQL_ns

#endif //TANGOGQL_GQLDBDEVICEINFO_H
