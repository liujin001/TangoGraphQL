//
// GQLDeviceAttributeFrame object
//

#ifndef TANGOGQL_GQLDEVICEATTRIBUTEFRAME_H
#define TANGOGQL_GQLDEVICEATTRIBUTEFRAME_H

#include "GQLDeviceAttribute.h"
#include "GQLAttributeInfo.h"

namespace TangoGQL_ns {

class GQLEngine;

class GQLDeviceAttributeFrame : public GQLBase {

public:

  GQLDeviceAttributeFrame(GQLEngine *root,Device *dev,int index,const std::string &devName,const std::string &evName, const std::string &attName, std::string& subscriptionError):
  GQLBase(root),devName(devName),eventName(evName),attName(attName) {

    this->dev = dev;
    this->index = index;
    this->subscriptionError = subscriptionError;
    gqlda = nullptr;
    gqlai = nullptr;

  }

  GQLDeviceAttributeFrame(GQLEngine *root,Device *dev,int index,const std::string &devName,const std::string &evName, const std::string &attName, Tango::DevErrorList& errors):
  GQLBase(root),devName(devName),eventName(evName),attName(attName) {

    this->dev = dev;
    this->index = index;
    gqlai = nullptr;

    // Create an error device attribute
    timeval tv{};
    gettimeofday(&tv,nullptr);
    Tango::DevErrorList *errCopy = new Tango::DevErrorList(errors);
    da.set_error_list(errCopy);
    da.name = attName;
    da.time.tv_sec = tv.tv_sec;
    da.time.tv_usec = tv.tv_usec;
    gqlda = new GQLDeviceAttribute(root,dev,da,false);
  }

  GQLDeviceAttributeFrame(GQLEngine *root,Device *dev,int index,const std::string &devName, const std::string &evName,const std::string &attName, Tango::DeviceAttribute &da):
  GQLBase(root),devName(devName),eventName(evName),attName(attName) {
    this->dev = dev;
    this->index = index;
    gqlai = nullptr;
    gqlda = new GQLDeviceAttribute(root,dev,da,false);
  }

  GQLDeviceAttributeFrame(GQLEngine *root,Device *dev,int index,const std::string &devName, const std::string &evName,const std::string &attName, Tango::AttributeInfoEx& ai):
  GQLBase(root),devName(devName),eventName(evName),attName(attName) {
    static const std::string _AttributeInfo = "AttributeInfo";
    this->dev = dev;
    this->index = index;
    gqlda = nullptr;
    gqlai = new GQLAttributeInfo(root,dev,ai,_AttributeInfo);
  }

  ~GQLDeviceAttributeFrame() {
    delete gqlda;
    delete gqlai;
  }

  std::string __typename() {
    return "AttributeFrame";
  }

  Device *dev;
  Tango::DeviceAttribute da;
  GQLDeviceAttribute *gqlda;
  GQLAttributeInfo *gqlai;
  int index;
  std::string subscriptionError;
  const std::string& devName;
  const std::string& attName;
  const std::string& eventName;

};

BEGIN_EVAL(GQLEvalDeviceAttributeFrameValue)
    GQLDeviceAttributeFrame *g = (GQLDeviceAttributeFrame *) node;
    if (g->gqlda == nullptr) {
      evalStr.append("null");
    } else {
      children->execute((void *) g->gqlda, evalStr);
    }
END_EVAL

BEGIN_EVAL(GQLEvalDeviceAttributeFrameAttributeInfo)
    GQLDeviceAttributeFrame *g = (GQLDeviceAttributeFrame *) node;
    if (g->gqlai == nullptr) {
      evalStr.append("null");
    } else {
      children->execute((void *) g->gqlai, evalStr);
    }
END_EVAL

BEGIN_EVAL(GQLEvalDeviceAttributeFrameSubscriptionError)
      GQLDeviceAttributeFrame *g = (GQLDeviceAttributeFrame *)node;
      GQLUtils::quoteOrNull(g->subscriptionError,evalStr, true);
END_EVAL

BEGIN_EVAL(GQLEvalDeviceAttributeFrameEvent)
    GQLDeviceAttributeFrame *g = (GQLDeviceAttributeFrame *)node;
    GQLUtils::quote(g->eventName,evalStr, true);
END_EVAL

BEGIN_EVAL(GQLEvalDeviceAttributeFrameIndex)
      GQLDeviceAttributeFrame *g = (GQLDeviceAttributeFrame *)node;
      GQLUtils::UInt64((uint64_t)g->index,evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalDeviceAttributeFrameFullName)
    GQLDeviceAttributeFrame *g = (GQLDeviceAttributeFrame *)node;
    std::string fullName;
    fullName.append(g->devName);
    fullName.push_back('/');
    fullName.append(g->attName);
    transform(fullName.begin(), fullName.end(), fullName.begin(),
              [](unsigned char c){ return std::tolower(c); });
    GQLUtils::quote(fullName,evalStr);
END_EVAL

} // namespace TangoGQL_ns

#endif //TANGOGQL_GQLDEVICEATTRIBUTEFRAME_H
