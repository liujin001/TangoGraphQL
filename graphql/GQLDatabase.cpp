//
// GQLDatabase object
//

#include "TangoGQL.h"
#include "GQLDatabase.h"
#include "GQLMutationStatus.h"


namespace TangoGQL_ns {

void GQLEvalDatabase::to_string(void *node,std::string &evalStr,GQLRequestFields *children,GQLRequestArgs *args) {

  GQLEngine *root = (GQLEngine *)node;
  std::string& tangoHost = args->getString("tangoHost","database()");

  GQLDatabase d(root,type);

  try {

    if (tangoHost == "default") {

      d.db = Tango::Util::instance()->get_database();
      d.host = d.db->get_db_host();
      d.port = d.db->get_db_port_num();

    } else {

      if(!root->isMultiTangoHostAllowed())
        Tango::Except::throw_exception(
                "MultiTangoHostNotAllowed",
                tangoHost + " not allowed, default only",
                "GQLEvalDatabase");

      size_t idx = tangoHost.find(':');
      if(idx==std::string::npos)
        Tango::Except::throw_exception(
                "AttrWrongTangoHost",
                tangoHost + " invalid database name, use host:port",
                "GQLEvalDatabase");
      std::string host = tangoHost.substr(0,idx);
      std::string port = tangoHost.substr(idx+1);
      d.db = new Tango::Database(host,std::stoi(port));
      d.needToDeleteDB = true;
      d.host = d.db->get_db_host();
      d.port = d.db->get_db_port_num();

    }

  } catch (Tango::DevFailed &e) {
    d.error = new Tango::DevFailed(e);
  }

  children->execute((void *) &d, evalStr);

}

void GQLEvalDatabaseItems::to_string(void *node,std::string &evalStr,GQLRequestFields *children,GQLRequestArgs *args) {

  GQLDbItem *parent = isRoot() ? nullptr : (GQLDbItem *)node;
  GQLDatabaseBase *d = (GQLDatabaseBase *) node;
  Tango::Database *db = d->db;
  GQLEngine *root = d->root;

  if(db == nullptr) {
    evalStr.append("null");
  } else {

    std::string& wildcard = args->getString("wildcard",getMethod());

    try {

      std::vector<GQLDbItem *> dbItems;
      getData(root,db,parent,wildcard,dbItems);
      PTRARRAY(dbItems);
      for(auto & dbItem : dbItems)
        delete dbItem;

    } catch (Tango::DevFailed &e) {

      evalStr.append("null");
      d->error = new Tango::DevFailed(e);

    }

  }

}

void GQLEvalDatabaseAddServer::to_string(void *node,std::string &evalStr,GQLRequestFields *children,GQLRequestArgs *args) {

  GQLDatabase *d = (GQLDatabase *) node;
  if (d->db == nullptr) {
    evalStr.append("null");
  } else {

    GQLMutationStatus *status = new GQLMutationStatus(d->root);
    try {

      std::string serverName = args->getString("serverName", "add_server()");
      JSONITEM devNames = args->get("devNames", "add_server()");
      JSONITEM classNames = args->get("classNames", "add_server()");
      if (devNames.items.size() != classNames.items.size()) {
        Tango::Except::throw_exception(
                "DBWrongParam",
                "devNames and classNames must have the same length in add_server",
                "GQLEvalDatabaseAddServer");
      }
      Tango::DbDevInfos dInfos;
      for (size_t i = 0; i < devNames.items.size(); i++) {
        Tango::DbDevInfo di;
        di.name = devNames.items[i];
        di._class = classNames.items[i];
        di.server = serverName;
        dInfos.push_back(di);
      }
      d->db->add_server(serverName, dInfos);

    } catch (Tango::DevFailed &e) {
      status->error = new Tango::DevFailed(e);
    }
    children->execute((void *) status, evalStr);

  }

}

void GQLEvalDatabaseDeleteServer::to_string(void *node,std::string &evalStr,GQLRequestFields *children,GQLRequestArgs *args) {

  GQLDatabase *d = (GQLDatabase *) node;
  if (d->db == nullptr) {
    evalStr.append("null");
  } else {

    GQLMutationStatus *status = new GQLMutationStatus(d->root);
    try {

      std::string serverName = args->getString("serverName", "delete_server()");
      d->db->delete_server(serverName);

    } catch (Tango::DevFailed &e) {
      status->error = new Tango::DevFailed(e);
    }
    children->execute((void *) status, evalStr);

  }

}

void GQLEvalDatabasePutProperty::to_string(void *node,std::string &evalStr,GQLRequestFields *children,GQLRequestArgs *args) {

  GQLDatabase *d = (GQLDatabase *) node;
  if (d->db == nullptr) {
    evalStr.append("null");
  } else {

    std::string &type = args->getString("type", "put_property()");
    std::string &name = args->getString("name", "put_property()");
    std::string &propName = args->getString("propName", "put_property()");
    JSONITEM propValue = args->get("value", "put_property()");
    bool deleteProp = !propValue.items.empty() &&
                      propValue.type == JSON_SCALAR &&
                      propValue.items[0] == "null";

    Tango::DbData dbData;
    Tango::DbDatum datum(propName);
    if (!deleteProp)
      datum << propValue.items;
    dbData.push_back(datum);
    GQLMutationStatus *status = new GQLMutationStatus(d->root);

    try {

      if (type == "DEVICE") {
        if (deleteProp)
          d->db->delete_device_property(name, dbData);
        else
          d->db->put_device_property(name, dbData);
      } else if (type == "CLASS") {
        if (deleteProp)
          d->db->delete_class_property(name, dbData);
        else
          d->db->put_class_property(name, dbData);
      } else if (type == "OBJECT") {
        if (deleteProp)
          d->db->delete_property(name, dbData);
        else
          d->db->put_property(name, dbData);
      } else {
        Tango::Except::throw_exception(
                "DBWrongType",
                "Only DEVICE,CLASS or OBJECT type are allowed for put_property",
                "GQLEvalDatabaseList");
      }

    } catch (Tango::DevFailed &e) {
      status->error = new Tango::DevFailed(e);
    }

    children->execute((void *) status, evalStr);

  }

}

void GQLEvalDatabasePutAttributeProperty::to_string(void *node,std::string &evalStr,GQLRequestFields *children,GQLRequestArgs *args) {

  GQLDatabase *d = (GQLDatabase *) node;
  if (d->db == nullptr) {
    evalStr.append("null");
  } else {

    std::string &type = args->getString("type", "put_attribute_property()");
    std::string &name = args->getString("name", "put_attribute_property()");
    std::string &attName = args->getString("attName", "put_attribute_property()");
    std::string &propName = args->getString("propName", "put_attribute_property()");
    JSONITEM propValue = args->get("value", "put_attribute_property()");
    bool deleteProp = !propValue.items.empty() &&
                      propValue.type == JSON_SCALAR &&
                      propValue.items[0] == "null";

    Tango::DbData dbData;
    Tango::DbDatum attDatum(attName);
    attDatum << 1; // One property
    dbData.push_back(attDatum);
    Tango::DbDatum datum(propName);
    if (!deleteProp)
      datum << propValue.items;
    dbData.push_back(datum);

    GQLMutationStatus *status = new GQLMutationStatus(d->root);

    try {

      if (type == "DEVICE") {
        if (deleteProp)
          d->db->delete_device_attribute_property(name, dbData);
        else
          d->db->put_device_attribute_property(name, dbData);
      } else if (type == "CLASS") {
        if (deleteProp)
          d->db->delete_class_attribute_property(name, dbData);
        else
          d->db->put_class_attribute_property(name, dbData);
      } else {
        Tango::Except::throw_exception(
                "DBWrongType",
                "Only DEVICE or CLASS type are allowed for put_attribute_property",
                "GQLEvalDatabaseList");
      }

    } catch (Tango::DevFailed &e) {
      status->error = new Tango::DevFailed(e);
    }

    children->execute((void *) status, evalStr);

  }

}

} // namespace TangoGQL_ns

