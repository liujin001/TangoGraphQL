//
// GQLAttributeInfo eval functions
//

#include "TangoGQL.h"
#include "GQLAttributeInfo.h"
#include "GQLMutationStatus.h"


// Mute GQLAttributeInfo::props, actual set_attribute_config is done
// in Device.cpp
#define WRITE_ATT_CONF(_field,_method)  {                               \
  std::string& value = args->getString("value", _method);                    \
  GQLAttributeInfo *p = (GQLAttributeInfo *)node;                       \
  GQLMutationStatus *status = new GQLMutationStatus(p->root);           \
  Tango::AttributeInfoEx *ptr = (Tango::AttributeInfoEx *)&(p->props);  \
  ptr->_field = value;                                                  \
  children->execute((void *) status, evalStr);                          \
  delete status;}

namespace TangoGQL_ns {


void GQLEvalAttributeInfoWriteDescription::to_string(void *node, std::string &evalStr, GQLRequestFields *children, GQLRequestArgs *args) {
  WRITE_ATT_CONF(description,"Description");
}

void GQLEvalAttributeInfoWriteLabel::to_string(void *node, std::string &evalStr, GQLRequestFields *children, GQLRequestArgs *args) {
  WRITE_ATT_CONF(label,"Label");
}

void GQLEvalAttributeInfoWriteUnit::to_string(void *node, std::string &evalStr, GQLRequestFields *children, GQLRequestArgs *args) {
  WRITE_ATT_CONF(unit,"Unit");
}

void GQLEvalAttributeInfoWriteStandardUnit::to_string(void *node, std::string &evalStr, GQLRequestFields *children, GQLRequestArgs *args) {
  WRITE_ATT_CONF(standard_unit,"StandardUnit");
}

void GQLEvalAttributeInfoWriteDisplayUnit::to_string(void *node, std::string &evalStr, GQLRequestFields *children, GQLRequestArgs *args) {
  WRITE_ATT_CONF(display_unit,"DisplayUnit");
}

void GQLEvalAttributeInfoWriteFormat::to_string(void *node, std::string &evalStr, GQLRequestFields *children, GQLRequestArgs *args) {
  WRITE_ATT_CONF(format,"Format");
}

void GQLEvalAttributeInfoWriteMinValue::to_string(void *node, std::string &evalStr, GQLRequestFields *children, GQLRequestArgs *args) {
  WRITE_ATT_CONF(min_value,"MinValue");
}

void GQLEvalAttributeInfoWriteMaxValue::to_string(void *node, std::string &evalStr, GQLRequestFields *children, GQLRequestArgs *args) {
  WRITE_ATT_CONF(max_value,"MaxValue");
}

void GQLEvalAttributeInfoWriteMinAlarm::to_string(void *node, std::string &evalStr, GQLRequestFields *children, GQLRequestArgs *args) {
  WRITE_ATT_CONF(alarms.min_alarm,"MinAlarm");
}

void GQLEvalAttributeInfoWriteMaxAlarm::to_string(void *node, std::string &evalStr, GQLRequestFields *children, GQLRequestArgs *args) {
  WRITE_ATT_CONF(alarms.max_alarm,"MaxAlarm");
}

void GQLEvalAttributeInfoWriteDeltaT::to_string(void *node, std::string &evalStr, GQLRequestFields *children, GQLRequestArgs *args) {
  WRITE_ATT_CONF(alarms.delta_t,"DeltaT");
}

void GQLEvalAttributeInfoWriteDeltaVal::to_string(void *node, std::string &evalStr, GQLRequestFields *children, GQLRequestArgs *args) {
  WRITE_ATT_CONF(alarms.delta_val,"DelaVal");
}

void GQLEvalAttributeInfoWriteChangeRelChange::to_string(void *node, std::string &evalStr, GQLRequestFields *children, GQLRequestArgs *args) {
  WRITE_ATT_CONF(events.ch_event.rel_change,"ChangeRelChange");
}

void GQLEvalAttributeInfoWriteChangeAbsChange::to_string(void *node, std::string &evalStr, GQLRequestFields *children, GQLRequestArgs *args) {
  WRITE_ATT_CONF(events.ch_event.abs_change,"ChangeAbsChange");
}

void GQLEvalAttributeInfoWritePeriodicPeriod::to_string(void *node, std::string &evalStr, GQLRequestFields *children, GQLRequestArgs *args) {
  WRITE_ATT_CONF(events.per_event.period,"PeriodicPeriod");
}

void GQLEvalAttributeInfoWriteArchiveRelChange::to_string(void *node, std::string &evalStr, GQLRequestFields *children, GQLRequestArgs *args) {
  WRITE_ATT_CONF(events.arch_event.archive_period,"ArchiveRelChange");
}

void GQLEvalAttributeInfoWriteArchiveAbsChange::to_string(void *node, std::string &evalStr, GQLRequestFields *children, GQLRequestArgs *args) {
  WRITE_ATT_CONF(events.arch_event.archive_abs_change,"ArchiveAbsChange");
}

void GQLEvalAttributeInfoWriteArchivePeriod::to_string(void *node, std::string &evalStr, GQLRequestFields *children, GQLRequestArgs *args) {
  WRITE_ATT_CONF(events.arch_event.archive_period,"ArchivePeriod");
}

} // namespace TangoGQL_ns