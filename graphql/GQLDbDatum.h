//
// GQLDbDatum object
//

#ifndef TANGOGQL_GQLDBDATUM_H
#define TANGOGQL_GQLDBDATUM_H

namespace TangoGQL_ns {

class GQLEngine;

class GQLDbDatum : public GQLBase {

public:

  GQLDbDatum(GQLEngine *root, const std::string& attName, const Tango::DbDatum& datum): GQLBase(root), dd(datum), attName(attName) {
  }

  const std::string& attName;
  const Tango::DbDatum& dd;

  std::string __typename() {
    return "DbDatum";
  }

};

BEGIN_EVAL(GQLEvalDbPropertyName)
    GQLDbDatum *d = (GQLDbDatum *) node;
    GQLUtils::quote(d->dd.name,evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalDbPropertyAttName)
    GQLDbDatum *d = (GQLDbDatum *) node;
    GQLUtils::quoteOrNull(d->attName,evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalDbPropertyValue)
    GQLDbDatum *d = (GQLDbDatum *) node;
    GQLUtils::stringArr(d->dd.value_string,evalStr,true);
END_EVAL

} // namespace TangoGQL_ns

#endif //TANGOGQL_GQLDBDATUM_H
