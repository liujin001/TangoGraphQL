//
// GQL Engine
//
#include "GQLEngine.h"
#include <algorithm>
#include "schema/GQLEnumEval.h"
#include "schema/GQLTypeEval.h"
#include "schema/GQLFieldEval.h"
#include "schema/GQLInputValueEval.h"
#include "schema/GQLSchemaEval.h"
#include "GQLDeviceAttribute.h"
#include "GQLDeviceAttributeFrame.h"
#include "GQLAttributeInfo.h"
#include "GQLCommandInfo.h"
#include "GQLSubscription.h"
#include "GQLMutationStatus.h"
#include "GQLDatabase.h"


namespace TangoGQL_ns {


// Include at the end of the file to avoid clion lint failure
extern const char *gqlBase;

// Init static vars
GQLSchema * GQLEngine::mainSchema = nullptr;
bool GQLEngine::forceLog = false;

GQLEngine::GQLEngine(TangoGQL *tangoGql,DeviceFactory *factory):Tango::LogAdapter(tangoGql),factory(factory) {
}
void GQLEngine::enableLog() {
  forceLog = true;
}

std::string GQLEngine::getSchemaAsString() {
  std::string s(gqlBase);
  // Remove the first line
  std::string::size_type idx = s.find("# GraphQL");
  if(idx!=std::string::npos)
    return s.substr(idx);
  else
    return s;
}

void GQLEngine::init() {

  if(mainSchema != nullptr)
    return;

  mainSchema = new GQLSchema();

  // Parse and build the GraphQL schema introspection system
  std::string s(gqlBase);

  try {
    Parser p(s,"schema");
    mainSchema->parse(&p);
  } catch (std::string& err) {
    std::cerr << "Fatal error: Syntax error in GQLSchema.schema: " << err << std::endl;
    exit(-1);
  }

  // Evaluation object
  GQLType *t = mainSchema->getType("__Type");
  t->getField("name")->evalFunction = new GQLEvalTypeName();
  t->getField("kind")->evalFunction = new GQLEvalTypeKind();
  t->getField("description")->evalFunction = new GQLEvalTypeDescription();
  t->getField("interfaces")->evalFunction = new GQLEvalTypeInterfaces();
  t->getField("fields")->evalFunction = new GQLEvalTypeFields();
  t->getField("enumValues")->evalFunction = new GQLEvalTypeEnumValues();
  t->getField("inputFields")->evalFunction = new GQLEvalTypeInputFields();
  t->getField("ofType")->evalFunction = new GQLEvalTypeOfType();

  t = mainSchema->getType("__EnumValue");
  t->getField("name")->evalFunction = new GQLEvalEnumName();
  t->getField("description")->evalFunction = new GQLEvalEnumDescription();
  t->getField("isDeprecated")->evalFunction = new GQLEvalEnumIsDeprecated();
  t->getField("deprecationReason")->evalFunction = new GQLEvalEnumDepreciationReason();

  t = mainSchema->getType("__Field");
  t->getField("name")->evalFunction = new GQLEvalFieldName();
  t->getField("description")->evalFunction = new GQLEvalFieldDescription();
  t->getField("args")->evalFunction = new GQLEvalFieldArgs();
  t->getField("type")->evalFunction = new GQLEvalFieldType();
  t->getField("isDeprecated")->evalFunction = new GQLEvalFieldIsDeprecated();
  t->getField("deprecationReason")->evalFunction = new GQLEvalFieldDepreciationReason();

  t = mainSchema->getType("__InputValue");
  t->getField("name")->evalFunction = new GQLEvalInputValueName();
  t->getField("description")->evalFunction = new GQLEvalInputValueDescription();
  t->getField("type")->evalFunction = new GQLEvalInputValueType();
  t->getField("defaultValue")->evalFunction = new GQLEvalInputValueDefaultValue();

  t = mainSchema->getType("__Schema");
  t->getField("description")->evalFunction = new GQLEvalSchemaDescription();
  t->getField("types")->evalFunction = new GQLEvalSchemaTypes();
  t->getField("queryType")->evalFunction = new GQLEvalSchemaQueryType();
  t->getField("mutationType")->evalFunction = new GQLEvalSchemaMutationType();
  t->getField("subscriptionType")->evalFunction = new GQLEvalSchemaSubscriptionType();
  t->getField("directives")->evalFunction = new GQLEvalSchemaDirectives();

  t = mainSchema->getType("DeviceRead");
  t->getField("name")->evalFunction = new GQLEvalDeviceName();
  t->getField("read_attributes")->evalFunction = new GQLEvalDeviceReadAttributes();
  t->getField("read_attribute_hist")->evalFunction = new GQLEvalDeviceReadAttributeHist();
  t->getField("attribute_list")->evalFunction = new GQLEvalDeviceAttributeList();
  t->getField("attribute_info")->evalFunction = new GQLEvalDeviceAttributeInfo();
  t->getField("command_list")->evalFunction = new GQLEvalDeviceCommandList();
  t->getField("command_info")->evalFunction = new GQLEvalDeviceCommandInfo();
  t->getField("error")->evalFunction = new GQLEvalDeviceError();

  t = mainSchema->getType("DbDatum");
  t->getField("name")->evalFunction = new GQLEvalDbPropertyName();
  t->getField("att_name")->evalFunction = new GQLEvalDbPropertyAttName();
  t->getField("value")->evalFunction = new GQLEvalDbPropertyValue();

  t = mainSchema->getType("DatabaseRead");
  t->getField("host_name")->evalFunction = new GQLEvalDatabaseHostName();
  t->getField("port")->evalFunction = new GQLEvalDatabasePort();
  t->getField("info")->evalFunction = new GQLEvalDatabaseInfo();
  t->getField("list")->evalFunction = new GQLEvalDatabaseList();
  t->getField("property_list")->evalFunction = new GQLEvalDatabasePropertyList();
  t->getField("properties")->evalFunction = new GQLEvalDatabaseProperties();
  t->getField("attribute_properties")->evalFunction = new GQLEvalDatabaseAttributeProperties();
  t->getField("domains")->evalFunction = new GQLEvalDatabaseDomains();
  t->getField("servers")->evalFunction = new GQLEvalDatabaseServers();
  t->getField("device_info")->evalFunction = new GQLEvalDatabaseDeviceInfo();
  t->getField("error")->evalFunction = new GQLEvalDatabaseError();

  t = mainSchema->getType("DbDomain");
  t->getField("name")->evalFunction = new GQLEvalDatabaseItemsName();
  t->getField("error")->evalFunction = new GQLEvalDatabaseError();
  t->getField("families")->evalFunction = new GQLEvalDatabaseFamilies();

  t = mainSchema->getType("DbFamily");
  t->getField("name")->evalFunction = new GQLEvalDatabaseItemsName();
  t->getField("error")->evalFunction = new GQLEvalDatabaseError();
  t->getField("members")->evalFunction = new GQLEvalDatabaseMembers();

  t = mainSchema->getType("DbMember");
  t->getField("name")->evalFunction = new GQLEvalDatabaseItemsName();

  t = mainSchema->getType("DbServer");
  t->getField("name")->evalFunction = new GQLEvalDatabaseItemsName();
  t->getField("error")->evalFunction = new GQLEvalDatabaseError();
  t->getField("instances")->evalFunction = new GQLEvalDatabaseInstances();

  t = mainSchema->getType("DbInstance");
  t->getField("name")->evalFunction = new GQLEvalDatabaseItemsName();
  t->getField("error")->evalFunction = new GQLEvalDatabaseError();
  t->getField("classes")->evalFunction = new GQLEvalDatabaseClasses();

  t = mainSchema->getType("DbClass");
  t->getField("name")->evalFunction = new GQLEvalDatabaseItemsName();
  t->getField("error")->evalFunction = new GQLEvalDatabaseError();
  t->getField("devices")->evalFunction = new GQLEvalDatabaseDevices();

  t = mainSchema->getType("DbDevice");
  t->getField("name")->evalFunction = new GQLEvalDatabaseItemsName();

  t = mainSchema->getType("DbDeviceInfo");
  t->getField("dev_name")->evalFunction = new GQLEvalDbDeviceInfoDevName();
  t->getField("class_name")->evalFunction = new GQLEvalDbDeviceInfoClassName();
  t->getField("ds_full_name")->evalFunction = new GQLEvalDbDeviceInfoDSFullNameName();
  t->getField("host")->evalFunction = new GQLEvalDbDeviceInfoHost();
  t->getField("started_date")->evalFunction = new GQLEvalDbDeviceInfoStartedDate();
  t->getField("stopped_date")->evalFunction = new GQLEvalDbDeviceInfoStoppedDate();
  t->getField("dev_name")->evalFunction = new GQLEvalDbDeviceInfoDevName();
  t->getField("dev_name")->evalFunction = new GQLEvalDbDeviceInfoDevName();
  t->getField("pid")->evalFunction = new GQLEvalDbDeviceInfoPID();

  t = mainSchema->getType("DeviceWrite");
  t->getField("write_attribute")->evalFunction = new GQLEvalDeviceWriteAttribute();
  t->getField("command_inout")->evalFunction = new GQLEvalDeviceCommandInOut();
  t->getField("write_attribute_info")->evalFunction = new GQLEvalDeviceWriteAttributeInfo();
  t->getField("error")->evalFunction = new GQLEvalDeviceError();

  t = mainSchema->getType("DatabaseWrite");
  t->getField("put_property")->evalFunction = new GQLEvalDatabasePutProperty();
  t->getField("put_attribute_property")->evalFunction = new GQLEvalDatabasePutAttributeProperty();
  t->getField("add_server")->evalFunction = new GQLEvalDatabaseAddServer();
  t->getField("delete_server")->evalFunction = new GQLEvalDatabaseDeleteServer();
  t->getField("error")->evalFunction = new GQLEvalDatabaseError();

  t = mainSchema->getType("MutationStatus");
  t->getField("command_output")->evalFunction = new GQLEvalMutationStatusCommandOutput();
  t->getField("error")->evalFunction = new GQLEvalMutationStatusError();

  t = mainSchema->getType("DeviceAttribute");
  t->getField("name")->evalFunction = new GQLEvalDeviceAttributeName();
  t->getField("quality")->evalFunction = new GQLEvalDeviceAttributeQuality();
  t->getField("value")->evalFunction = new GQLEvalDeviceAttributeValue();
  t->getField("write_value")->evalFunction = new GQLEvalDeviceAttributeWriteValue();
  t->getField("timestamp")->evalFunction = new GQLEvalDeviceAttributeTimeStamp();
  t->getField("error")->evalFunction = new GQLEvalDeviceAttributeError();
  t->getField("dim_x")->evalFunction = new GQLEvalDeviceAttributeDimX();
  t->getField("dim_y")->evalFunction = new GQLEvalDeviceAttributeDimY();
  t->getField("w_dim_x")->evalFunction = new GQLEvalDeviceAttributeWDimX();
  t->getField("w_dim_y")->evalFunction = new GQLEvalDeviceAttributeWDimY();
  t->getField("data_type")->evalFunction = new GQLEvalDeviceAttributeDataType();
  t->getField("data_format")->evalFunction = new GQLEvalDeviceAttributeDataFormat();
  t->getField("encoded_format")->evalFunction = new GQLEvalDeviceAttributeEncodedFormat();
  t->getField("error")->evalFunction = new GQLEvalDeviceAttributeError();

  t = mainSchema->getType("AttributeFrame");
  t->getField("full_name")->evalFunction = new GQLEvalDeviceAttributeFrameFullName();
  t->getField("event")->evalFunction = new GQLEvalDeviceAttributeFrameEvent();
  t->getField("index")->evalFunction = new GQLEvalDeviceAttributeFrameIndex();
  t->getField("value")->evalFunction = new GQLEvalDeviceAttributeFrameValue();
  t->getField("attribute_info")->evalFunction = new GQLEvalDeviceAttributeFrameAttributeInfo();
  t->getField("subscription_error")->evalFunction = new GQLEvalDeviceAttributeFrameSubscriptionError();

  t = mainSchema->getType("AttributeInfo");
  t->getField("name")->evalFunction = new GQLEvalAttributeInfoName();
  t->getField("writable")->evalFunction = new GQLEvalAttributeInfoWritable();
  t->getField("data_format")->evalFunction = new GQLEvalAttributeInfoDataFormat();
  t->getField("data_type")->evalFunction = new GQLEvalAttributeInfoDataType();
  t->getField("max_dim_x")->evalFunction = new GQLEvalAttributeInfoMaxDimX();
  t->getField("max_dim_y")->evalFunction = new GQLEvalAttributeInfoMaxDimY();
  t->getField("description")->evalFunction = new GQLEvalAttributeInfoDescription();
  t->getField("label")->evalFunction = new GQLEvalAttributeInfoLabel();
  t->getField("unit")->evalFunction = new GQLEvalAttributeInfoUnit();
  t->getField("standard_unit")->evalFunction = new GQLEvalAttributeInfoStandardUnit();
  t->getField("display_unit")->evalFunction = new GQLEvalAttributeInfoDisplayUnit();
  t->getField("format")->evalFunction = new GQLEvalAttributeInfoFormat();
  t->getField("min_value")->evalFunction = new GQLEvalAttributeInfoMinValue();
  t->getField("max_value")->evalFunction = new GQLEvalAttributeInfoMaxValue();
  t->getField("min_alarm")->evalFunction = new GQLEvalAttributeInfoMinAlarm();
  t->getField("max_alarm")->evalFunction = new GQLEvalAttributeInfoMaxAlarm();
  t->getField("delta_t")->evalFunction = new GQLEvalAttributeInfoDeltaT();
  t->getField("delta_val")->evalFunction = new GQLEvalAttributeInfoDeltaVal();
  t->getField("enum_labels")->evalFunction = new GQLEvalAttributeInfoEnumLabels();
  t->getField("disp_level")->evalFunction = new GQLEvalAttributeInfoDispLevel();
  t->getField("change_rel_change")->evalFunction = new GQLEvalAttributeInfoChangeRelChange();
  t->getField("change_abs_change")->evalFunction = new GQLEvalAttributeInfoChangeAbsChange();
  t->getField("periodic_period")->evalFunction = new GQLEvalAttributeInfoPeriodicPeriod();
  t->getField("archive_rel_change")->evalFunction = new GQLEvalAttributeInfoArchiveRelChange();
  t->getField("archive_abs_change")->evalFunction = new GQLEvalAttributeInfoArchiveAbsChange();
  t->getField("archive_period")->evalFunction = new GQLEvalAttributeInfoArchivePeriod();

  t = mainSchema->getType("SetAttributeInfo");
  t->getField("description")->evalFunction = new GQLEvalAttributeInfoWriteDescription();
  t->getField("label")->evalFunction = new GQLEvalAttributeInfoWriteLabel();
  t->getField("unit")->evalFunction = new GQLEvalAttributeInfoWriteUnit();
  t->getField("standard_unit")->evalFunction = new GQLEvalAttributeInfoWriteStandardUnit();
  t->getField("display_unit")->evalFunction = new GQLEvalAttributeInfoWriteDisplayUnit();
  t->getField("format")->evalFunction = new GQLEvalAttributeInfoWriteFormat();
  t->getField("min_value")->evalFunction = new GQLEvalAttributeInfoWriteMinValue();
  t->getField("max_value")->evalFunction = new GQLEvalAttributeInfoWriteMaxValue();
  t->getField("min_alarm")->evalFunction = new GQLEvalAttributeInfoWriteMinAlarm();
  t->getField("max_alarm")->evalFunction = new GQLEvalAttributeInfoWriteMaxAlarm();
  t->getField("delta_t")->evalFunction = new GQLEvalAttributeInfoWriteDeltaT();
  t->getField("delta_val")->evalFunction = new GQLEvalAttributeInfoWriteDeltaVal();
  t->getField("change_rel_change")->evalFunction = new GQLEvalAttributeInfoWriteChangeRelChange();
  t->getField("change_abs_change")->evalFunction = new GQLEvalAttributeInfoWriteChangeAbsChange();
  t->getField("periodic_period")->evalFunction = new GQLEvalAttributeInfoWritePeriodicPeriod();
  t->getField("archive_rel_change")->evalFunction = new GQLEvalAttributeInfoWriteArchiveRelChange();
  t->getField("archive_abs_change")->evalFunction = new GQLEvalAttributeInfoWriteArchiveAbsChange();
  t->getField("archive_period")->evalFunction = new GQLEvalAttributeInfoWriteArchivePeriod();

  t = mainSchema->getType("CommandInfo");
  t->getField("name")->evalFunction = new GQLEvalCommandInfoName();
  t->getField("tag")->evalFunction = new GQLEvalCommandInfoTag();
  t->getField("in_type")->evalFunction = new GQLEvalCommandInfoInType();
  t->getField("out_type")->evalFunction = new GQLEvalCommandInfoOutType();
  t->getField("in_type_desc")->evalFunction = new GQLEvalCommandInfoInTypeDesc();
  t->getField("out_type_desc")->evalFunction = new GQLEvalCommandInfoOutTypeDesc();

  // Queries and meta queries (query stating with __ are hidden)
  t = mainSchema->getType("Query");
  t->getField("__type")->evalFunction = new GQLEvalQuery__type();
  t->getField("__schema")->evalFunction = new GQLEvalQuery__schema();
  t->getField("__devnames")->evalFunction = new GQLEvalQueryDumpFactory();
  t->getField("device")->evalFunction = new GQLEvalDevice("DeviceRead");
  t->getField("database")->evalFunction = new GQLEvalDatabase("DataBaseRead");

  // Mutations
  t = mainSchema->getType("Mutation");
  t->getField("device")->evalFunction = new GQLEvalDevice("DeviceWrite");
  t->getField("database")->evalFunction = new GQLEvalDatabase("DataBaseWrite");

  // Subscriptions
  t = mainSchema->getType("Subscription");
  t->getField("subscribe")->evalFunction = new GQLEvalSubscription();

}

void GQLEngine::test() {

  std::string j0 = "{\"type\":\"connection_init\",\"payload\":{}}";

  std::string i1 = "{\"query\":\"{\n"
              "  __type(name:\\\"Query\\\") {\n"
              "    fields {\n"
              "      name\n"
              "      args {\n"
              "        name\n"
              "        type {\n"
              "          name\n"
              "          kind\n"
              "          ofType {\n"
              "            name\n"
              "            kind\n"
              "          }\n"
              "        }\n"
              "      }\n"
              "    }\n"
              "  }\n"
              "}\"}";

  std::string i2 = "{\"query\":\"{\n"
              "  __schema {\n"
              "    queryType {\n"
              "      name\n"
              "    }\n"
              "  }\n"
              "}\"}";

  std::string i3 = "{\"query\":\"{\n"
              "  __type(name:\\\"Query\\\") {\n"
              "    fields {\n"
              "      name\n"
              "      __typename\n"
              "    }\n"
              "  }\n"
              "}\"}";

  std::string intro = R"({"query":"\n    query IntrospectionQuery {\n      __schema {\n        \n        queryType { name }\n        mutationType { name }\n        subscriptionType { name }\n        types {\n          ...FullType\n        }\n        directives {\n          name\n          description\n          \n          locations\n          args {\n            ...InputValue\n          }\n        }\n      }\n    }\n\n    fragment FullType on __Type {\n      kind\n      name\n      description\n      \n      fields(includeDeprecated: true) {\n        name\n        description\n        args {\n          ...InputValue\n        }\n        type {\n          ...TypeRef\n        }\n        isDeprecated\n        deprecationReason\n      }\n      inputFields {\n        ...InputValue\n      }\n      interfaces {\n        ...TypeRef\n      }\n      enumValues(includeDeprecated: true) {\n        name\n        description\n        isDeprecated\n        deprecationReason\n      }\n      possibleTypes {\n        ...TypeRef\n      }\n    }\n\n    fragment InputValue on __InputValue {\n      name\n      description\n      type { ...TypeRef }\n      defaultValue\n      \n      \n    }\n\n    fragment TypeRef on __Type {\n      kind\n      name\n      ofType {\n        kind\n        name\n        ofType {\n          kind\n          name\n          ofType {\n            kind\n            name\n            ofType {\n              kind\n              name\n              ofType {\n                kind\n                name\n                ofType {\n                  kind\n                  name\n                  ofType {\n                    kind\n                    name\n                  }\n                }\n              }\n            }\n          }\n        }\n      }\n    }\n  ","operationName":"IntrospectionQuery"})";

  std::string params = R"({name:"DevVarLongStringArray",argin:{long:[1,2],std::string:["a","b"]},argin2:[1,2,3],argin3:["a","b","c"]})";

  try {

    std::string result;
    std::string allowMutation = "ALWAYS";

    JSON js0;
    parseJSON(j0,js0);
    std::cout << js0.size() << std::endl;

    execute(i1, result, allowMutation, true);
    std::cout << result << std::endl;

    execute(i2, result, allowMutation, true);
    std::cout << result << std::endl;

    execute(i3, result, allowMutation, true);
    std::cout << result << std::endl;

    execute(intro, result, allowMutation, true);
    std::cout << result << std::endl;

    JSON json;
    GQLEngine::parseJSON(params,json);
    std::cout << "Key:" << json.size() << std::endl;

  } catch (std::string& message) {
    std::cout << "GQL Parse error: " << message << std::endl;
  }

}

bool GQLEngine::execute(std::string& jsonRequest,std::string& result,std::string& mutationMode,bool allowMultiTangoHost) {

  result.clear();
  result.reserve(512*1024);
  this->allowMultiTangoHost = allowMultiTangoHost;

  try {
    time_t t0 = GetTicks();

    GQLJson req;
    result.append("{\"data\":");
    req.parse(mainSchema, jsonRequest);

    time_t t1 = GetTicks();
    double parseTime = (double)(t1-t0)/10.0;
    t0=t1;

    if(req.request->type=="mutation" && mutationMode=="NEVER") {
      result.clear();
      result.append(R"({"errors":"Forbidden"})");
      return false;
    }

    req.execute(this, result);
    result.push_back('}');

    t1 = GetTicks();
    double exeTime = (double)(t1-t0)/10.0;

    if(logEnabled()) {
      std::string logStr;
      logStr.append("Parse: ");
      GQLUtils::Double(parseTime,logStr);
      logStr.append("ms Exec: ");
      GQLUtils::Double(exeTime,logStr);
      logStr.append("ms");
      log("GQLEngine::execute", logStr);
    }

    return true;

  } catch (std::string& err) {
    result.clear();
    result.append(R"({"errors":")");
    GQLUtils::escape(err,result);
    result.append("\"}");
    return false;
  }

}

bool GQLEngine::logEnabled() {
  return forceLog || get_logger()->is_info_enabled();
}

void GQLEngine::log(const std::string& method,const std::string& message) {
  int sock = (factory)?factory->getConn()->getSock():0;
  if(get_logger()->is_info_enabled()) {
    INFO_STREAM << method << "(" << sock << ") " << message << std::endl;
  } else {
    std::cout << method << "(" << sock << ") " << message << std::endl;
  }
}

void GQLEngine::unregisterSubscription(Connection *conn,bool signalWSEnd) {

  log("GQLEngine::unregisterSubscription","UUID=" + conn->getUUID());

  std::string UUID = conn->getUUID();
  size_t nbConn = 0;

  // Unsubscribe
  for(auto & subscription : subscriptions) {
    if( subscription->getUUID() == UUID ) {
      subscription->unsubscribe();
      nbConn++;
    }
  }

  if(nbConn==0)
    return;

  // Wait that all pending events have been processed
  conn->wsProcessingMutex.lock();

  // Send complete to the WS client
  if(signalWSEnd)
    conn->sendComplete();

  // Free memory
  for(size_t i=0;i<subscriptions.size();) {
    if( subscriptions[i]->getUUID() == UUID ) {
      delete subscriptions[i];
      subscriptions.erase(subscriptions.cbegin() + i);
    } else {
      i++;
    }
  }

  conn->wsProcessingMutex.unlock();

}

bool GQLEngine::executeSubscription(std::string& jsonRequest,std::string& result) {

  result.clear();
  GQLJson *req = new GQLJson();

  try {
    req->parse(mainSchema, jsonRequest);
    std::vector<GQLSubscription *> subs = req->executeSubscription(this);
    if(subs.empty())
      throw "Subscription request cannot be empty";
    for(auto & sub : subs)
      subscriptions.push_back(sub);
    // req will be deleted when unregisterSubscription will be call
    // delete has to be done only once
    subs[0]->setMainRequest(req);
    return true;
  } catch (std::string& err) {
    delete req;
    result.clear();
    result.append(R"([{"message":")");
    result.append(err);
    result.append("\"}]");
    return false;
  }

}

void GQLEngine::parseJSON(std::string& jsonText,JSON& json) {

  Parser parser(jsonText,"JSON");
  parser.parseJSON(json);

}


// Include the schema
const char *gqlBase =
#include "graphql/schema/GQLSchema.schema"
;

} // namespace TangoGQL_ns
