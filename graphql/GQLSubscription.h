//
// GQLSubscription object
//

#ifndef TANGOGQL_GQLSUBSCRIPTION_H
#define TANGOGQL_GQLSUBSCRIPTION_H

#include "TangoGQL.h"

namespace TangoGQL_ns {

class GQLRequestFields;
class GQLJson;
class GQLSubscription;
class GQLDeviceAttributeFrame;

class TangoEventHandler:public Tango::CallBack {

public:

  /**
   * Create a Tango Event handler
   * @param root Root GQL engine
   * @param parent GQLSubscription parent
   * @param index Index in the given attribute list of subscribe() subscription
   * @param devName Device name
   * @param attName Attribute name
   */
  TangoEventHandler(GQLEngine *root,GQLSubscription *parent,int index,std::string& devName,std::string& attName);

  /**
   * Set subscription info
   * @param dev Device object
   * @param subId Subscription id returned by subscribe_event
   */
  void setSubscriptionInfo(Device *dev,int subId);

  /**
   * Send subsrciption error notification
   * @param evName Event type
   * @param error Error message
   */
  void sendSubscriptionError(std::string& evName,std::string& error);

  /**
   * Event callback
   * @param event Event data
   */
  void push_event(Tango::EventData *event);
  void push_event(Tango::AttrConfEventData *event);

  /**
   * Unsubscribe from tango device
   */
  void unsubscribe();

private:

  Connection *getConn();
  void sendFrame(GQLDeviceAttributeFrame *node);

  GQLSubscription *parent;
  int index;
  int subscribeId;
  std::string devName;
  std::string attName;
  Device *dev;
  GQLEngine *root;

};

class GQLSubscription {

public:

  GQLSubscription(DeviceFactory *factory,GQLRequestFields *req);
  ~GQLSubscription();

  void unsubscribe();

  std::string getUUID();
  DeviceFactory *getFactory();
  GQLRequestFields *getRequest();

  void setMainRequest(GQLJson *request);
  void addCallback(TangoEventHandler *callback);

private:

  DeviceFactory *factory;
  GQLRequestFields *req;
  GQLJson *mainRequest;
  std::vector<TangoEventHandler *> callbacks;

};

/**
 * Main entry point of the subscribe request
 */
DECLARE_EVAL(GQLEvalSubscription)
  GQLSubscription *subscription(void *node,GQLRequestFields *children,GQLRequestArgs *args);
  void subscribe(GQLEngine *root,Device *dev,std::string& attShortName,Tango::EventType eventType,TangoEventHandler *handler);
END_DECLARE_EVAL

} // namespace TangoGQL_ns

#endif //TANGOGQL_GQLSUBSCRIPTION_H
