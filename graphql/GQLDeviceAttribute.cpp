//
// GQLDeviceAttribute Object
//

#include "GQLDeviceAttribute.h"
#include "GQLMutationStatus.h"


namespace TangoGQL_ns {

#define FORMAT_SCALAR(func, typeCase, id, field, type) \
case Tango::typeCase:                                  \
func<Tango::type>(data.field, id, evalStr);            \
break;

#define FORMAT_ARRAY(func, typeCase, s,l, field, type)  \
case Tango::typeCase:                                   \
func<Tango::type>(data.field, s,l, evalStr);            \
break;

#define EXTRACT(typeCase,field,type)    \
case Tango::typeCase: {                 \
data.field = new std::vector<type>;          \
da >> (*(data.field));}break;


GQLDeviceAttribute::GQLDeviceAttribute(GQLEngine *root,Device *dev,Tango::DeviceAttribute& da,bool isHistory):GQLBase(root),da(da),dev(dev),isHistory(isHistory) {

  data.ptr = nullptr;
  encodedData = nullptr;

  if (!da.has_failed()) {

    if( da.quality!=Tango::ATTR_INVALID ) {

      // Special case for State (Workaround Tango cppApi issue #845)
      if(da.get_type()==Tango::DEV_STATE /*&& ::strcasecmp(da.get_name().c_str(),"state")==0*/) {
        data.states = new std::vector<Tango::DevState>;
        Tango::DevState s;
        da >> s;
        data.states->push_back(s);
        da << s;
        da.data_format = Tango::SCALAR;
        return;
      }

      // Extract data
      switch (da.get_type()) {
        EXTRACT(DEV_BOOLEAN, bools, Tango::DevBoolean);
        EXTRACT(DEV_STATE, states, Tango::DevState);
        EXTRACT(DEV_ENUM, enums, Tango::DevEnum);
        EXTRACT(DEV_UCHAR, u8, Tango::DevUChar);
        EXTRACT(DEV_SHORT, i16, Tango::DevShort);
        EXTRACT(DEV_USHORT, u16, Tango::DevUShort);
        EXTRACT(DEV_LONG, i32, Tango::DevLong);
        EXTRACT(DEV_ULONG, u32, Tango::DevULong);
        EXTRACT(DEV_LONG64, i64, Tango::DevLong64);
        EXTRACT(DEV_ULONG64, u64, Tango::DevULong64);
        EXTRACT(DEV_FLOAT, floats, Tango::DevFloat);
        EXTRACT(DEV_DOUBLE, doubles, Tango::DevDouble);
        EXTRACT(DEV_STRING, strings, std::string);
        case Tango::DEV_ENCODED: {
          encodedData = new Tango::DevEncoded();
          da >> *encodedData;
        } break;
      }

      // It seems that Tango does not update data_format for DeviceAttributeHistory !
      if(isHistory) da.data_format = ((da.get_dim_x()>1)?Tango::SPECTRUM:Tango::SCALAR);

    }

  }

}

GQLDeviceAttribute::~GQLDeviceAttribute() {
  if(data.ptr != nullptr) {
    data.ptr->clear();
    delete data.ptr;
  }
  delete encodedData;
}

void GQLDeviceAttribute::formatValue(std::string &evalStr,bool isWrite,bool resolveEnum) {

  int start;
  int length;

  if(da.has_failed() || da.get_quality()==Tango::ATTR_INVALID) {
    evalStr.append("null");
    return;
  }

  switch(da.get_data_format()) {

    case Tango::SCALAR:
      formatScalarValue(evalStr,isWrite,resolveEnum);
      break;

    case Tango::SPECTRUM:
      if(isWrite) {
        start = da.get_dim_x();
        length = da.get_written_dim_x();
      } else {
        start = 0;
        length = da.get_dim_x();
      }
      formatArrayValue(evalStr,start,length,resolveEnum);
      break;

    case Tango::IMAGE:
      if(isWrite) {
        start = da.get_dim_x()*da.get_dim_y();
        length = da.get_written_dim_x()*da.get_written_dim_y();
      } else {
        start = 0;
        length = da.get_dim_x()*da.get_dim_y();
      }
      formatArrayValue(evalStr,start,length,resolveEnum);
      break;

    default:
      evalStr.append("null");
  }

}

void GQLDeviceAttribute::formatArrayValue(std::string &evalStr,int start,int length,bool resolveEnum) {

  switch (da.get_type()) {
    FORMAT_ARRAY(serializeUIntArray, DEV_UCHAR, start, length, u8, DevUChar);
    FORMAT_ARRAY(serializeIntArray, DEV_SHORT, start, length, i16, DevShort);
    FORMAT_ARRAY(serializeUIntArray, DEV_USHORT, start, length, u16, DevUShort);
    FORMAT_ARRAY(serializeIntArray, DEV_LONG, start, length, i32, DevLong);
    FORMAT_ARRAY(serializeUIntArray, DEV_ULONG, start, length, u32, DevULong);
    FORMAT_ARRAY(serializeIntArray, DEV_LONG64, start, length, i64, DevLong64);
    FORMAT_ARRAY(serializeUIntArray, DEV_ULONG64, start, length, u64, DevULong64);
    FORMAT_ARRAY(serializeFloatArray, DEV_FLOAT, start, length, floats, DevFloat);
    FORMAT_ARRAY(serializeFloatArray, DEV_DOUBLE, start, length, doubles, DevDouble);
    case Tango::DEV_STATE:
      if(resolveEnum)
        serializeStateArray(data.states,start, length,evalStr);
      else
        serializeIntArray(data.states,start, length,evalStr);
      break;
    case Tango::DEV_ENUM:
      if(resolveEnum)
        serializeEnumArray(data.enums,start, length,evalStr);
      else
        serializeIntArray(data.enums,start, length,evalStr);
      break;
    case Tango::DEV_STRING:
      serializeStringArray(data.strings,start, length,evalStr);
      break;
    case Tango::DEV_BOOLEAN:
      serializeBoolArray(data.bools,start, length,evalStr);
      break;
    default:
      evalStr.append("null");
  }

}

void GQLDeviceAttribute::formatScalarValue(std::string &evalStr,bool isWrite,bool resolveEnum) {

  int id = (isWrite)?1:0;

  switch (da.get_type()) {

    FORMAT_SCALAR(serializeUIntScalar, DEV_UCHAR, id, u8, DevUChar);
    FORMAT_SCALAR(serializeIntScalar, DEV_SHORT, id, i16, DevShort);
    FORMAT_SCALAR(serializeUIntScalar, DEV_USHORT, id, u16, DevUShort);
    FORMAT_SCALAR(serializeIntScalar, DEV_LONG, id, i32, DevLong);
    FORMAT_SCALAR(serializeUIntScalar, DEV_ULONG, id, u32, DevULong);
    FORMAT_SCALAR(serializeIntScalar, DEV_LONG64, id, i64, DevLong64);
    FORMAT_SCALAR(serializeUIntScalar, DEV_ULONG64, id, u64, DevULong64);
    FORMAT_SCALAR(serializeFloatScalar, DEV_FLOAT, id, floats, DevFloat);
    FORMAT_SCALAR(serializeFloatScalar, DEV_DOUBLE, id, doubles, DevDouble);
    case Tango::DEV_STATE:
      if(resolveEnum)
        serializeState(data.states,id,evalStr);
      else
        serializeIntScalar(data.states,id,evalStr);
      break;
    case Tango::DEV_ENUM:
      if(resolveEnum)
        serializeEnum(data.enums,id,evalStr);
      else
        serializeIntScalar(data.enums,id,evalStr);
      break;
    case Tango::DEV_STRING:
      serializeString(data.strings,id,evalStr);
      break;
    case Tango::DEV_BOOLEAN:
      serializeBool(data.bools,id,evalStr);
      break;
    case Tango::DEV_ENCODED:
      serializeEncoded(encodedData,evalStr);
      break;
    default:
      evalStr.append("null");

  }

}


template<typename inType>
void GQLDeviceAttribute::serializeIntScalar(std::vector<inType> *value,int id,std::string &evalStr) {
  if(id<(int)value->size()) {
    int64_t v64 = (int64_t) (*value)[id];
    GQLUtils::Int64(v64,evalStr);
  } else {
    evalStr.append("null");
  }
}

template<typename inType>
void GQLDeviceAttribute::serializeUIntScalar(std::vector<inType> *value,int id,std::string &evalStr) {
  if(id<(int)value->size()) {
    uint64_t v64 = (uint64_t) (*value)[id];
    GQLUtils::UInt64(v64,evalStr);
  } else {
    evalStr.append("null");
  }
}

template<typename inType>
void GQLDeviceAttribute::serializeFloatScalar(std::vector<inType> *value,int id,std::string &evalStr) {
  if(id<(int)value->size()) {
    double v64 = (double) (*value)[id];
    GQLUtils::Double(v64,evalStr);
  } else {
    evalStr.append("null");
  }
}

void GQLDeviceAttribute::serializeStateArray(std::vector<Tango::DevState> *value,int start,int length,std::string &evalStr) {
  int end = start+length;
  if (end <= (int)value->size()) {
    evalStr.push_back('[');
    for(int i=start;i<end;i++) {
      GQLUtils::quote(Tango::DevStateName[(*value)[i]],evalStr);
      if(i<end-1) evalStr.push_back(',');
    }
    evalStr.push_back(']');

  } else {
    evalStr.append("null");
  }
}

void GQLDeviceAttribute::serializeState(std::vector<Tango::DevState> *value,int id,std::string &evalStr) {
  if (id < (int)value->size()) {
    GQLUtils::quote(Tango::DevStateName[(*value)[id]],evalStr);
  } else {
    evalStr.append("null");
  }
}

void GQLDeviceAttribute::serializeEnum(std::vector<Tango::DevEnum> *value,int id,std::string &evalStr) {
  if (id < (int)value->size()) {
    Tango::AttributeInfoEx props;
    dev->getAttributeProperties(da.get_name(),props);
    int eValue = (*value)[id];
    evalStr.push_back('\"');
    if( eValue>=0 && eValue<(int)props.enum_labels.size() )
      evalStr.append(props.enum_labels[eValue]);
    else
      evalStr.append(std::to_string(eValue));
    evalStr.push_back('\"');
  } else {
    evalStr.append("null");
  }
}
void GQLDeviceAttribute::serializeEnumArray(std::vector<Tango::DevEnum> *value,int start,int length,std::string &evalStr) {
  int end = start+length;
  if (end <= (int)value->size()) {
    Tango::AttributeInfoEx props;
    dev->getAttributeProperties(da.get_name(),props);
    evalStr.push_back('[');
    for(int i=start;i<end;i++) {
      int eValue = (*value)[i];
      evalStr.push_back('\"');
      if( eValue>=0 && eValue<(int)props.enum_labels.size() )
        evalStr.append(props.enum_labels[eValue]);
      else
        evalStr.append(std::to_string(eValue));
      evalStr.push_back('\"');
      if(i<end-1) evalStr.push_back(',');
    }
    evalStr.push_back(']');

  } else {
    evalStr.append("null");
  }
}

void GQLDeviceAttribute::serializeStringArray(std::vector<std::string> *value,int start,int length,std::string &evalStr) {
  int end = start+length;
  if (end <= (int)value->size()) {
    evalStr.push_back('[');
    for(int i=start;i<length;i++) {
      GQLUtils::quote((*value)[i],evalStr,true);
      if(i<end-1) evalStr.push_back(',');
    }
    evalStr.push_back(']');

  } else {
    evalStr.append("null");
  }
}

void GQLDeviceAttribute::serializeString(std::vector<std::string> *value,int id,std::string &evalStr) {
  if (id < (int)value->size()) {
    GQLUtils::quote((*value)[id],evalStr,true);
  } else {
    evalStr.append("null");
  }
}

void GQLDeviceAttribute::serializeBoolArray(std::vector<Tango::DevBoolean> *value,int start,int length,std::string &evalStr) {

  int end = start+length;
  if (end <= (int)value->size()) {
    evalStr.push_back('[');
    for(int i=start;i<end;i++) {
      evalStr.append((*value)[i]?"true":"false");
      if(i<end-1) evalStr.push_back(',');
    }
    evalStr.push_back(']');

  } else {
    evalStr.append("null");
  }
}

void GQLDeviceAttribute::serializeBool(std::vector<Tango::DevBoolean> *value,int id,std::string &evalStr) {
  if (id < (int)value->size()) {
    evalStr.append((*value)[id]?"true":"false");
  } else {
    evalStr.append("null");
  }
}

template<typename inType>
void GQLDeviceAttribute::serializeUIntArray(std::vector<inType> *value,int start,int length,std::string &evalStr) {

  int end = start+length;
  if (end <= (int)value->size()) {

    evalStr.push_back('[');
    for(int i=start;i<end;i++) {
      uint64_t v64 = (uint64_t) (*value)[i];
      GQLUtils::UInt64(v64,evalStr);
      if(i<end-1) evalStr.push_back(',');
    }
    evalStr.push_back(']');

  } else {
    evalStr.append("null");
  }

}

void GQLDeviceAttribute::serializeEncoded(Tango::DevEncoded *value,std::string &evalStr) {

  Tango::DevVarCharArray& edata = (*value).encoded_data;
  evalStr.push_back('[');
  for(size_t i=0;i<edata.length();i++) {
    uint64_t v64 = (uint64_t) edata[i];
    GQLUtils::UInt64(v64,evalStr);
    if(i<edata.length()-1) evalStr.push_back(',');
  }
  evalStr.push_back(']');

}

template<typename inType>
void GQLDeviceAttribute::serializeIntArray(std::vector<inType> *value,int start,int length,std::string &evalStr) {

  if ((start+length) <= (int)value->size()) {

    evalStr.push_back('[');
    for(int i=start;i<length;i++) {
      int64_t v64 = (int64_t) (*value)[i];
      GQLUtils::Int64(v64,evalStr);
      if(i<length-1) evalStr.push_back(',');
    }
    evalStr.push_back(']');

  } else {
    evalStr.append("null");
  }

}

template<typename inType>
void GQLDeviceAttribute::serializeFloatArray(std::vector<inType> *value,int start,int length,std::string &evalStr) {

  int end = start+length;
  if (end <= (int)value->size()) {

    evalStr.push_back('[');
    for(int i=start;i<end;i++) {
      double v64 = (double) (*value)[i];
      GQLUtils::Double(v64,evalStr);
      if(i<end-1) evalStr.push_back(',');
    }
    evalStr.push_back(']');

  } else {
    evalStr.append("null");
  }

}



} // namespace TangoGQL_ns
