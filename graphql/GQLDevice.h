//
// GQLDevice object
//

#ifndef TANGOGQL_GQLDEVICEPROXY_H
#define TANGOGQL_GQLDEVICEPROXY_H

#include "utils/GQLUtils.h"

namespace TangoGQL_ns {

class GQLEngine;

class GQLDevice: public GQLBase {

public:
  GQLDevice(GQLEngine *root,std::string &name,const std::string& type):GQLBase(root),name(name),type(type) {
    error = nullptr;
    dev = nullptr;
  }
  ~GQLDevice() {
    delete error;
    // Do not delete dev (it is managed by the DeviceFactory)
  }

  Tango::DevFailed   *error;
  Device             *dev;
  std::string&        name;
  const std::string&  type;

  std::string __typename() {
    return type;
  }

};

/**
 * Main entry point of the device(name:String!) query or mutation
 */
DECLARE_EVAL(GQLEvalDevice)
  explicit GQLEvalDevice(const std::string& type):type(type) {}
private:
  const std::string type;
END_DECLARE_EVAL

/** Returns the device name */
BEGIN_EVAL(GQLEvalDeviceName)
    GQLDevice *d = (GQLDevice *)node;
    GQLUtils::quote(d->name, evalStr);
END_EVAL

/** Returns the error */
BEGIN_EVAL(GQLEvalDeviceError)
    GQLDevice *d = (GQLDevice *)node;
    if(d->error== nullptr)
      evalStr.append("null");
    else
      appendError(d->error->errors,args,evalStr);
END_EVAL

/** Returns the attribute list */
BEGIN_EVAL(GQLEvalDeviceAttributeList)
    GQLDevice *d = (GQLDevice *)node;
    if(d->dev == nullptr) {
      evalStr.append("null");
    } else {
      try {
        std::vector<std::string> list;
        d->dev->getAttributeList(list);
        GQLUtils::stringArr(list, evalStr);
      } catch (Tango::DevFailed &e) {
        evalStr.append("null");
        d->error = new Tango::DevFailed(e);
      }
    }
END_EVAL

/** Returns the command list */
BEGIN_EVAL(GQLEvalDeviceCommandList)
    GQLDevice *d = (GQLDevice *)node;
    if(d->dev == nullptr) {
      evalStr.append("null");
    } else {
      try {
        std::vector<std::string> list;
        d->dev->getCommandList(list);
        GQLUtils::stringArr(list, evalStr);
      } catch (Tango::DevFailed &e) {
        evalStr.append("null");
        d->error = new Tango::DevFailed(e);
      }
    }
END_EVAL

/** read_attributes */
DECLARE_EVAL(GQLEvalDeviceReadAttributes)
END_DECLARE_EVAL

/** read_attribute_hist */
DECLARE_EVAL(GQLEvalDeviceReadAttributeHist)
END_DECLARE_EVAL

/** write_attribute */
DECLARE_EVAL(GQLEvalDeviceWriteAttribute)
private:
  size_t findEnum(std::string &attName, std::string &value, std::vector<std::string> &enumValues);
  size_t findState(std::string &attName, std::string &value);
END_DECLARE_EVAL

/** Read attribute properties */
DECLARE_EVAL(GQLEvalDeviceAttributeInfo)
END_DECLARE_EVAL

/** Write attribute properties */
DECLARE_EVAL(GQLEvalDeviceWriteAttributeInfo)
END_DECLARE_EVAL

/** Execute command */
DECLARE_EVAL(GQLEvalDeviceCommandInOut)
END_DECLARE_EVAL

/** Read command properties */
DECLARE_EVAL(GQLEvalDeviceCommandInfo)
END_DECLARE_EVAL

/** Dump the factory */
BEGIN_EVAL(GQLEvalQueryDumpFactory)
    DeviceFactory *factory = ((GQLEngine *)node)->getFactory();
    factory->dump(evalStr);
END_EVAL

} // namespace TangoGQL_ns

#endif //TANGOGQL_GQLDEVICEPROXY_H
